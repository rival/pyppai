(**
 * Based on Xavier's static analysis code.
 *
 * Reference:
 *   sa_utils.ml: aux funcs used in sa_gen.ml
 *   sa_gen.ml: defines static analysis using Apron
 *   main.ml: run static analysis defined in sa_gen.ml
 * 
 * Apron Ocaml interface:
 *   http://apron.cri.ensmp.fr/library/0.9.10/mlapronidl/index.html 
 *)


(** ******************)
(** modules/functors *)
(** ******************)

(* Ints = int + infty *)
module type ARITH =
  sig
    type t (* values *)
    val to_string: t -> string
    val compare: t -> t -> int
    val min: t -> t -> t
    val max: t -> t -> t
    val of_int: int -> t
    val to_int: t -> int option
    val minf: t
    val pinf: t
    val add: t -> t -> t
    val sub: t -> t -> t
    val mul: t -> t -> t
    val div: t -> t -> t
  end
module Ints : ARITH

(* VALUE_DOMAIN *)    
module type VALUE_DOMAIN =
  sig
    type aval
    (* Printing *)
    val aval_to_string: aval -> string
    (* Constants *)
    val aval_bot: aval
    val aval_top: aval
    val aval_pos: aval
    (* Bounds *)
    val of_bounds: (int option * int option) option -> aval
    val to_bounds: aval -> (int option * int option) option
    (* Lattice operations *)
    val aval_is_le: aval -> aval -> bool
    val aval_lub:   aval -> aval -> aval
    val aval_glb:   aval -> aval -> aval
    val aval_widen: aval -> aval -> aval
    (* Abstract operations *)
    val aval_of_const: int -> aval
    val aval_add: aval -> aval -> aval
    val aval_sub: aval -> aval -> aval
    val aval_mul: aval -> aval -> aval
    val aval_cond: aval -> op -> int -> aval
  end
module Val_Consts : VALUE_DOMAIN
module Val_Signs  : VALUE_DOMAIN + extra
module Val_Intvs  : VALUE_DOMAIN

(* STATE_DOMAIN *)
module type STATE_DOMAIN =
  sig
    type astate
    (* Printing *)
    val pp: out_channel -> astate -> unit
    (* Top *)
    val top: var list -> astate
    (* Bounds *)
    val get_range_of_var: var -> astate -> (int option * int option) option
    (* Lattice operations *)
    val is_le: astate -> astate -> bool
    val join: astate -> astate -> astate
    val widen: astate -> astate -> astate
    (* Abstract Operations *)
    val post_cond: expr -> astate -> astate
    val post_input: var -> astate -> astate
    val post_assign: var -> expr -> astate -> astate
    (* Variables, functions and block operations *)
    val block_enter: astate -> astate
    val block_exit: astate -> astate
    val function_entry: astate -> astate
    val function_exit: astate -> astate -> astate
    val var_new: var -> astate -> astate
  end

(* VALUE_DOMAIN -> STATE_DOMAIN *)
module Non_rel : VALUE_DOMAIN -> STATE_DOMAIN
                                   
(* PRE_ARPON *)
module type PRE_APRON =
  sig
    type t
    val man: t Apron.Manager.t
  end
module PA_box    : PRE_APRON
module PA_oct    : PRE_APRON
module PA_polka  : PRE_APRON

(* PRE_APRON -> STATE_DOMAIN *)
module Dom_Apron : PRE_APRON -> STATE_DOMAIN
module E = Apron.Environment
module A = Apron.Abstract1

(* STATE_DOMAIN -> STATIC_ANALYSIS *)
module Sa : STATE_DOMAIN ->
            (module type _ =
               sig
                 val alfp : ?
                 val ai_stmt : ?
                 val ai_list : ?
                 val ai_block : ?
                 val analyze_prog : ?
               end)


(** ******************************)
(** Apron APIS used in Dom_Apron *)
(** ******************************)

(* defns in Dom_Apron *)
module PA : PRE_APRON
type astate = (PA.t) Apron.Abstract1.t
val man = PA.man

(* 'a Apron.Abstract1.t *)
type 'a Apron.Abstract1.t = {
  mutable abstract0 : 'a Apron.Abstract0;
  mutable env : Apron.Environment.t;
}
            
(* Apron.Var *)
val of_string : string -> t               
      
(* Apron.Environment *)
val make : Var.t array -> Var.t array -> t
val add : t -> Var.t array -> Var.t array -> t
                     
(* Apron.Abstract1 *)
(* - env *)
val env : 'a t -> Environment.t
(* val change_environment : 'a Manager.t -> 'a t -> Environment.t -> bool -> 'a t (* add_var *) *)

(* - operation *)
val top : 'a Manager.t -> Environment.t -> 'a t
val is_leq : 'a Manager.t -> 'a t -> 'a t -> bool
val join   : 'a Manager.t -> 'a t -> 'a t -> 'a t
val widening : 'a Manager.t -> 'a t -> 'a t -> 'a t
val forget_array : 'a Manager.t -> 'a t -> Var.t array -> bool -> 'a t (* used in: post_input *)
val assign_texpr_array : 'a Manager.t -> 'a t -> Var.t array
                         -> Texpr1.t array -> 'a t option -> 'a t (* used in: post_assign *)
(* -- for generating Texpr1.t array, we need to implement the following *)
val translate_expr : Environment.t -> expr -> Texpr1.t (* see: sa_util.ml *)

(* - debug *)
val to_lincons_array : 'a Manager.t -> 'a t -> Lincons1.earray (* for debug printing *)
