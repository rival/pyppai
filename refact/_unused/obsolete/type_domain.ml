(** 
 * Abstract domain for simple type inference
 *
 * To compile this file:
 *   ocamlc -o type_domain ir.ml util.ml static_analysis.ml type_domain.ml
 *)

open Ir
open Util
open Static_analysis

module SS = StringSet

module TyDomain : ABST_DOMAIN =
  struct

    (* t is the type for abstract values:
     * (1) TyBot means the empty set of states.
     * (2) TyTuple(v, w, a, b) means the set of states where 
     *     the variables in v store real numbers,
     *     the variables in w store tensors, 
     *     the string constructors in a are used as names of sampled random variables, and
     *     the strings in b are used as names of sampled random variables.
     *)
    type t = TyBot | TyTuple of SS.t * SS.t * SS.t * SS.t 

    type exp_ty = ET_tensor | ET_real | ET_bool | ET_unknown

    let rec get_exp_ty v w = function
      | True | False -> 
          ET_bool
      | UOp(Not, _) -> 
          ET_bool
      | Comp _ -> 
          ET_bool
      | Num _ -> 
          ET_real
      | Str _ | StrFmt _ -> 
          ET_unknown
      | Name(x) -> 
          if SS.mem x v then 
            ET_real
          else if SS.mem x w then 
            ET_tensor
          else 
            ET_unknown
      | BOp(bop, e1, e2) -> 
          begin 
            match bop with
            | And | Or -> ET_bool
            | Add | Sub | Mult | Div ->
                let e1_ty = get_exp_ty v w e1 in
                let e2_ty = get_exp_ty v w e2 in
                if (e1_ty = ET_tensor || e2_ty = ET_tensor) then
                  ET_tensor
                else if (e1_ty = ET_real || e2_ty = ET_real) then
                  ET_real
                else if (e1_ty = ET_bool && e2_ty = ET_bool) then
                  ET_bool
                else
                  ET_unknown
          end

    let is_tensor_exp v w e =
      get_exp_ty v w e = ET_tensor
    let is_real_exp v w e =
      get_exp_ty v w e = ET_real

    let is_real_dist = function
      | Normal | Exponential | Poisson -> true
      | Bernoulli -> false
    let is_bool_dist = function
      | Normal | Exponential | Poisson -> false
      | Bernoulli -> true

    let update_name_sets a b = function
      | Str(s) -> (a, SS.add s b)
      | StrFmt(s, _) -> (SS.add s a, b)
      | _ -> (a, b)

    let eval ac = function
      | TyBot -> 
          TyBot
      | TyTuple(v, w, a, b) as z ->
          match ac with 
          | Assume _ | Observe _ -> 
              z 
          | Sample(x, name, dist, args) ->
              let (a_new, b_new) = update_name_sets a b name in
              let (v_new, w_new) = 
                if (List.exists (is_tensor_exp v w) args) then    
                  (SS.remove x v, SS.add x w)
                else if (is_real_dist dist) then
                  (SS.add x v, SS.remove x w)
                else
                  (SS.remove x v, SS.remove x w) 
              in 
              TyTuple(v_new, w_new, a_new, b_new)
          | Assn(x, e) ->
              begin 
                match get_exp_ty v w e with 
                | ET_tensor -> 
                    TyTuple(SS.remove x v, SS.add x w, a, b)
                | ET_real ->
                    TyTuple(SS.add x v, SS.remove x w, a, b)
                | ET_unknown | ET_bool -> 
                    TyTuple(SS.remove x v, SS.remove x w, a, b)
              end
          | AssnCall(x, _, _) ->
              TyTuple(SS.remove x v, SS.remove x w, a, b)

    let bot = TyBot

    let join z1 z2 =
      match (z1, z2) with 
      | (_, TyBot) -> 
          z1
      | (TyBot, _) -> 
          z2
      | (TyTuple(v1, w1, a1, b1), TyTuple(v2, w2, a2, b2)) -> 
          let v = SS.inter v1 v2 in
          let w = SS.inter w1 w2 in 
          let a = SS.inter a1 a2 in 
          let b = SS.inter b1 b2 in 
          TyTuple(v, w, a, b)

    let widen = join

    let leq z1 z2 =
      match (z1, z2) with
      | (TyBot, _) -> 
          true
      | (_, TyBot) -> 
          false
      | (TyTuple(v1, w1, a1, b1), TyTuple(v2, w2, a2, b2)) -> 
          (SS.subset v2 v1) 
          && (SS.subset w2 w1) 
          && (SS.subset a2 a1) 
          && (SS.subset b2 b1)
  end
