(**
 * The funtion cleanup_l in this file gets a raw Pyast.stmt list to return a new Pyast.smst list with same semantics.
 * It pulls out every Call expression inside an expression e to the statement level by inserting a statement like Assign(v, Call(..), ..) before the expression e, where v is a fresh variable.
 *
 * To compile this file: 
 *   ocamlfind ocamlc  -o pyast_cleanup pyast.ml pyast_cleanup.ml;
 *
 *)
type 'a expr_stmts_tuple = 'a Pyast.expr * 'a Pyast.stmt list ;;

let getExpr (t : 'a expr_stmts_tuple) : 'a Pyast.expr = 
  match t with
  | (e, _) -> e 

let getStmts (t: 'a expr_stmts_tuple) : 'a Pyast.stmt list = 
  match t with
  | (_, ss) -> ss

let fresh_count = ref (-1) ;;

let gen_fresh_var () = incr fresh_count; Pyast.Name ("@FRESH" ^ (string_of_int !fresh_count), Pyast.Store, "a") ;;

type 'a with_stmts_tuple = 'a Pyast.withitem list * 'a Pyast.stmt list;;

let getItems (t: 'a with_stmts_tuple) : 'a Pyast.withitem list = 
  match t with 
  | (wl, _) -> wl

let getStmts_w (t: 'a with_stmts_tuple) : 'a Pyast.stmt list =
  match t with
  | (_ , stmts) -> stmts

let rec cleanup_expr (e : 'a Pyast.expr) : 'a expr_stmts_tuple =        
  match e with
  | Pyast.BoolOp (op, values, a) ->
     let () = assert (List.length values = 2) in
     let l  = values |> List.hd |> cleanup_expr in
     let r  = values |> List.tl |> List.hd |> cleanup_expr in
     let l_expr  = l |> getExpr in
     let r_expr  = r |> getExpr in
     let l_stmts = l |> getStmts in
     let r_stmts = r |> getStmts in
      (Pyast.BoolOp (op,
                     l_expr :: r_expr :: [],
                     a),
       l_stmts @ r_stmts)

  | Pyast.BinOp (left, op, right, a) ->
     let l = left |> cleanup_expr in
     let r = right |> cleanup_expr in
     let l_expr  = l |> getExpr in
     let r_expr  = r |> getExpr in
     let l_stmts = l |> getStmts in
     let r_stmts = r |> getStmts in
      (Pyast.BinOp (l_expr, op, r_expr, a),
       l_stmts @ r_stmts)
                         
  | Pyast.UnaryOp (op, operand, a) ->
     let exst = operand |> cleanup_expr in
      (Pyast.UnaryOp (op, exst |> getExpr, a),
       exst |> getStmts)
     
  | Pyast.Compare (left, ops, comparators, a) ->
     let () = assert (List.length ops = 1 && List.length comparators = 1) in
     let op    = List.hd ops in
     let right = List.hd comparators in
     let l = left  |> cleanup_expr in
     let r = right |> cleanup_expr in
     let l_expr  = l |> getExpr in
     let r_expr  = r |> getExpr in
     let l_stmts = l |> getStmts in
     let r_stmts = r |> getStmts in 
      (Pyast.Compare (l_expr, op :: [], r_expr :: [], a),
       l_stmts @ r_stmts)

  (* When we see a Call expression sitting at an undesired position, we will pull it out. *)        
  | Pyast.Call (func, args, keywords, a) -> 
     let freshvar = gen_fresh_var () in
     let clean_args =    List.map getExpr  (List.map cleanup_expr args) in
     let stmts = List.concat (List.map getStmts (List.map cleanup_expr args)) in 
      (freshvar,
       Pyast.Assign (freshvar :: [], 
                     Pyast.Call (func, clean_args, keywords, a), 
                    "a"):: stmts)

  | Pyast.Num (n, _) -> (e, [])
  | Pyast.Str (s, _) -> (e, [])
  | Pyast.Attribute (value, attr, ctx, _) -> (e, [])
  | Pyast.Name  (id,   ctx, _) -> (e, [])
  | Pyast.List  (elts, ctx, _) -> (e, [])
  | Pyast.Tuple (elts, ctx, _) -> (e, [])

let rec cleanup_withitems (items : 'a Pyast.withitem list) : 'a with_stmts_tuple =
  match items with
  | (context, vars) :: tl ->
     let c_context = context |> cleanup_expr |> getExpr in
     let c_stmts   = context |> cleanup_expr |> getStmts in
     let tlitems   = tl |> cleanup_withitems |> getItems in
     let tlstmts   = tl |> cleanup_withitems |> getStmts_w in
     ((c_context, vars) :: tlitems,     
      List.append c_stmts tlstmts)
  | [] -> ([],[])

let rec cleanup_s (s : 'a Pyast.stmt) : 'a Pyast.stmt list = 
  match s with
  (* this is the first form in which a Call expression is allowed to appear*)
  | Pyast.Assign (targets, 
                  Pyast.Call (func, args, keywords, a),
                  b) -> 
     let () = assert (List.length targets = 1) in (* not sure if we need this *)
     let clean_args =    List.map getExpr  (List.map cleanup_expr args) in
     let stmts = List.concat (List.map getStmts (List.map cleanup_expr args)) in            
     List.append stmts 
                 (Pyast.Assign (targets, 
                                Pyast.Call (func, clean_args, keywords, a), 
                                b) 
                  :: []) 

  (* second possible pattern which contains a Call expression *)         
  | Pyast.Expr (Pyast.Call (func, args, keywords, a), b) ->
     let clean_args =    List.map getExpr  (List.map cleanup_expr args) in
     let stmts = List.concat (List.map getStmts (List.map cleanup_expr args)) in            
     List.append stmts
                 (Pyast.Expr (Pyast.Call (func, clean_args, keywords, a), b) 
                  :: []) 
               
  | Pyast.Assign (targets, value, a) ->
     let () = assert (List.length targets = 1) in
     let clean_value = value |> cleanup_expr |> getExpr in
     let stmts       = value |> cleanup_expr |> getStmts in    
     stmts @ (Pyast.Assign (targets, clean_value, a) :: [])
     
  | Pyast.While (test, body, orelse, a) ->
     let () = assert (orelse = []) in
     let clean_test = test |> cleanup_expr |> getExpr in
     let calls      = test |> cleanup_expr |> getStmts in
     List.append calls 
                 (Pyast.While (clean_test,
                               body |> cleanup_l, 
                               orelse,
                               a) 
                  :: [])

  | Pyast.If (test, body, orelse, a) ->
     let clean_test = test |> cleanup_expr |> getExpr in
     let calls      = test |> cleanup_expr |> getStmts in
     List.append calls
                 (Pyast.If (clean_test,
                            body   |> cleanup_l,
                            orelse |> cleanup_l,
                            a) 
                  :: []) 
        
  | Pyast.With (items, body, a) ->
     let stmts       = items |> cleanup_withitems |> getStmts_w in
     let clean_items = items |> cleanup_withitems |> getItems in             
     List.append stmts (* Call stmts pulled out of items *)
                 (Pyast.With (clean_items,
                              body  |> cleanup_l,
                              a) 
                  :: [])
          
  | Pyast.Expr (value, a) ->
     let clean_value = value |> cleanup_expr |> getExpr in
     let stmts       = value |> cleanup_expr |> getStmts in    
     stmts @ (Pyast.Expr (clean_value, a) :: [])

  | Pyast.Break    (_) -> s :: []
  | Pyast.Continue (_) -> s :: []
  | _ -> failwith "to_prog_s"


and cleanup_l (slist : 'a Pyast.stmt list) : 'a Pyast.stmt list =
  match slist with
  | hd :: tl -> (hd |> cleanup_s) @ (tl |> cleanup_l)
  | [] -> failwith "cleanup_l"  
