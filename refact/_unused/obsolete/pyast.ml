(**
 * This file is based on ast.ml in ocaml-pythonlib.
 *
 * The full abstract grammar is available at:
 *   http://docs.python.org/library/ast.html#abstract-grammar
 *
 * To compile this file:
 *   ocamlc -o pyast pyast.ml; ./pyast
 *)


(** ********************)
(** Defn of Python AST *)
(** ********************)
(* 'a denotes the type of annotation. E.g., (lineno, col_offset) : int * int = 'a *)

(* built-in types *)
type identifier = string

type number =
  | Int     of int
  | Float   of float
  (* | LongInt of int *)
  (* | Imag    of string *)

(* module Python *)
type 'a modl =
  | Module      of 'a stmt list (* body *) * 'a
  (* | Interactive of 'a stmt list (* body *) * 'a *)
  (* | Expression  of 'a expr      (* body *) * 'a *)
  (* | Suite       of 'a stmt list (* body *) * 'a *)

and 'a stmt =
  | FunctionDef of identifier (* name *) * 'a arguments (* args *) *
                     'a stmt list (* body *) * 'a expr list (* decorator_list *) *
                       'a expr option (* returns *) * 'a
  (* | AsyncFunctionDef of ... *)

  (* | ClassDef of identifier (* name *) * 'a expr list (* bases *) *
                  'a keyword list (* keywords *) *
                    'a stmt list (* body *) * 'a expr list (* decorator_list *) * 'a *)
  | Return   of 'a expr option (* value *) * 'a

  (* | Delete    of 'a expr list (* targets *) * 'a *)
  | Assign    of 'a expr list (* targets *) * 'a expr (* value *) * 'a
  (* | AugAssign of 'a expr      (* target  *) * operator (* op *) * 'a expr (* value *) * 'a *)
  (* | AnnAssign of ... *)

  | For   of 'a expr (* target *) * 'a expr (* iter *) *
               'a stmt list (* body *) * 'a stmt list (* orelse *) * 'a
  (* | AsyncFor of ... *)
  | While of 'a expr (* test *) * 'a stmt list (* body *) * 'a stmt list (* orelse *) * 'a
  | If    of 'a expr (* test *) * 'a stmt list (* body *) * 'a stmt list (* orelse *) * 'a
  | With  of 'a withitem list (* items *) * 'a stmt list (* body *) * 'a
  (* | AsyncWith of ... *)

  (* | Raise  of 'a expr option (* exc *) * 'a expr option (* cause *) *)
  (* | Try    of 'a stmt list (* body *) * 'a excepthandler list (* handlers *) *
                'a stmt list (* orelse *) * 'a stmt list (* finalbody *) * 'a *)
  (* | Assert of 'a expr (* test *) * 'a expr option (* msg *) * 'a *)

  (* | Import     of alias list (* names *) * 'a *)
  (* | ImportFrom of identifier option (* module *) * alias list (* names *)
                  * int option (* level *) * 'a *)

  (* | Global   of identifier list (* names *) * 'a *)
  (* | Nonlocal of ... *)
  | Expr     of 'a expr (* value *) * 'a
  | Pass     of 'a
  | Break    of 'a
  | Continue of 'a

and 'a expr =
  | BoolOp   of boolop (* op *) * 'a expr list (* values *) * 'a
  | BinOp    of 'a expr (* left *) * operator (* op *) * 'a expr (* right *) * 'a
  | UnaryOp  of unaryop (* op *) * 'a expr (* operand *) * 'a
  (* | Lambda   of 'a arguments (* args *) * 'a expr (* body *) * 'a *)
  (* | IfExp    of 'a expr (* test *) * 'a expr (* body *) * 'a expr (* orelse *) * 'a *)
  (* | Dict     of 'a expr list (* keys *) * 'a expr list (* values *) * 'a *)
  (* | Set      of 'a expr list (* elts *) *)
  (* | ListComp of 'a expr (* elt *) * 'a comprehension list (* generators *) * 'a *)
  (* | SetComp  of ... *)
  (* | DictComp of ... *)
  (* | GeneratorExp of 'a expr (* elt *) * 'a comprehension list (* generators *) * 'a *)

  (* | Await of ... *)
  (* | Yield of 'a expr option (* value *) * 'a *)
  (* | YieldFrom of ... *)

  | Compare of 'a expr (* left *) * cmpop list (* ops *) * 'a expr list (* comparators *) * 'a
  | Call of 'a expr (* func *) * 'a expr list (* args *) * 'a keyword list (* keywords *) * 'a
  | Num of number (* n *) * 'a
  | Str of string (* s *) * 'a
  (* | FormattedValue of 'a expr (* value *) * int option (* conversion *) *
                        'a expr option (* format_spec *) * 'a *)
  (* | JoinedStr of ... *)
  (* | Bytes of ... *)
  (* | NameConstant of ... *)
  (* | Ellipsis of ... *)
  (* | Constant of ... *)

  | Attribute of 'a expr (* value *) * identifier (* attr *) * expr_context (* ctx *) * 'a
  (* | Subscript of 'a expr (* value *) * 'a slice (* slice *) * expr_context (* ctx *) * 'a *)
  (* | Starred of ... *)
  | Name  of identifier (* id *)     * expr_context (* ctx *) * 'a
  | List  of 'a expr list (* elts *) * expr_context (* ctx *) * 'a
  | Tuple of 'a expr list (* elts *) * expr_context (* ctx *) * 'a

(* AugLoad and AugStore are not used *)
and expr_context = Load | Store | Del | (* AugLoad | AugStore | *) Param

(* and 'a slice =
  | Slice of 'a expr option (* lower *) * 'a expr option (* upper *) * 'a expr option (* step *)
  | ExtSlice of 'a slice list (* dims *)
  | Index    of 'a expr (* value *) *)

and boolop = And | Or

and operator = Add | Sub | Mult | MatMult | Div | Mod | Pow | LShift
               | RShift | BitOr | BitXor | BitAnd | FloorDiv

and unaryop = Invert | Not | UAdd | USub

and cmpop = Eq | NotEq | Lt | LtE | Gt | GtE | Is | IsNot | In | NotIn

(* and 'a comprehension = 'a expr (* target *) * 'a expr (* iter *) * 'a expr list (* ifs *) *
                         int (* is_async *) *)

(* and 'a excepthandler = ExceptHandler of 'a expr option (* type *) * identifier option (* name *) *
                                          'a stmt list (* body *) * 'a *)

and 'a arguments = 'a expr list (* args *) * 'a arg option (* vararg *) *
                     'a arg list (* kwonlyargs *) * 'a expr list (* kw_defaults *) *
                       'a arg option (* kwargs *) * 'a expr list (* defaults *)

and 'a arg = identifier (* arg *) * 'a expr option (* annotation *) * 'a

and 'a keyword = identifier option (* arg *) * 'a expr (* value *)

(* and alias = identifier (* name *) * identifier option (* asname *) *)

and 'a withitem = 'a expr (* context_expr *) * 'a expr option (* optional_vars *)


(** *************)
(** name_of_... *)
(** *************)
let name_of_number = function
  | Int _       -> "Int"
  | Float _     -> "Float"
  (* | LongInt _   -> "LongInt" *)
  (* | Imag _      -> "Imag" *)
and name_of_modl = function
  | Module _      -> "Module"
  (* | Interactive _ -> "Interactive" *)
  (* | Expression _  -> "Expression" *)
  (* | Suite _       -> "Suite" *)
and name_of_stmt = function
  | FunctionDef _ -> "FunctionDef"
  (* | AsyncFunctionDef _ -> ... *)
  (* | ClassDef _    -> "ClassDef" *)
  | Return _      -> "Return"
  (* | Delete _      -> "Delete" *)
  | Assign _      -> "Assign"
  (* | AugAssign _   -> "AugAssign" *)
  (* | AnnAssign _   -> ... *)
  | For _         -> "For"
  (* | AsyncFor _    -> ... *)
  | While _       -> "While"
  | If _          -> "If"
  | With _        -> "With"
  (* | AsyncWith _   -> ... *)
  (* | Raise _       -> "Raise" *)
  (* | Try _         -> "Try" *)
  (* | Assert _      -> "Assert" *)
  (* | Import _      -> "Import" *)
  (* | ImportFrom _  -> "ImportFrom" *)
  (* | Global _      -> "Global" *)
  (* | Nonlocal _    -> ... *)
  | Expr _        -> "Expr"
  | Pass _        -> "Pass"
  | Break _       -> "Break"
  | Continue _    -> "Continue"
and name_of_expr = function
  | BoolOp _       -> "BoolOp"
  | BinOp _        -> "BinOp"
  | UnaryOp _      -> "UnaryOp"
  (* | Lambda _       -> "Lambda" *)
  (* | IfExp _        -> "IfExp" *)
  (* | Dict _         -> "Dict" *)
  (* | ListComp _     -> "ListComp" *)
  (* | SetComp _      -> ... *)
  (* | DictComp _     -> ... *)
  (* | GeneratorExp _ -> "GeneratorExp" *)
  (* | Await _        -> ... *)
  (* | Yield _        -> "Yield" *)
  (* | YieldFrom _    -> "YieldFrom" *)
  | Compare _      -> "Compare"
  | Call _         -> "Call"
  | Num _          -> "Num"
  | Str _          -> "Str"
  (* | FormattedValue _ -> "FormattedValue" *)
  (* | JoinedStr _    -> ... *)
  (* | Bytes _        -> ... *)
  (* | NameConstant _ -> ... *)
  (* | Ellipsis _     -> ... *)
  (* | Constant _     -> ... *)
  | Attribute _    -> "Attribute"
  (* | Subscript _    -> "Subscript" *)
  (* | Starred _      -> ... *)
  | Name _         -> "Name"
  | List _         -> "List"
  | Tuple _        -> "Tuple"
and name_of_expr_context = function
  | Load        -> "Load"
  | Store       -> "Store"
  | Del         -> "Del"
  (* | AugLoad     -> "AugLoad" *)
  (* | AugStore    -> "AugStore" *)
  | Param       -> "Param"
(* and name_of_slice = function
  | Slice _     -> "Slice"
  | ExtSlice _  -> "ExtSlice"
  | Index _     -> "Index" *)
and name_of_boolop = function
  | And -> "And"
  | Or  -> "Or"
and name_of_operator = function
  | Add         -> "Add"
  | Sub         -> "Sub"
  | Mult        -> "Mult"
  | MatMult     -> "MatMult"
  | Div         -> "Div"
  | Mod         -> "Mod"
  | Pow         -> "Pow"
  | LShift      -> "LShift"
  | RShift      -> "RShift"
  | BitOr       -> "BitOr"
  | BitXor      -> "BitXor"
  | BitAnd      -> "BitAnd"
  | FloorDiv    -> "FloorDiv"
and name_of_unaryop = function
  | Invert      -> "Invert"
  | Not         -> "Not"
  | UAdd        -> "UAdd"
  | USub        -> "USub"
and name_of_cmpop = function
  | Eq          -> "Eq"
  | NotEq       -> "NotEq"
  | Lt          -> "Lt"
  | LtE         -> "LtE"
  | Gt          -> "Gt"
  | GtE         -> "GtE"
  | Is          -> "Is"
  | IsNot       -> "IsNot"
  | In          -> "In"
  | NotIn       -> "NotIn"
(* and name_of_excepthandler = function
  | ExceptHandler _ -> "ExceptHandler" *)


(** ***************)
(** string_of_... *)
(** ***************)
let string_of_number = function
  | Int (n)      -> string_of_int n
  | Float (n)    -> (string_of_float n)
  (* | LongInt (n)  -> (string_of_int n) ^ "L" *)
  (* | Imag (n)     -> n *)
let string_of_boolop = function
  | And -> "and"
  | Or  -> "or"
let string_of_operator = function
  | Add         -> "+"
  | Sub         -> "-"
  | Mult        -> "*"
  | MatMult     -> "@"
  | Div         -> "/"
  | Mod         -> "%"
  | Pow         -> "**"
  | LShift      -> "<<"
  | RShift      -> ">>"
  | BitOr       -> "|"
  | BitXor      -> "^"
  | BitAnd      -> "&"
  | FloorDiv    -> "//"
let string_of_unaryop = function
  | Invert -> "~"
  | Not    -> "not"
  | UAdd   -> "+"
  | USub   -> "-"
let string_of_cmpop = function
  | Eq    -> "=="
  | NotEq -> "!="
  | Lt    -> "<"
  | LtE   -> "<="
  | Gt    -> ">"
  | GtE   -> ">="
  | Is    -> "is"
  | IsNot -> "is not"
  | In    -> "in"
  | NotIn -> "not in"


(*
(** **************)
(** annot_of_... *)
(** **************)
let annot_of_modl = function
  | Module (_, a)
  (* | Interactive (_, a) *)
  (* | Expression (_, a) *)
  (* | Suite (_, a) *)
    -> a
and annot_of_stmt = function
  | FunctionDef (_, _, _, _, _, a)
  (* | AsyncFunctionDef (...) *)
  (* | ClassDef (_, _, _, _, a) *)
  | Return (_, a)
  (* | Delete (_, a) *)
  | Assign (_, _, a)
  (* | AugAssign (_, _, _, a) *)
  (* | AnnAssign (...) *)
  | For (_, _, _, _, a)
  (* | AsyncFor (..) *)
  (* | While (_, _, _, a) *)
  | If (_, _, _, a)
  | With (_, _, a)
  (* | AsyncWith (..) *)
  (* | Raise (_, _, _, a) *)
  (* | Try (_, _, _, a) *)
  (* | Assert (_, _, a) *)
  (* | Import (_, a) *)
  (* | ImportFrom (_, _, _, a) *)
  (* | Global (_, a) *)
  (* | Nonlocal (..) *)
  | Expr (_, a)
  | Pass (a)
  | Break (a)
  | Continue (a)
    -> a
and annot_of_expr = function
  | BoolOp (_, _, a)
  | BinOp (_, _, _, a)
  | UnaryOp (_, _, a)
  (* | Lambda (_, _, a) *)
  (* | IfExp (_, _, _, a) *)
  (* | Dict (_, _, a) *)
  (* | Set (..) *)
  (* | ListComp (_, _, a) *)
  (* | SetComp (..) *)
  (* | DictComp (..) *)
  (* | GeneratorExp (_, _, a) *)
  (* | Await (..) *)
  (* | Yield (_, a) *)
  (* | YieldFrom (..) *)
  | Compare (_, _, _, a)
  (* | Call (_, _, _, _, _, a) *)
  | Num (_, a)
  | Str (_, a)
  | FormattedValue (_, _, _, a)
  (* | JoinedStr (..) *)
  (* | Bytes (..) *)
  (* | NameConstant (..) *)
  (* | Ellipsis (..) *)
  (* | Constant (..) *)
  | Attribute (_, _, _, a)
  (* | Subscript (_, _, _, a) *)
  (* | Starred (..) *)
  | Name (_, _, a)
  | List (_, _, a)
  | Tuple (_, _, a)
    -> a
(*and annot_of_excepthandler = function
  | ExceptHandler (_, _, _, a) -> a *)
and annot_of_arg (_, _, a) = a


(** *****************)
(** context_of_expr *)
(** *****************)
let context_of_expr = function
  | Attribute (_, _, ctx, _)
  (* | Subscript (_, _, ctx, _) *)
  (* | Starred (..) *)
  | Name (_, ctx, _)
  | List (_, ctx, _)
  | Tuple (_, ctx, _)
      -> Some ctx
  | _ -> None
 *)


(** ************)
(** Annot, Pos *)
(** ************)
(* Lexing.poition = { pos_fname : string, pos_{lnum,bol,cnum} : int } *)
module type Annot = sig
  type t
  val of_pos : Lexing.position -> t
  val to_pos : t -> Lexing.position
end

module Pos : Annot = struct
  type t = Lexing.position
  let of_pos pos = pos
  let to_pos pos = pos
end
