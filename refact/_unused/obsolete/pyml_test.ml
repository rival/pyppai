(*
   to compile this file:
     ocamlfind ocamlc -o pyml_test -linkpkg -package pyml pyml_test.ml; ./pyml_test
 *)


(**********)
(* Basics *)
(**********)

(* print_string *)
let () = print_string "\n====== python output ends, ocaml output starts =====\n"

(* Printf.printf *)
open Printf
let () =
  let () = printf "test: List.iter, Printf.printf: " in
  let () = List.iter (printf "%d ") [1;2;3;4] in
  printf "\n"

	 
(********)
(* Pyml *)
(********)

(* init pyml *)
let () = Py.initialize ~version:3 ()  (* use python 3 *)

(* Py.Run.simple_string *)
let () =
  let _   = Py.Run.simple_string "print('===== python output starts =====')" in
  let ret = Py.Run.simple_string "print('test: Py.Run.simple_string: hello')" in
  printf "test: Py.Run.simple_string: ret_val = %b\n" ret

(* Py.Run.eval for expr*)
let () =
  let str = "18+42" in
  let res_python = Py.Run.eval str in
  let res_ocaml  = Py.Int.to_int res_python in
  printf "test: Py.Run.eval (for expr): %s = %d\n" str res_ocaml

(* Py.Run.eval for module *)
let () = printf "test: Py.Run.eval (for module)\n"

(* [1] print python version *)
let _ = Py.Run.eval ~start:Py.File "
import sys
print ('test: Py.Run.eval: python version =', sys.version.replace('\\n',''))"

(* [2] pass ocaml value to python *)
let m = Py.Import.add_module "ocaml"
let () = Py.Module.set m "example_value" (Py.List.of_list_map Py.Int.of_int [1;2;3])
let _ = Py.Run.eval ~start:Py.File "
from ocaml import example_value
print('test: Py.Run.eval:', example_value)"

(* Py.Run.interactive *)		    
(* let _ = Py.Run.interactive () *)


(********************************)
(* Obtain python AST using pyml *)
(********************************)

let () = print_string "test: obtain Python AST using Pyml\n"

(* helper funcs *)
let option_get = function
  | Some v -> v
  | None   -> failwith "option_get: None"
let println_string (str : string) = print_string str; print_newline ()
					  
let pyobj_attr (pyobj) (str) : Py.Object.t = option_get (Py.Object.get_attr_string pyobj str)
let pyobj_str  (pyobj) : string =
  let _pyobj_type    (pyobj) : Py.Object.t = Py.Object.get_type pyobj in
  let _pyobj_content (pyobj) : Py.Object.t = 
    if Py.Object.has_attr_string pyobj "__dict__"
    then pyobj_attr pyobj "__dict__"
    else pyobj in
  let _pyobj_str (pyobj) : string = Py.Object.to_string pyobj in
  (pyobj |> _pyobj_content |> _pyobj_str) ^ " : " ^ (pyobj |> _pyobj_type |> _pyobj_str)

let print_pyobj   pyobj = pyobj |> pyobj_str |> print_string
let println_pyobj pyobj = pyobj |> pyobj_str |> println_string

(* main func *)
let get_python_ast (python_prog:string) : Py.Object.t =
  let m = Py.Import.add_module "ocaml" in
  let retval : Py.Object.t ref = ref Py.none in
  let callback (args : Py.Object.t array) : Py.Object.t = retval := Array.get args 0; Py.none in
  let () = Py.Module.set_function m "callback" callback in
  let python_prog_wrap = "
import ast
from ocaml import callback
e = '" ^ python_prog ^ "'
callback(ast.parse(e))" in
  let _ = Py.Run.simple_string python_prog_wrap in
  !retval

(* test *)
let python_prog = "if x == y: y += 4"
let prog = get_python_ast python_prog
let body = pyobj_attr prog "body"
let () = "  prog = " ^ (pyobj_str prog) |> println_string 
let () = "  body = " ^ (pyobj_str body) |> println_string
