(**
 * To compile this file:
 *   ocamlc -o ir ir.ml; ./ir
 *)


(** ************)
(** Defn of IR *)
(** ************)

type idtf = string
type number =
  | Int   of int
  | Float of float

type uop =
  | Not
type bop =
  | And | Or
  | Add | Sub | Mult | Div
type cop =
  | Eq | NotEq | Lt | LtE | Gt | GtE

(* expr *)
type expr =
  | True | False
  | Num  of number (* n *)
  | Name of idtf (* id *)

  | UOp  of uop (* op *) * expr (* v *)
  | BOp  of bop (* op *) * expr (* el *) * expr (* er *)
  | Comp of cop (* op *) * expr (* el *) * expr (* er *)

  | Str    of string (* s *)
  | StrFmt of string (* s *) * expr (* v *)

(* dist *)
type dist =
  | Normal  | Exponential
  | Poisson | Bernoulli

(* acmd *)
type acmd  =
  | Assume   of expr (* cond *)
  | Assn     of idtf (* trgt *) * expr (* v *)
  | AssnCall of idtf (* trgt *) * expr (* func *) * expr list (* args *)
  | Sample   of idtf (* trgt *) * expr (* name *) * dist (* d *) * expr list (* args *)
  | Observe  of idtf (* trgt *) * expr (* name *) * dist (* d *) * expr list (* args *) * expr (* obsv *)

(* withitem *)
type withitem = expr * expr option
  
(* prog *)
type prog =
  | Atomic of acmd (* ac *)
  | Seq    of prog (* p1 *) * prog (* p2 *)
  | If     of expr (* cond *) * prog (* body *) * prog (* orelse *)
  | While  of expr (* cond *) * prog (* body *)
  | With   of withitem list (* items *) * prog (* body *)
  | Break
  | Continue
