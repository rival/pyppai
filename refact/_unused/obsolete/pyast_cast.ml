(**
 * To compile this file:
 *   ocamlfind ocamlc -o pyast_cast -linkpkg -package pyml pyast.ml pyast_cast.ml; ./pyast_cast
 *)

open Pyast


(** **************)
(** helper funcs *) (* related to Py.Object *)
(** **************)
let opt_get = function
  | Some v -> v
  | None   -> failwith "opt_get: None"

(* pyobj_... *)
let pyobj_attr (pyobj) (str) : Py.Object.t =
  opt_get (Py.Object.get_attr_string pyobj str)

let pyobj_classname (pyobj) : string =
  Py.String.to_string (pyobj_attr (pyobj_attr pyobj "__class__") "__name__")

let pyobj_str (pyobj) : string =
  let _pyobj_type    (pyobj) : Py.Object.t = Py.Object.get_type pyobj in
  let _pyobj_content (pyobj) : Py.Object.t =
    if Py.Object.has_attr_string pyobj "__dict__"
    then pyobj_attr pyobj "__dict__"
    else pyobj in
  let _pyobj_str (pyobj) : string = Py.Object.to_string pyobj in
  (pyobj |> _pyobj_content |> _pyobj_str) ^ " : " ^ (pyobj |> _pyobj_type |> _pyobj_str)

(* print{,ln}_pyobj *)
let println_string (str : string) = print_string str; print_newline ()
let print_pyobj   pyobj = pyobj |> pyobj_str |> print_string
let println_pyobj pyobj = pyobj |> pyobj_str |> println_string


(** *****************)
(** to_{primitives} *)
(** *****************)
let to_string : Py.Object.t -> string = Py.String.to_string
let to_int    : Py.Object.t -> int    = Py.Int.to_int
let to_list (f : Py.Object.t -> 'a) (pyobj) : 'a list   = Py.List.to_list_map f pyobj
let to_opt  (f : Py.Object.t -> 'a) (pyobj) : 'a option =
  match pyobj_classname pyobj with
  | "NoneType" -> None
  | _          -> Some (f pyobj)


(** ************)
(** to_{pyast} *)
(** ************)
let to_identifier : Py.Object.t -> identifier = to_string

let to_number pyobj : number =
  match pyobj_classname pyobj with
  | "int"   -> Int   (Py.Int  .to_int   pyobj)
  | "float" -> Float (Py.Float.to_float pyobj)
  | _ -> failwith ("to_number: unreachable [" ^ (pyobj_str pyobj) ^ "]")

let rec to_modl pyobj : 'a option modl =
  let a = None in
  match pyobj_classname pyobj with
  | "Module" -> Module (pyobj_attr pyobj "body" |> to_list to_stmt, a)
  | _ -> failwith ("to_modl: unreachable [" ^ (pyobj_str pyobj) ^ "]")

and to_stmt pyobj : 'a option stmt =
  let a = None in
  match pyobj_classname pyobj with
  | "FunctionDef" -> FunctionDef
                       (pyobj_attr pyobj "name"           |> to_identifier,
                        pyobj_attr pyobj "args"           |> to_arguments,
                        pyobj_attr pyobj "body"           |> to_list to_stmt,
                        pyobj_attr pyobj "decorator_list" |> to_list to_expr,
                        pyobj_attr pyobj "returns"        |> to_opt  to_expr, a)

  | "Return" -> Return (pyobj_attr pyobj "value"   |> to_opt  to_expr, a)
  | "Assign" -> Assign (pyobj_attr pyobj "targets" |> to_list to_expr,
                        pyobj_attr pyobj "value"   |> to_expr, a)

  | "For"  -> For  (pyobj_attr pyobj "target" |> to_expr,
                    pyobj_attr pyobj "iter"   |> to_expr,
                    pyobj_attr pyobj "body"   |> to_list to_stmt,
                    pyobj_attr pyobj "orelse" |> to_list to_stmt, a)
  | "If"   -> If   (pyobj_attr pyobj "test"   |> to_expr,
                    pyobj_attr pyobj "body"   |> to_list to_stmt,
                    pyobj_attr pyobj "orelse" |> to_list to_stmt, a)
  | "With" -> With (pyobj_attr pyobj "items"  |> to_list to_withitem,
                    pyobj_attr pyobj "body"   |> to_list to_stmt, a)

  | "Expr" -> Expr (pyobj_attr pyobj "value"  |> to_expr, a)
  | "Pass"     -> Pass     (a)
  | "Break"    -> Break    (a)
  | "Continue" -> Continue (a)

  | _ -> failwith ("to_stmt: unreachable [" ^ (pyobj_str pyobj) ^ "]")

and to_expr pyobj : 'a option expr =
  let a = None in
  match pyobj_classname pyobj with
  | "BoolOp" -> BoolOp  (pyobj_attr pyobj "op"      |> to_boolop,
                         pyobj_attr pyobj "values"  |> to_list to_expr, a)
  | "BinOp"  -> BinOp   (pyobj_attr pyobj "left"    |> to_expr,
                         pyobj_attr pyobj "op"      |> to_operator,
                         pyobj_attr pyobj "right"   |> to_expr, a)
  | "UnaryOp"-> UnaryOp (pyobj_attr pyobj "op"      |> to_unaryop,
                         pyobj_attr pyobj "operand" |> to_expr, a)

  | "Compare"-> Compare   (pyobj_attr pyobj "left"        |> to_expr,
                           pyobj_attr pyobj "ops"         |> to_list to_cmpop,
                           pyobj_attr pyobj "comparators" |> to_list to_expr, a)
  | "Num"    -> Num       (pyobj_attr pyobj "n"           |> to_number, a)
  | "Str"    -> Str       (pyobj_attr pyobj "s"           |> to_string, a)
  (* | "FormattedValue" -> FormattedValue
                          (pyobj_attr pyobj "value"       |> to_expr,
                           pyobj_attr pyobj "conversion"  |> to_opt to_int,
                           pyobj_attr pyobj "format_spec" |> to_opt to_expr, a) *)

  | "Attribute" -> Attribute
                     (pyobj_attr pyobj "value" |> to_expr,
                      pyobj_attr pyobj "attr"  |> to_identifier,
                      pyobj_attr pyobj "ctx"   |> to_expr_context, a)
  | "Name"  -> Name  (pyobj_attr pyobj "id"    |> to_identifier,
                      pyobj_attr pyobj "ctx"   |> to_expr_context, a)
  | "List"  -> List  (pyobj_attr pyobj "elts"  |> to_list to_expr,
                      pyobj_attr pyobj "ctx"   |> to_expr_context, a)
  | "Tuple" -> Tuple (pyobj_attr pyobj "id"    |> to_list to_expr,
                      pyobj_attr pyobj "ctx"   |> to_expr_context, a)

  | _ -> failwith ("to_expr: unreachable [" ^ (pyobj_str pyobj) ^ "]")

and to_expr_context pyobj : expr_context =
  match pyobj_classname pyobj with
  | "Load"  -> Load
  | "Store" -> Store
  | "Del"   -> Del
  | "Param" -> Param
  | _  -> failwith ("to_expr_context: unreachable [" ^ (pyobj_str pyobj) ^ "]")

and to_boolop pyobj : boolop =
  match pyobj_classname pyobj with
  | "And" -> And
  | "Or"  -> Or
  | _  -> failwith ("to_bool: unreachable [" ^ (pyobj_str pyobj) ^ "]")

and to_operator pyobj : operator =
  match pyobj_classname pyobj with
  | "Add"         -> Add
  | "Sub"         -> Sub
  | "Mult"        -> Mult
  | "MatMult"     -> MatMult
  | "Div"         -> Div
  | "Mod"         -> Mod
  | "Pow"         -> Pow
  | "LShift"      -> LShift
  | "RShift"      -> RShift
  | "BitOr"       -> BitOr
  | "BitXor"      -> BitXor
  | "BitAnd"      -> BitAnd
  | "FloorDiv"    -> FloorDiv
  | _  -> failwith ("to_operator: unreachable [" ^ (pyobj_str pyobj) ^ "]")

and to_unaryop pyobj : unaryop =
  match pyobj_classname pyobj with
  | "Invert"      -> Invert
  | "Not"         -> Not
  | "UAdd"        -> UAdd
  | "USub"        -> USub
  | _  -> failwith ("to_unaryop: unreachable [" ^ (pyobj_str pyobj) ^ "]")

and to_cmpop pyobj : cmpop =
  match pyobj_classname pyobj with
  | "Eq"          -> Eq
  | "NotEq"       -> NotEq
  | "Lt"          -> Lt
  | "LtE"         -> LtE
  | "Gt"          -> Gt
  | "GtE"         -> GtE
  | "Is"          -> Is
  | "IsNot"       -> IsNot
  | "In"          -> In
  | "NotIn"       -> NotIn
  | _  -> failwith ("to_cmpop: unreachable [" ^ (pyobj_str pyobj) ^ "]")

and to_arguments pyobj : 'a option arguments =
  (pyobj_attr pyobj "args"        |> to_list to_expr,
   pyobj_attr pyobj "vararg"      |> to_opt  to_arg,
   pyobj_attr pyobj "kwonlyargs"  |> to_list to_arg,
   pyobj_attr pyobj "kw_defaults" |> to_list to_expr,
   pyobj_attr pyobj "kwargs"      |> to_opt  to_arg,
   pyobj_attr pyobj "defaults"    |> to_list to_expr)

and to_arg pyobj : 'a option arg =
  let a = None in
  (pyobj_attr pyobj "arg"        |> to_identifier,
   pyobj_attr pyobj "annotation" |> to_opt to_expr, a)

and to_withitem pyobj : 'a option withitem =
  (pyobj_attr pyobj "context_expr"  |> to_expr,
   pyobj_attr pyobj "optional_vars" |> to_opt to_expr)
