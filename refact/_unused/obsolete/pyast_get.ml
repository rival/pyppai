(**
 * To compile this file:
     ocamlfind ocamlc -o pyast_get -linkpkg -package pyml \
     pyast.ml pyast_cast.ml pyast_get.ml; ./pyast_get
 *)


(** ***************)
(** get_pyobj_ast *)
(** ***************)
let get_pyobj_ast (py_prog:string) : Py.Object.t =
  let m = Py.Import.add_module "ocaml" in
  let retval : Py.Object.t ref = ref Py.none in
  let callback (args : Py.Object.t array) : Py.Object.t = retval := Array.get args 0; Py.none in
  let () = Py.Module.set_function m "callback" callback in
  let py_prog_wrap = "
import ast
from ocaml import callback
e = \"\"\"
" ^ py_prog ^ "
\"\"\"
callback(ast.parse(e))" in
  let _ = Py.Run.simple_string py_prog_wrap in
  !retval

    
(** ***********)
(** get_pyast *) (* main func *)
(** ***********)
let get_pyast_modl (py_prog:string) : 'a option Pyast.modl =
  get_pyobj_ast py_prog |> Pyast_cast.to_modl

		  
(** ***********)
(** init pyml *) (* must be called before using Pyml package *)
(** ***********)
let init () = Py.initialize ~version:3 ()
