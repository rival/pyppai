(**
 * To compile this file:
 *   ocamlc -o static_analysis ir.ml util.ml static_analysis.ml; ./static_analysis
 *)


open Ir
open Util

(* sig *)
module type ABST_DOMAIN =
  sig
    type t
    val eval : acmd -> t -> t
    val bot : t
    val join : t -> t -> t
    val widen : t -> t -> t
    val leq : t -> t -> bool
  end

module type ANALYSIS =
  functor (State : ABST_DOMAIN) ->
    sig
      val eval : prog -> State.t -> (State.t * State.t * State.t)
    end

(* impl *)
module MakeAnalysis : ANALYSIS = 
  functor (AD : ABST_DOMAIN) ->
    struct
      let rec wfix f st = 
        let st_new = f st in
        if (AD.leq st_new st) then st
        else wfix f (AD.widen st st_new)

      let rec eval p st = 
        match p with 
        | Atomic(ac) -> 
            (AD.eval ac st, AD.bot, AD.bot) 
        | Seq(p1, p2) ->
            let (st1, st1c, st1b) = eval p1 st in 
            let (st2, st2c, st2b) = eval p2 st1 in 
            (st2, AD.join st1c st2c, AD.join st1b st2b) 
        | If(e, p_t, p_f) -> 
            let (st_t, stc_t, stb_t) = eval p_t (AD.eval (Assume(e)) st) in
            let (st_f, stc_f, stb_f) = eval p_f (AD.eval (Assume(UOp(Not,e))) st) in
            (AD.join st_t st_f, AD.join stc_t stc_f, AD.join stb_t stb_f) 
        | While(e, p_b) ->
            let f_iter st0 =   
              let (st1, st1c, _) = eval p_b (AD.eval (Assume(e)) st0) in
              AD.join st (AD.join st1 st1c) in 
            let st_fix = wfix f_iter AD.bot in
            let (_, _, st_b) = eval p_b st_fix in
            let st_filtered = AD.eval (Assume(UOp(Not, e))) st_fix in
            (AD.join st_b st_filtered, AD.bot, AD.bot)
        | With(item_l, p_b) ->
            assert false 
        | Break ->
            (AD.bot, AD.bot, st) 
        | Continue ->
            (AD.bot, st, AD.bot)
    end  

module SS = StringSet

(* string analysis *)
module MayStringDomain : ABST_DOMAIN =
  struct
    type t = SS.t
    let rec eval ac s = 
      match ac with
      | Assume _ | Assn _ | AssnCall _ | Observe _ -> s
      | Sample(_, Str(str), _, _)-> SS.add str s
      | Sample _ -> s
    let bot = SS.empty
    let join = SS.union
    let widen = SS.union 
    let leq = SS.subset 
  end

module MustStringDomain : ABST_DOMAIN =
  struct
    type t = Top | Normal of SS.t
    let rec eval ac d =
      match ac with
      | Assume _ | Assn _ | AssnCall _ | Observe _ -> d
      | Sample(_, Str(str), _, _) ->
        (match d with
        | Top -> Top
        | Normal s -> Normal (SS.add str s))
      | Sample _ -> d
    let bot = Top
    let join d1 d2 =
      match (d1, d2) with
        | (_, Top) -> d1
        | (Top, _) -> d2
        | (Normal s1, Normal s2) -> Normal (SS.inter s1 s2)
    let widen = join
    let leq d1 d2 =
      match (d1, d2) with
      | (_, Top) -> true
      | (Top, _) -> false
      | (Normal s1, Normal s2) -> SS.subset s2 s1
  end


module MayStringAnalysis = MakeAnalysis(MayStringDomain)
module MustStringAnalysis = MakeAnalysis(MustStringDomain)

let prog = 
  Seq (Atomic (Sample ("a", Str "x", Normal, [])),
       If (True,
           Atomic (Sample ("b", Str "y", Normal, [])),
           Atomic (Sample ("c", Str "z", Normal, []))))

