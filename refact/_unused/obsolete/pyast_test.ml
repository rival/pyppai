(**
 * To compile this file:
     ocamlfind ocamlc -o pyast_test -linkpkg -package pyml \
     pyast.ml pyast_dump.ml pyast_cast.ml pyast_get.ml pyast_test.ml; ./pyast_test
 *)

open Format
open Pyast


(*********)
(** init *)
(*********)
let _ = printf("\n")
let () = Pyast_get.init ()

       
(***********************)		  
(** test Pyast{,_dump} *)
(***********************)
let _ = printf("test: Pyast: \n")
let expr = BinOp(Num(Int 1, None),
                 Add,
                 Num(Int 2, None),
                 None)
let modl = Module ([Expr(expr, None)], None)
let _ = Pyast_dump.print_modl modl; printf("\n")


(*******************)		  
(** test Pyast_get *)
(*******************)		  
let py_prog = "3+4"

let _ = printf("test: Pyast_get: \n")
let modl = Pyast_get.get_pyast_modl py_prog
let _ = Pyast_dump.print_modl modl; printf("\n")
