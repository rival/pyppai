(**
 * To compile this file:
 *   ocamlfind ocamlc -o ir_cast pyast.ml ir.ml ir_cast.ml; ./ir_cast
 *)

open Ir


(** **************)
(** helper funcs *)
(** **************)
let gen_dummy_lvar_pyast () = Pyast.Name ("@DUMMY", Pyast.Store, "a")

let rec to_withitem (items : 'a Pyast.withitem list) : withitem list =
  match items with
  | (context, vars) :: tail ->
     let hd = (context |> to_expr,
               (match vars with 
                | None -> None                                                       
                | Some expr -> Some (expr |> to_expr)
                | _ -> failwith "to_withitem"))
     in
     hd :: (tail |> to_withitem)
  | [] -> failwith "to_withitem"

let rec to_expr_list (elist: 'a Pyast.expr list) : expr list =
  match elist with
  | hd :: tl -> (hd |> to_expr) :: (tl |> to_expr_list) 
  | [] -> failwith "to_expr_list"

(* maybe better to move this to pyast-related .ml *)


(** *****************)
(** to_{primitives} *)
(** *****************)
let to_idtf (id : Pyast.identifier) : idtf = id
let to_number (n : Pyast.number) : number =
  match n with
  | Pyast.Int   (i) -> Int   (i)
  | Pyast.Float (f) -> Float (f)

let to_uop (op : Pyast.unaryop) : uop =
  match op with
  | Pyast.Not -> Not
  | _ -> failwith "to_uop: unreachable"
let to_bop_bool (op : Pyast.boolop) : bop =
  match op with
  | Pyast.And -> And
  | Pyast.Or  -> Or
let to_bop_other (op : Pyast.operator) : bop =
  match op with
  | Pyast.Add  -> Add
  | Pyast.Sub  -> Sub
  | Pyast.Mult -> Mult
  | Pyast.Div  -> Div
  | _ -> failwith "to_bop_other"
let to_cop (op : Pyast.cmpop) : cop =
  match op with
  | Pyast.Eq    -> Eq
  | Pyast.NotEq -> NotEq
  | Pyast.Lt    -> Lt
  | Pyast.LtE   -> LtE
  | Pyast.Gt    -> Gt
  | Pyast.GtE   -> GtE
  | _ -> failwith "to_cop"
  
let to_dist (dstr : string) : dist =
  match dstr with
  | "Normal"      -> Normal
  | "Exponential" -> Exponential
  | "Poisson"     -> Poisson
  | "Bernoulli"   -> Bernoulli
  | _ -> failwith "to_dist"

(** *********)
(** to_{ir} *)
(** *********)
let rec to_expr (e : 'a Pyast.expr) : expr =
  match e with
  | Pyast.BoolOp (op, values, _) ->
     let () = assert (List.length values = 2) in
     let v1 = values |> List.hd in
     let v2 = values |> List.tl |> List.hd in
     BOp (op |> to_bop_bool,
          v1 |> to_expr,
          v2 |> to_expr)

  | Pyast.BinOp (left, op, right, _) ->
     BOp (op    |> to_bop_other,
          left  |> to_expr,
          right |> to_expr)

  | Pyast.UnaryOp (op, operand, _) ->
     UOp (op      |> to_uop,
          operand |> to_expr)
     
  | Pyast.Compare (left, ops, comparators, _) ->
     let () = assert (List.length ops = 1 && List.length comparators = 1) in
     let op    = List.hd ops in
     let right = List.hd comparators in
     Comp (op    |> to_cop,
           left  |> to_expr,
           right |> to_expr)
     
  | Pyast.Call (func, args, keywords, _) -> failwith "to_expr: unreachable"
  | Pyast.Num (n, _) -> Num (n |> to_number)
  | Pyast.Str (s, _) -> Str (s)

  | Pyast.Attribute (value, attr, ctx, _) -> failwith "to_expr: unreachable"
  | Pyast.Name  (id,   ctx, _) -> Name (id |> to_idtf)
  | Pyast.List  (elts, ctx, _) -> failwith "to_expr: unreachable"
  | Pyast.Tuple (elts, ctx, _) -> failwith "to_expr: unreachable"

let rec to_acmd (trgt : 'a Pyast.expr) (value : 'a Pyast.expr) : acmd =
  match trgt with
  | Pyast.Name (trgtid, _, _) ->
     (match value with
      | Pyast.Call (Pyast.Attribute (Pyast.Name ("pyro", _, _), "sample", _, _), 
                    (Pyast.Str (id, _) :: (Pyast.Call (Pyast.Name (dist, _, _), dist_args, _, _) :: [])), 
                    keywords, _)->
         (match keywords with
           | ((Some "obs"), obs_val) :: tl ->       
             Observe (trgtid |> to_idtf,
                      Name (id |> to_idtf), 
                      dist |> to_dist, 
                      dist_args |> to_expr_list, 
                      obs_val |> to_expr)
           | [] -> 
             Sample  (trgtid |> to_idtf, 
                      Name (id |> to_idtf), 
                      dist |> to_dist, 
                      dist_args |> to_expr_list))
      | Pyast.Call (func, args, _, _) ->
         AssnCall (trgtid |> to_idtf, 
                   func |> to_expr, 
                   args |> to_expr_list)        
      | _ -> 
         Assn (trgtid |> to_idtf,
               value  |> to_expr))

  | _ -> failwith "to_acmd: unreachable"

let rec to_prog_s (s : 'a Pyast.stmt) : prog =
  match s with
  | Pyast.Assign (targets, value, _) ->
     let () = assert (List.length targets = 1) in
     let trgt = List.hd targets in
     Atomic (to_acmd trgt value)
     
  | Pyast.While (test, body, orelse, _) ->
     let () = assert (orelse = []) in
     While (test |> to_expr,
            body |> to_prog_l)

  | Pyast.If (test, body, orelse, _) ->
     If (test   |> to_expr,
         body   |> to_prog_l,
         orelse |> to_prog_l)
        
  | Pyast.With (items, body, _) ->
     With (items |> to_withitem (* TODO *),
           body  |> to_prog_l)
          
  | Pyast.Expr (value, a) ->
     Atomic ( to_acmd (gen_dummy_lvar_pyast ()) value) 

  | Pyast.Break    (_) -> Break
  | Pyast.Continue (_) -> Continue
  | _ -> failwith "to_prog_s"

and to_prog_l (slist : 'a Pyast.stmt list) : prog =
  match slist with
  | hd :: tl -> Seq (hd |> to_prog_s,
                     tl |> to_prog_l)
  | [] -> failwith "to_prog_l"
