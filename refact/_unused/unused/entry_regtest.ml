(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** entry_regtest.ml: entry point for regression testing *)
open Ir_sig
open Lib
open Analysis_sig
open Main

module AF = Adom_fib

(* Definition of the tests selected to be run *)
type sel_tests =
  | Tsall
  | Tsnone
  | Tsonly of int

(* Helper functions for generating a predicate that
 * asserts the allocation of a list of random variables. *)
let gen_alloc_str l =
  List.map (fun x -> UOp(SampledStr, Name(x))) l
let gen_alloc_strfmt l =
  List.map (fun x -> UOp(SampledStrFmt, Name(x))) l

(* Helper functions for generating a predicate that
 * asserts the upper bound on the indices of the names
 * of random variables sharing the same strfmt. *)
let gen_lconst_strfmt sfmt e =
  let low = AF.dim_sample_low_make(AF.Must)(sfmt) in
  Comp(Eq, e, Name(low))
let gen_hconst_strfmt sfmt e =
  let high = AF.dim_sample_high_make(AF.Must)(sfmt) in
  Comp(Eq, e, Name(high))

(* Helper functions for creating a range and composing two functions *)
(* s -- e = [s,e) \cap \Z *)
let (--) s e =
  let rec create_range acc i =
    if (i < e) then create_range (i::acc) (i+1)
    else (List.rev acc) in
  create_range [] s

(*let (%>) f g = fun x -> (g (f x))*)

(**
 *
 * Non-relational tests
 *
 *)
let test0 =
  ({ aopts_default with
     ao_do_num  = Some AD_oct;
     ao_input   = AI_pyfile "./test/test0.py" },
   [Comp(Eq, Name("y"), BOp(Add, Name("x"), Num(Float(4.0))))],
   [])

let test1 =
  ({ aopts_default with
     ao_do_num  = Some AD_oct;
     ao_input   = AI_pyfile "./test/test1.py" },
   [Comp(GtE, BOp(Sub, Name("x"), Name("y")), Num(Float(1.0)));
    Comp(LtE, BOp(Sub, Name("x"), Name("y")), Num(Float(2.0)))],
   [])

let test2 =
  ({ aopts_default with
     ao_do_num  = Some AD_oct;
     ao_input   = AI_pyfile "./test/test2.py" },
   gen_alloc_str ["y"; "z"],
   gen_alloc_str ["x"])

let test3 =
  ({ aopts_default with
     ao_do_num  = Some AD_oct;
     ao_input   = AI_pyfile "./test/test3.py" },
   gen_alloc_str [],
   gen_alloc_str ["x"; "mean"])

let test4 =
  ({ aopts_default with
     ao_do_num   = Some AD_oct;
     ao_input    = AI_pyfile "./test/test4.py";
     ao_sim_assm = true },
   gen_lconst_strfmt "z_{}" (Num(Int(0)))
     :: gen_hconst_strfmt "z_{}" (Num(Int(20)))
     :: gen_alloc_str ["mu1"; "mu2"]
     @  gen_alloc_strfmt ["z_{}"],
   gen_alloc_strfmt ["data_{}"])

let test5 =
  ({ aopts_default with
     ao_do_num  = Some AD_oct;
     ao_input   = AI_pyfile "./test/test5.py";
     ao_sim_assm = true },
   gen_lconst_strfmt "z_{}" (Num(Int(0)))
     :: gen_hconst_strfmt "z_{}" (Num(Int(40)))
     :: gen_alloc_strfmt ["z_{}"],
   gen_alloc_strfmt ["x_{}"])

let test6 =
  ({ aopts_default with
     ao_do_num  = Some AD_oct;
     ao_input   = AI_pyfile "./test/test6.py" },
   gen_lconst_strfmt "z_{}" (Num(Int(0)))
     :: gen_hconst_strfmt "z_{}" (Num(Int(2)))
     :: gen_alloc_strfmt ["z_{}"],
   [])

let test7 =
  ({ aopts_default with
     ao_do_num  = Some AD_oct;
     ao_input   = AI_pyfile "./test/test7.py";
     ao_sim_assm = true },
   gen_lconst_strfmt "z_{}" (Num(Int(0)))
     :: gen_hconst_strfmt "z_{}" (Num(Int(30)))
     :: gen_alloc_strfmt ["z_{}"],
   gen_alloc_str ["p"; "scale"]
     @  gen_alloc_strfmt ["x_{}"])

let test8 =
  ({ aopts_default with
     ao_do_num  = Some AD_oct;
     ao_input   = AI_pyfile "./test/test8.py" },
   [Comp(Eq, Num(Int(8)), Name("x"))],
   [])

let test9 =
  ({ aopts_default with
     ao_do_num  = Some AD_oct;
     ao_input   = AI_pyfile "./test/test9.py" },
   [Comp(Eq, Name("y"), BOp(Add, Name("z"), Num(Int(8)))); Comp(Eq, Name("x"), Num(Int(8)))],
   [])

let test10 =
  ({ aopts_default with
     ao_do_num  = Some AD_oct;
     ao_input   = AI_pyfile "./test/test10.py" },
   [Comp(Eq, Name("x"), Num(Int(8)))],
   [])

let test11 =
  ({ aopts_default with
     ao_do_num  = Some AD_oct;
     ao_input   = AI_pyfile "./test/test11.py" },
   [Comp(GtE, Name("x"), Num(Float(2.0)))],
   [])

let test12 =
  ({ aopts_default with
     ao_do_num  = Some AD_oct;
     ao_input   = AI_pyfile "./test/test12.py" },
   [False],
   [])

(* debugging threshold *)
let test13 =
  ({ aopts_default with
     ao_do_num  = Some AD_oct;
     ao_input   = AI_pyfile "./test/test13.py";
     ao_wid_thr = true },
   [False],
   [])

(* distribution type *)
let test14 =
  ({ aopts_default with
     ao_do_num  = Some AD_oct;
     ao_input   = AI_pyfile "./test/test14.py" },
   [],
   [])

let test15 =
  ({ aopts_default with
     ao_do_num  = Some AD_oct;
     ao_input   = AI_pyfile "./test/broadcast0.py" },
   [],
   [])

let test16 =
  ({ aopts_default with
     ao_do_num  = Some AD_oct;
     ao_input   = AI_pyfile "./test/broadcast1.py" },
   [],
   [])

let test17 =
  ({ aopts_default with
     ao_do_num  = Some AD_oct;
     ao_input   = AI_pyfile "./test/broadcast2.py" },
   [],
   [])

let test18 =
  ({ aopts_default with
     ao_do_num  = Some AD_oct;
     ao_input   = AI_pyfile "./test/broadcast3.py" },
   [],
   [])

let test19 =
  ({ aopts_default with
     ao_do_num  = Some AD_oct;
     ao_input   = AI_pyfile "./test/broadcast4.py" },
   [],
   [])

let test20 =
  ({ aopts_default with
     ao_do_num  = Some AD_oct;
     ao_input   = AI_pyfile "./test/broadcast5.py" },
   [],
   [])

let test21 =
  ({ aopts_default with
     ao_do_num  = Some AD_oct;
     ao_input   = AI_pyfile "./test/pyro_test_suite/nr0.py" },
   [],
   [])

(* All non-relational test cases *)
let all_tests_nr =
  [ (* test0-7 check the basics of our analysis *)
    test0;
    test1;
    test2;
    test3;
    test4;
    test5;
    test6; (* xr: I added this test as a simpler, loop-less test4.py *)
    test7; (* hy: a variant of test5 where the for loop is converted to a while loop and the rool is unrolled once *)
    (* test8-12 mainly check the use of Apron by our analysis *)
    test8;
    test9;
    test10;
    test11;
    test12;
    test13;
    (* test14 check the distribution type *)
    test14;
    test15;
    test16;
    test17;
    test18;
    test19;
    test20;
    test21
  ]

(**
 *
 * Relational tests
 *
 *)

let create_test_case_r ?(ao_zone = false) (model_fname, guide_fname) =
  let model =
    { aopts_default with
      ao_do_num   = Some AD_oct;
      ao_input    = AI_pyfile model_fname;
      ao_sim_assm = true;
      ao_zone     = ao_zone } in
  let guide =
    { aopts_default with
      ao_do_num   = Some AD_oct;
      ao_input    = AI_pyfile guide_fname;
      ao_sim_assm = true;
      ao_zone     = ao_zone } in
  (model, guide)

let create_file_names_r n =
  let model_fname =
    "./test/pyro_test_suite/model" ^ (string_of_int n) ^ ".py" in
  let guide_fname =
    "./test/pyro_test_suite/guide" ^ (string_of_int n) ^ ".py" in
  (model_fname, guide_fname)

(* adhoc_test *)
let adhoc_test_oracles_r =
  [ (* Bayesian regression from pyro tutorial *)
    TOR_fail;
    TOR_succeed;
    (* VAE example from pyro tutorial *)
    TOR_succeed;
    (* SSVAE example from pyro tutorial *)
    TOR_succeed;
    TOR_succeed;
    (* AIR example from pyro tutorial *)
    TOR_succeed;
    (* CSIS example from pyro tutorial *)
    TOR_succeed;
    (* SGDEF example from pyro tutorial *)
    TOR_succeed;
    (* LDA example from pyro tutorial *)
    TOR_fail;
    TOR_fail;
    TOR_succeed;
    (* DMM example from pyro tutorial *)
    TOR_succeed;
    (* from pyro test suite *)
    TOR_succeed;
    TOR_succeed;
  ]

let adhoc_test_file_names_r =
  [ (* Bayesian regression from pyro tutorial *)
    ("./test/pyro_example/br_model0.py", "./test/pyro_example/br_guide0.py");
    ("./test/pyro_example/br_model1.py", "./test/pyro_example/br_guide1.py");
    (* VAE example from pyro tutorial *)
    ("./test/pyro_example/vae_model.py", "./test/pyro_example/vae_guide.py");
    (* SSVAE example from pyro tutorial *)
    ("./test/pyro_example/ssvae_model0.py", "./test/pyro_example/ssvae_guide0.py");
    ("./test/pyro_example/ssvae_model1.py", "./test/pyro_example/ssvae_guide1.py");
    (* AIR example from pyro tutorial *)
    ("./test/pyro_example/air_model.py", "./test/pyro_example/air_guide.py");
    (* CSIS example from pyro tutorial: Note the opposite order of model & guide *)
    ("./test/pyro_example/csis_guide.py", "./test/pyro_example/csis_model.py");
    (* SGDEF example from pyro tutorial *)
    ("./test/pyro_example/sgdef_model.py", "./test/pyro_example/sgdef_guide.py");
    (* LDA example from pyro tutorial *)
    ("./test/pyro_example/lda_model.py", "./test/pyro_example/lda_guide0.py" (* original *));
    ("./test/pyro_example/lda_model.py", "./test/pyro_example/lda_guide1.py" (* edited *));
    ("./test/pyro_example/lda_model.py", "./test/pyro_example/lda_guide2.py" (* edited *));
    (* DMM example from pyro tutorial *)
    ("./test/pyro_example/dmm_model.py", "./test/pyro_example/dmm_guide.py");
    (* from pyro test suite *)
    ("./test/pyro_test_suite/model21_variant.py", "./test/pyro_test_suite/guide21_variant.py");
    ("./test/pyro_test_suite/model22_variant.py", "./test/pyro_test_suite/guide22_variant.py");
  ]

(* canonical_test *)
let canonical_test_oracles_r =
  [ (*  0 -  4 test_r *)
    TOR_succeed;
    TOR_succeed;
    TOR_error;
    TOR_fail;
    TOR_fail;
    (*  5 -  9 test_r *)
    TOR_error;
    TOR_succeed;
    TOR_succeed;
    TOR_error;
    TOR_succeed;
    (* 10 - 14 test_r *)
    TOR_succeed;
    TOR_succeed;
    (* TOR_succeed;
     * TOR_succeed;
     * TOR_succeed; *)
    (* 15 - 19 test_r *)
    (* TOR_succeed; (* hy: Tricky case. Original ok, but our version not. Can't be detected at the moment. *) *)
    TOR_succeed; (* hy: Tricky case. Original ok, but not clear why. *)
    TOR_succeed; (* hy: Tricky case. Original ok, but not clear why. *)
    TOR_fail; (* hy: Tricky case. Original not ok, but not clear why. *)
    TOR_fail; (* hy: Tricky case. Original not ok, but not clear why. *)
    (* 20 - 24 test_r *)
    TOR_error;
    TOR_succeed;
    TOR_succeed;
    TOR_succeed;
    TOR_succeed;
    (* 25 - 29 test_r *)
    TOR_succeed;
    TOR_error;
    TOR_succeed;
    TOR_error;
    TOR_succeed;
    (* 30 - 34 test_r *)
    TOR_succeed;
    TOR_error;
    TOR_succeed;
    TOR_succeed;
    TOR_succeed;
    (* 35 - 38 test_r *)
    TOR_error;
    TOR_succeed;
    TOR_succeed;
    TOR_succeed;
  ]

let canonical_test_file_names_r =
  (* wy: for now, we exclude 12 -- 16 because they need zone domain. *)
  (* List.map create_file_names_r (0 -- 38) *)
  List.map create_file_names_r (0 -- 12)
  @ List.map create_file_names_r (16 -- 39)

(* zone_test *)
let zone_test_oracles_r =
  [ (* 12 - 15 test_r *)
    TOR_succeed;
    TOR_succeed;
    TOR_succeed;
    TOR_succeed; (* hy: Tricky case. Original ok, but our version not. Can't be detected at the moment. *)]

let zone_test_file_names_r =
  List.map create_file_names_r (12 -- 16)

(* tzone_tests *)
let tzone_tests =
  let tzone_14 =
    let model =
      { aopts_default with
        ao_input    = AI_pyfile "./test/pyro_test_suite/model14.py";
        ao_do_num   = Some AD_oct;
        ao_zone     = true;
        ao_sim_assm = true } in
    let guide =
      { aopts_default with
        ao_input    = AI_pyfile "./test/pyro_test_suite/guide14.py";
        ao_do_num   = Some AD_oct;
        ao_zone     = true;
        ao_sim_assm = true } in
    model, guide, TOR_succeed in
  let tzone_14_variant =
    let model =
      { aopts_default with
        ao_input    = AI_pyfile "./test/pyro_test_suite/model14_variant.py";
        ao_do_num   = Some AD_oct;
        ao_zone     = true;
        ao_sim_assm = true } in
    let guide =
      { aopts_default with
        ao_input    = AI_pyfile "./test/pyro_test_suite/guide14_variant.py";
        ao_do_num   = Some AD_oct;
        ao_zone     = true;
        ao_sim_assm = true } in
    model, guide, TOR_succeed in
  let dmm_ij =
    let model =
      { aopts_default with
        ao_input    = AI_pyfile "./test/pyro_example/dmm_ij_model.py";
        ao_do_num   = Some AD_oct;
        ao_zone     = true;
        ao_sim_assm = true } in
    let guide =
      { aopts_default with
        ao_input    = AI_pyfile "./test/pyro_example/dmm_ij_guide.py";
        ao_do_num   = Some AD_oct;
        ao_zone     = true;
        ao_sim_assm = true } in
    model, guide, TOR_succeed in
  [ tzone_14; tzone_14_variant; dmm_ij;]

(* All relational test cases *)
let all_tests_r =
  let all_test_file_names_r =
    adhoc_test_file_names_r
    @ adhoc_test_file_names_r
    @ canonical_test_file_names_r in
  let all_test_oracles_r =
    adhoc_test_oracles_r
    @ adhoc_test_oracles_r
    @ canonical_test_oracles_r in
  List.map2
    (fun (m_fname, g_fname) o -> (m_fname, g_fname, o))
    (List.map (create_test_case_r ~ao_zone:false) all_test_file_names_r)
    all_test_oracles_r
  (*@ (List.map2
      (fun (m_fname, g_fname) o -> (m_fname, g_fname, o))
      (List.map (create_test_case_r ~ao_zone:true) zone_test_file_names_r)
      zone_test_oracles_r)*)
  (*@ tzone_tests*)

let get_prog_name aopts =
  match aopts.ao_input with
  | AI_pyfile s -> Printf.sprintf "file <%s>" s
  | AI_pystring s -> s

(* Debug (main switch; no debug nowhere when false) *)
let debug: bool ref = ref true
(* Timeout *)
let timeout: int ref = ref 1 (* optional timeout value; 0 is no timeout *)
(* Results and display *)
type result_core = RPass | RFail | RCrash | RTO
type result = result_core * string * float
let results_nrel: result IntMap.t ref = ref IntMap.empty
let results_rel:  result IntMap.t ref = ref IntMap.empty
let fname_nrel:   string IntMap.t ref = ref IntMap.empty
let fname_rel:    string IntMap.t ref = ref IntMap.empty
let display_results (): unit =
  let comp m =
    IntMap.fold
      (fun _ (v,_,_) (tot, p, f, c, t) ->
        match v with
        | RPass  -> tot + 1, p + 1, f, c, t
        | RFail  -> tot + 1, p, f + 1, c, t
        | RCrash -> tot + 1, p, f, c + 1, t
        | RTO    -> tot + 1, p, f, c, t + 1
      ) m (0, 0, 0, 0, 0) in
  let pp_res_i i ((res,out,time), fname) =
    let str =
      match res with
      | RPass  -> Printf.sprintf "%2d: Pass%s" i out
      | RFail  -> Printf.sprintf "%2d: Fail%s" i out
      | RCrash -> Printf.sprintf "%2d: Crash" i
      | RTO    -> Printf.sprintf "%2d: Timeout" i in
    let n = String.length str in
    let f = String.make (20 - n) ' ' in
    Printf.printf "%s%s\ttime(sec)=\t%f\t%s\n" str f time fname in
  Printf.printf "\n\nRESULTS(NR):\n";
  IntMap.iter pp_res_i (merge_imap !results_nrel !fname_nrel);
  Printf.printf "\n\nRESULTS(R):\n";
  IntMap.iter pp_res_i (merge_imap !results_rel  !fname_rel);
  let count, rpass, rfail, rcrash, rtimeout = comp !results_nrel in
  Printf.printf "\n\nRESULTS(NR):\n";
  Printf.printf " TESTS: %d\n PASS:  %d\n FAIL:  %d\n CRASH: %d\n TO:    %d\n"
    count rpass rfail rcrash rtimeout;
  let count, rpass, rfail, rcrash, rtimeout = comp !results_rel in
  Printf.printf "\n\nRESULTS(R):\n";
  Printf.printf " TESTS: %d\n PASS:  %d\n FAIL:  %d\n CRASH: %d\n TO:    %d\n"
    count rpass rfail rcrash rtimeout
(* Running jobs *)
type job =
  | J_nrel of int * analysis_opts * expr list * expr list
  | J_rel of int * analysis_opts * analysis_opts * test_oracle_r
let job_list: job list ref = ref [ ]
exception Timeout
let run_job: job -> unit = function
  | J_rel (i, aopts0, aopts1, oracle) ->
      let input0 = get_prog_name aopts0 in
      let input1 = get_prog_name aopts1 in
      Printf.printf "#### TEST(R) %d ####\n" i;
      Printf.printf "entry0: %s\n" input0;
      Printf.printf "entry1: %s\n" input1;
      Printf.printf "expected result: %s\n\n" (string_of_test_oracle_r oracle);
      let time = ref 0. in
      let result, str =
        try
          let time_st = Unix.gettimeofday() in
          let is_ok, str = start_r !debug aopts0 aopts1 oracle in
          let time_ed = Unix.gettimeofday() in
          time := time_ed -. time_st;
          if is_ok then Printf.printf "[RESULT OF TEST(R) %d] PASSED\n\n" i
          else Printf.printf "[RESULT OF TEST(R) %d] FAILED\n\n" i;
          if is_ok then RPass, str
          else RFail, str
        with exn ->
          Printf.printf "[RESULT OF TEST(R) %d] CRASHED\n" i;
          Printf.printf "%s\n\n" (Printexc.to_string exn);
          if exn = Timeout then RTO, ""
          else RCrash, "" in
      results_rel := IntMap.add i (result, str, !time) !results_rel;
      fname_rel   := IntMap.add i input1 !fname_rel
  | J_nrel (i, aopts, postconds, violations) ->
      let input = get_prog_name aopts in
      Printf.printf "#### TEST(NR) %d ####\n" i;
      Printf.printf "entry: %s\n\n" input;
      let time = ref 0. in
      let result =
        try
          let time_st = Unix.gettimeofday() in
          let is_ok = start_nr !debug aopts postconds violations in
          let time_ed = Unix.gettimeofday() in
          time := time_ed -. time_st;
          if is_ok then Printf.printf "[RESULT OF TEST(NR) %d] PASSED\n\n" i
          else Printf.printf "[RESULT OF TEST(NR) %d] FAILED\n\n" i;
          if is_ok then RPass else RFail
        with exn ->
          Printf.printf "[RESULT OF TEST(NR) %d] CRASHED\n" i;
          Printf.printf "%s\n\n" (Printexc.to_string exn);
          if exn = Timeout then RTO
          else RCrash in
      results_nrel := IntMap.add i (result, "", !time) !results_nrel;
      fname_nrel   := IntMap.add i input  !fname_nrel
let rec run_jobs_from_list (): unit =
  match !job_list with
  | [ ] -> display_results () (* display results *)
  | job :: others ->
      Printf.printf "Job: remaining %d\n" (List.length others);
      run_job job;
      job_list := others;
      run_jobs_from_list ()
let run_jobs () =
  (* if there is a timeout, sets handler and start clock *)
  if !timeout > 0 then
    begin
      let handle_to _ =
        ignore (Unix.alarm !timeout);
        raise Timeout in
      Sys.set_signal Sys.sigalrm (Sys.Signal_handle handle_to);
      ignore (Unix.alarm !timeout)
    end;
  run_jobs_from_list ()


(* Top level routine. Runs both non-relational and relational test cases. *)
type tests =
  | Tall          (* all tests should run *)
  | Trel of int   (* only relational test i *)
  | Tnrel of int  (* only non relational test i *)
let select_tests (isrel: bool) = function
  | Tall -> Tsall
  | Trel i -> if isrel then Tsonly i else Tsnone
  | Tnrel i -> if isrel then Tsnone else Tsonly i
let main () =
  (* Parsing of arguments *)
  let sel = ref Tall
  and ltests = ref false (* if true, only lists the tests *) in
  let no_debug () =
    debug := false ;
    Ai_make.debug := false ;
    Main.output := false in
  Arg.parse
    [ (* test selection *)
      "-r",  Arg.Int (fun i -> sel := Trel i) , "relational test";
      "-n",  Arg.Int (fun i -> sel := Tnrel i), "non relational test";
      (* lists the tests and exit *)
      "-l",  Arg.Set ltests, "print list of tests to perform";
      (* bench mode, turns out debug *)
      "-b",  Arg.Unit no_debug, "bench mode, no debug information";
      (* sets timeout *)
      "-to", Arg.Int (fun i -> timeout := i), "sets timeout value (s)";
    ]
    (fun s -> ()) "regression testing";
  (* Selects the tests to launch *)
  (*let sel_rel  = select_tests true  !sel*)
  (*and sel_nrel = select_tests false !sel in*)
  (* Definition of the jobs to do *)
  let jobs =
    let mk_rel i (a,b,c) = J_rel (i,a,b,c)
    and mk_nrel i (a,b,c) = J_nrel (i,a,b,c) in
    match !sel with
    | Tall -> List.mapi mk_rel all_tests_r @ List.mapi mk_nrel all_tests_nr
    | Trel i -> [ mk_rel i (List.nth all_tests_r i) ]
    | Tnrel i -> [ mk_nrel i (List.nth all_tests_nr i) ] in
  job_list := jobs;
  if !ltests then
    (* lists the series of tests to be performed, and exists *)
    let str_of_test t =
      match t.ao_input with
      | AI_pyfile s -> s
      | AI_pystring _ -> "<...>" in
    Printf.printf "Relational tests:\n";
    List.iteri (fun i (t,_,_) -> Printf.printf "%d: %s\n" i (str_of_test t))
      all_tests_r;
    Printf.printf "Non-relational tests:\n";
    List.iteri (fun i (t,_,_) -> Printf.printf "%d: %s\n" i (str_of_test t))
      all_tests_nr;
  else
    run_jobs ()

(* Start! *)
let _ = main ()
