(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** adom_string.mli: Abstract domains to describe sets of names *)
open Lib
open Adom_sig


module MayStringDomain:  ABST_DOMAIN_NB
module MustStringDomain: ABST_DOMAIN_NB
