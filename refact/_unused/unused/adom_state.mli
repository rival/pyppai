(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** adom_state.mli: abstract domain for state *)
open Adom_sig

module StateDomain: ABST_DOMAIN_NB
