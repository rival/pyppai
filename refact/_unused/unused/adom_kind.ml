(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** adom_kind.ml: abstract domain for distribution type *)
open Ir_sig
open Lib
open Adom_sig
open Adom_type

module KindDomain =
  struct
    (* t is the type for K^#:
     * (1) z denotes an underlying type (v,w,a,b),
     * (2) m denotes an element of K^#(z).
     *
     * map_ext is the type of abstract values:
     * (1) MBot means the empty set of states,
     * (2) MMap(f) denotes a map f from AB(= A(z) \cup B(z)) to dist_ext,
     *     and means the set of states where
     *     the random variable with name u (\in AB) has
     *     the distribution type f(u) (\in dist_ext).
     *
     * dist_ext is the type for dist \cup \{\top\}.
     *
     * Note:
     * - K^# needs to be a dependent type on z,
     *   but Ocaml doesn't support dependent types.
     *   So I implement it using a mutable variable z (as in Apron).
     * - Given {z=z; m=MMap f}, f(u) is not well-defined
     *   for all u \notin AB (= a(z) \cup b(z)).
     *   So we must apply f to u after checking if u \in AB.
     *)
    type dist_ext = DTop | DSingle of dist
    type map_ext = MBot | MMap of (dist_ext SM.t)
    type t = { z: TyDomainPriv.t;
               m: map_ext; }

    let buf_dist_ext (buf: Buffer.t) : dist_ext -> unit = function
      | DTop -> Printf.bprintf buf "T"
      | DSingle d -> Ir_util.buf_dist buf d
    let buf_t (buf: Buffer.t) (k: t) : unit =
      match k.m with
      | MBot -> Printf.bprintf buf "_|_"
      | MMap f ->
         let ab = SS.union (TyDomainPriv.get_a k.z)
                           (TyDomainPriv.get_b k.z) in
         Printf.bprintf buf "[";
         SS.iter (fun s ->
                  Printf.bprintf
                    buf "%s->%a, " s
                    buf_dist_ext (SM.find s f)) ab;
         Printf.bprintf buf "]"
    let to_string = buf_to_string buf_t
    let pp = buf_to_channel buf_t

    let bot (z: TyDomainPriv.t) : t = {z=z; m=MBot}
    let top (z: TyDomainPriv.t) : t =
      let ab = SS.union (TyDomainPriv.get_a z)
                        (TyDomainPriv.get_b z) in
      let add_top (u: SS.elt) (f: dist_ext SM.t) : dist_ext SM.t =
        (SM.add u DTop f) in
      let map_top = MMap (SS.fold add_top ab SM.empty) in
      {z=z; m=map_top}

    (* let eval = assert false *)

    let join (k1: t) (k2: t) : t =
      assert (k1.z = k2.z);
      match k1.m, k2.m with 
      | _, MBot -> k1
      | MBot, _ -> k2
      | MMap f1, MMap f2 ->
         let fun_merge u de1 de2 =
           match de1, de2 with
           | Some d1, Some d2 -> Some (if d1=d2 then d1 else DTop)
           | _ -> failwith "join: k1.m and k2.m have different domains" in
         let f_new = SM.merge fun_merge f1 f2 in
         let m_new = MMap f_new in
         {z=k1.z; m=m_new}

    (* let widen (k1: t) (k2: t) : t = 
      assert false *)

    let leq (k1: t) (k2: t) : bool =
      assert (k1.z = k2.z);
      match k1.m, k2.m with
      | MBot, _ -> true
      | _, MBot -> false
      | MMap f1, MMap f2 ->
         let ab = SS.union (TyDomainPriv.get_a k1.z)
                           (TyDomainPriv.get_b k1.z) in
         let leq_each (u: SS.elt) (b: bool) : bool =
           let de1 = SM.find u f1 in
           let de2 = SM.find u f2 in
           b && (de2 = DTop || de1 = de2)
         in SS.fold leq_each ab true

    let proj (k: t) (z_new: TyDomainPriv.t) : t =
      let m_new =
        match k.m with
        | MBot   -> MBot
        | MMap f ->
           let ab_new = SS.union (TyDomainPriv.get_a z_new)
                                 (TyDomainPriv.get_b z_new) in
           let f_new = SM.filter (fun u _ -> SS.mem u ab_new) f in
           MMap f_new
      in {z=z_new; m=m_new}

    let add_map (k: t) (u0: SS.elt) (d0: dist) : t =
      let ab = SS.union (TyDomainPriv.get_a k.z)
                        (TyDomainPriv.get_b k.z) in
      assert (SS.mem u0 ab);
      let m_new =
        match k.m with
        | MBot   -> MMap (SM.singleton u0 (DSingle d0))
        | MMap f -> MMap (SM.add       u0 (DSingle d0) f)
      in {z=k.z; m=m_new}
      
  end
