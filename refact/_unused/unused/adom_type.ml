(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** adom_type.ml: abstract domain for typing *)
open Ir_sig
open Lib
open Adom_sig

module TyDomainPriv =
  struct
    (* t is the type for abstract values:
     * A type (v, w, a, b) describes the set of states where
     *     the variables in v store real numbers,
     *     the variables in w store tensors,
     *     the string constructors in a are used as names of
     *         sampled random variables, and
     *     the strings in b are used as names of sampled random variables.
     *)
    let module_name = "TyDomainPriv"

    type t =
        { (* component "v": variables that store real numbers *)
          t_real:     SS.t ;
          (* component "w": variables that store tensors *)
          t_tens:     SS.t ;
          (* component "a": string constructors, name of sampled random vars *)
          t_sampidx:  SS.t ;
          (* component "b": name of sampled random vars *)
          t_sampvars: SS.t }

    let buf_t (buf: Buffer.t) (t: t): unit =
      Printf.bprintf buf "[%a, %a, %a, %a]"
        buf_ss t.t_real buf_ss t.t_tens buf_ss t.t_sampidx buf_ss t.t_sampvars
    let to_string = buf_to_string buf_t
    let pp = buf_to_channel buf_t

    type exp_ty = ET_tensor | ET_real | ET_bool | ET_unknown

    let top = (* check! *)
      { t_real     = SS.empty ;
        t_tens     = SS.empty ;
        t_sampidx  = SS.empty ;
        t_sampvars = SS.empty }
    let is_bot (_: t) = false

    (* Helper functions: extraction of type information *)
    let get_exp_ty (t: t): expr -> exp_ty =
      let rec aux = function
        | True | False ->
            ET_bool
        | UOp (Not, _) | UOp (SampledStr, _) | UOp (SampledStrFmt, _) ->
            ET_bool
        | Comp _ ->
            ET_bool
        | Num _ ->
            ET_real
        | Str _ | StrFmt _ ->
            ET_unknown
        | Name x ->
            if SS.mem x t.t_real then
              ET_real
            else if SS.mem x t.t_tens then
              ET_tensor
            else
              ET_unknown
        | BOp (bop, e1, e2) ->
            begin
              match bop with
              | And | Or -> ET_bool
              | Add | Sub | Mult | Div ->
                  let e1_ty = aux e1 in
                  let e2_ty = aux e2 in
                  if e1_ty = ET_tensor || e2_ty = ET_tensor then
                    ET_tensor
                  else if e1_ty = ET_real || e2_ty = ET_real then
                    ET_real
                  else if e1_ty = ET_bool && e2_ty = ET_bool then
                    ET_bool
                  else
                    ET_unknown
            end in
      aux
    let is_tensor_exp t e =
      get_exp_ty t e = ET_tensor
    let is_real_exp t e =
      get_exp_ty t e = ET_real
    let is_real_dist = function
      | Normal | Exponential | Poisson -> true
      | Bernoulli -> false
    let is_bool_dist = function
      | Normal | Exponential | Poisson -> false
      | Bernoulli -> true

    (* Helper functions: updates to abstract states *)
    let update_name_sets (t: t): expr -> t = function
      | Str s -> { t with t_sampvars = SS.add s t.t_sampvars }
      | StrFmt (s, _) -> { t with t_sampidx = SS.add s t.t_sampidx }
      | _ -> t
    (* xr: I think a call to any of these functions should be followed by
     *     some adaptation of the dimensions in the underlying numerical
     *     domain
     * example: if a variable becomes tensor, it would be good to add
     *          dimensions storing the dimensions of that tensor in num. dom.
     *
     * a similar issue occurs when calling the functions on update_name_sets:
     * the dimensions related to the distributions need to be modified
     *
     * I think the current structure of the domain (type is a sub-module)
     * makes these operations more awkward than if we had type information
     * inside the functor Adom_fib.Make, because each of these operations
     * here should come together with some operation on the octagon/other
     * numerical abstract value dimensions.
     *)
    let var_becomes_real (x: string) (t: t): t =
      { t with
        t_real = SS.add    x t.t_real ;
        t_tens = SS.remove x t.t_tens }
    let var_becomes_tens (x: string) (t: t): t =
      { t with
        t_real = SS.remove x t.t_real ;
        t_tens = SS.add    x t.t_tens }
    let var_becomes_other (x: string) (t: t): t =
      { t with
        t_real = SS.remove x t.t_real ;
        t_tens = SS.remove x t.t_tens }

    let eval ac (t: t): t =
      match ac with 
      | Assume _ | Observe _ -> 
          t
      | Assert b ->
          if (b = True) then
            t
          else 
            failwith (Printf.sprintf "%s.eval: Cannot prove Assert" module_name)
      | Sample (x, name, dist, args) ->
          let t = update_name_sets t name in
          if List.exists (is_tensor_exp t) args then
            var_becomes_tens x t
          else if is_real_dist dist then
            var_becomes_real x t
          else
            var_becomes_other x t
      | Assn (x, e) ->
          begin
            match get_exp_ty t e with
            | ET_tensor -> var_becomes_tens x t
            | ET_real -> var_becomes_real x t
            | ET_unknown | ET_bool -> var_becomes_other x t
          end
      | AssnCall (x, _, _, _) -> var_becomes_other x t

    let sat _ _ = false

    let join (t0: t) (t1: t) =
      { t_real     = SS.inter t0.t_real t1.t_real ;
        t_tens     = SS.inter t0.t_tens t1.t_tens ;
        t_sampidx  = SS.inter t0.t_sampidx t1.t_sampidx ;
        t_sampvars = SS.inter t0.t_sampvars t1.t_sampvars }
    let widen _ = join

    let leq (t0: t) (t1: t): bool =
      SS.subset t1.t_real t0.t_real
        && SS.subset t1.t_tens t0.t_tens
        && SS.subset t1.t_sampidx t0.t_sampidx
        && SS.subset t1.t_sampvars t0.t_sampvars

    (* additional *)
    let get_v (t: t): SS.t = t.t_real

    let get_w (t: t): SS.t = t.t_tens

    let get_a (t: t): SS.t = t.t_sampidx

    let get_b (t: t): SS.t = t.t_sampvars
  end

module TyDomain: ABST_DOMAIN_NB = 
  struct
    include TyDomainPriv 
    let module_name = "TyDomain"
  end
