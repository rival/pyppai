(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** adom_state.ml: abstract domain for state *)
open Ir_sig
open Lib
open Adom_sig
open Adom_type
open Adom_kind

module StateDomain: ABST_DOMAIN_NB =
  struct
    (* t is the type for abstract values:
     * (1) StBot means the empty set of states.
     * (2) StTuple(z, k) means the set of states where
     *     ...
     *)
    let module_name = "StateDomain"

    type t = TyDomainPriv.t * KindDomain.t (* NumDomain.t? *)

    let buf_t (buf: Buffer.t) (z,k): unit = 
        Printf.bprintf buf "[%a, %a]"
                       TyDomainPriv.buf_t z
                       KindDomain.buf_t k
    let to_string = buf_to_string buf_t
    let pp = buf_to_channel buf_t

    let top = TyDomainPriv.top, KindDomain.top(TyDomainPriv.top)
    let is_bot _ = false

    let reduce (z, k) = 
      let v_cap_w = SS.inter (TyDomainPriv.get_v z)
                             (TyDomainPriv.get_w z) in
      if not (SS.is_empty v_cap_w) ||
           (k = (KindDomain.bot z)) then raise Bottom
      else (z, k)
                   
    let eval' ac ((z0: TyDomainPriv.t),
                  (z1: TyDomainPriv.t),
                  (k: KindDomain.t)) : KindDomain.t =
      match ac with 
      | Assert b -> 
          if (b = True) then
            k
          else 
            failwith (Printf.sprintf "%s.eval: Cannot prove Assert" module_name)
      | Assume _ -> k
      | Observe _ -> k
      | Sample (x, name, dist, args) ->
         begin
           match name with
           | Str     s     -> KindDomain.add_map k s dist
           | StrFmt (a, e) -> KindDomain.add_map k a dist
           | _ -> failwith "eval'"
         end
      | Assn (x, e) -> k
      | AssnCall (x, _, _, _) -> k

    let eval ac st =
      try 
        let (z0, k0) = reduce st in
        let z1 = TyDomainPriv.eval ac z0 in
        let k1 = KindDomain.proj k0 z1 in
        let k_new = eval' ac (z0, z1, k1) in
        (z1, k_new)
      with Bottom -> raise Bottom (* check! *)

    let sat _ _ = failwith "todo: sat"

    let join (z1,k1) (z2,k2) =
      let z_new = TyDomainPriv.join z1 z2 in
      let k_new = KindDomain.join (KindDomain.proj k1 z_new)
                                  (KindDomain.proj k2 z_new) in
      (z_new, k_new)
      
    let widen _ (z1,k1) (z2,k2) =
      let z_new = TyDomainPriv.join z1 z2 in
      let k_new = KindDomain.join (KindDomain.proj k1 z_new)
                                  (KindDomain.proj k2 z_new) in
      (z_new, k_new)

    let leq (z1,k1) (z2,k2) =
      let k1_proj = KindDomain.proj k1 z2 in
      (TyDomainPriv.leq z1 z2) &&
        (KindDomain.leq k1_proj k2)
           
  end
