(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** adom_string.ml: Abstract domains to describe sets of names *)
open Lib
open Ir_sig
open Adom_sig


module MayStringDomain: ABST_DOMAIN_NB =
  struct
    let module_name = "MayStringDomain" 

    type t = SS.t
    let buf_t (buf: Buffer.t) (x: t): unit =
      Printf.bprintf buf "%a" buf_ss x
    let to_string = buf_to_string buf_t
    let pp = buf_to_channel buf_t
    let rec eval ac s =
      match ac with
      | Assert b ->
          if (b = True) then 
            s
          else
            failwith (Printf.sprintf "%s.eval: Cannot prove Assert" module_name)
      | Assume _ | Assn _ | AssnCall _ | Observe _ -> s
      | Sample (_, Str str, _, _) -> SS.add str s
      | Sample _ -> s
    let sat _ _ = false
    let top = SS.empty (* xr: check ! *)
    let is_bot _ = false
    let join = SS.union
    let widen = SS.union
    let leq = SS.subset
  end

module MustStringDomain : ABST_DOMAIN_NB =
  struct
    let module_name = "MustStringDomain" 

    type t = Top | Normal of SS.t
    let buf_t (buf: Buffer.t): t -> unit = function
      | Top -> Printf.bprintf buf "TOP"
      | Normal x -> Printf.bprintf buf "must: %a" buf_ss x
    let to_string = buf_to_string buf_t
    let pp = buf_to_channel buf_t
    let rec eval ac d =
      match ac with
      | Assert b ->
          if (b = True) then 
            d
          else
            failwith (Printf.sprintf "%s.eval: Cannot prove Assert" module_name)
      | Assume _ | Assn _ | AssnCall _ | Observe _ -> d
      | Sample (_, Str str, _, _) ->
          begin
            match d with
            | Top -> Top
            | Normal s -> Normal (SS.add str s)
          end
      | Sample _ -> d
    let sat _ _ = false
    let top = Top (* check ! *)
    let is_bot _ = false
    let join d1 d2 =
      match d1, d2 with
      | _, Top -> d1
      | Top, _ -> d2
      | Normal s1, Normal s2 -> Normal (SS.inter s1 s2)
    let widen = join
    let leq d1 d2 =
      match d1, d2 with
      | _, Top -> true
      | Top, _ -> false
      | Normal s1, Normal s2 -> SS.subset s2 s1
  end
