(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** adom_type.mli: abstract domain for typing *)
open Adom_sig

module TyDomainPriv:
  sig
    include ABST_DOMAIN_NB
    val get_v: t -> Lib.SS.t
    val get_w: t -> Lib.SS.t
    val get_a: t -> Lib.SS.t
    val get_b: t -> Lib.SS.t
  end

module TyDomain: ABST_DOMAIN_NB
