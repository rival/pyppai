(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** GNU General Public License
 **
 ** Authors:
 **  Wonyeol Lee, KAIST
 **  Xavier Rival, INRIA Paris
 **  Hongseok Yang, KAIST
 **  Hangyeol Yu, KAIST
 **
 ** Copyright (c) 2019 KAIST and INRIA Paris
 **
 ** rtest_data.ml: data for regression testing for the smoothness analysis *)
open Smooth_sig


(** Some utility functions to define goals *)
let all_diff (lv: string list): (string * string list) list =
  List.map (fun v -> v, lv) lv


let test_00 =
  let t_dep = { rte_name     = "basic test 0 (assignments)" ;
                rte_filename = "test/smooth/st-00.py" ;
                rte_analysis = Ak_fwd_ai ;
                rte_verbose  = false ;
                rte_adom_num = Nd_none ;
                rte_property = Sp_dep ;
                rte_outcome  = [ CEx, PDep [ "u", [ "u" ] ;
                                             "t", [ "u" ] ] ];
                rte_exp      = false } in
  let allv = [ "t"; "u"; "x"; "y"; "z" ] in
  let t_cd = { t_dep with
               rte_property = Sp_cont_diff ;
               rte_outcome  = [ CEx, PCDf (all_diff allv) ] } in
  let r_li = [ "t", [ "t", None ; "u", None ; "y", None ; "z", None ] ;
               "u", [ "u", Some 1. ] ] in
  let t_li = { t_dep with
               rte_property = Sp_lipschitz ;
               rte_verbose  = false;
               rte_outcome  = [ CEx, PLip r_li ] } in
  [ t_dep ; t_cd ; t_li ]

let test_01 =
  let t_dep = { rte_name     = "basic test 1 (assignments)" ;
                rte_filename = "test/smooth/st-01.py" ;
                rte_analysis = Ak_fwd_ai ;
                rte_verbose  = false ;
                rte_adom_num = Nd_none ;
                rte_property = Sp_dep ;
                rte_outcome  = [ CEx, PDep [ "t", [ "x"; "z" ] ;
                                             "u", [ "x" ] ;
                                             "x", [ "x" ] ;
                                             "y", [ "x"; "z" ] ;
                                             "z", [ "z" ] ] ];
                rte_exp      = false } in
  let allv = [ "t"; "u"; "x"; "y"; "z" ] in
  let t_cdf = { t_dep with
                rte_property = Sp_cont_diff ;
                rte_outcome  = [ CEx, PCDf [ "t", [ "t"; "u"; "y" ] ;
                                             "u", allv ;
                                             "x", allv ;
                                             "y", allv ;
                                             "z", allv ] ] } in
  let r_li = [ "t", [ "x", None ; "z", None ] ;
               "u", [ "x", Some 1. ] ;
               "x", [ "x", Some 1. ] ;
               "y", [ "x", None ; "z", None ] ;
               "z", [ "z", Some 1. ] ] in
  let t_li = { t_dep with
               rte_property = Sp_lipschitz ;
               rte_verbose  = true;
               rte_outcome  = [ CEx, PLip r_li ] } in
  [ t_dep ; t_cdf ; t_li ]

let test_02 =
  let t_dep = { rte_name     = "basic test 2 (if)" ;
                rte_filename = "test/smooth/st-02.py" ;
                rte_analysis = Ak_fwd_ai ;
                rte_verbose  = true ;
                rte_adom_num = Nd_none ;
                rte_property = Sp_dep ;
                rte_outcome  = [ CEx, PDep [ "t", [ "x" ] ;
                                             "u", [ "x"; "y" ] ;
                                             "x", [ "x" ] ;
                                             "y", [ "x" ] ;
                                             "z", [ "y" ] ] ];
                rte_exp      = false } in
  let allv = [ "t"; "u"; "x"; "y"; "z" ] in
  let partv = [ "t"; "u"; "y"; "z" ] in
  let t_cdf = { t_dep with
                rte_property = Sp_cont_diff ;
                rte_outcome  = [ CEx, PCDf [ "t", partv ;
                                             "u", partv ;
                                             "x", allv ;
                                             "y", allv ;
                                             "z", allv ] ] } in
  let r_li = [ "t", [ "x", None ] ;
               "u", [ "x", None ; "y", None ] ;
               "x", [ "x", Some 1. ] ;
               "y", [ "x", Some 1. ] ;
               "z", [ "y", Some 1. ] ] in
  let t_li = { t_dep with
               rte_property = Sp_lipschitz ;
               rte_verbose  = true;
               rte_outcome  = [ CEx, PLip r_li ] } in
  [ t_dep ; t_cdf ; t_li ]

let test_03 =
  let t_dep = { rte_name     = "basic test 3 (relu)" ;
                rte_filename = "test/smooth/st-03.py" ;
                rte_analysis = Ak_fwd_ai ;
                rte_verbose  = true ;
                rte_adom_num = Nd_none ;
                rte_property = Sp_dep ;
                rte_outcome  = [ CEx, PDep [ "t", [ "t" ] ;
                                             "x", [ "x" ] ;
                                             "y", [ "x" ] ;
                                             "z", [ "t"; "x" ] ] ];
                rte_exp      = false } in
  let allv = [ "t"; "x"; "y"; "z" ] in
  let restv = [ "t"; "y"; "z" ] in
  let t_cdf = { t_dep with
                rte_property = Sp_cont_diff ;
                rte_outcome  = [ CEx, PCDf [ "t", allv ;
                                             "x", allv ;
                                             "y", restv ;
                                             "z", restv ] ] } in
  let r_li = [ "t", [ "t", Some 1. ] ;
               "x", [ "x", Some 1. ] ;
               "y", [ "x", None ] ;
               "z", [ "t", None ; "x", None ] ] in
  let t_li_off = { t_dep with
                   rte_property = Sp_lipschitz ;
                   rte_verbose  = true;
                   rte_outcome  = [ CEx, PLip r_li ] } in
  let t_li_on  = { t_li_off with
                   rte_adom_num = Nd_box } in
  let r_li_exp = [ "t", [ "t", Some 1. ] ;
                   "x", [ "x", Some 1. ] ;
                   "y", [ "x", Some 1. ] ;
                   "z", [ "t", None ; "x", None ] ] in
  let t_li_exp = { t_li_on with
                   rte_exp      = true;
                   rte_outcome  = [ CEx, PLip r_li_exp ] } in
  [ t_dep ; t_cdf ; t_li_off ; t_li_on ; t_li_exp ]

let test_04 =
  let t_dep = { rte_name     = "basic test 4 (while)" ;
                rte_filename = "test/smooth/st-04.py" ;
                rte_analysis = Ak_fwd_ai ;
                rte_verbose  = true ;
                rte_adom_num = Nd_none ;
                rte_property = Sp_dep ;
                rte_outcome  = [ CEx, PDep [ "t", [ "t" ] ;
                                             "x", [ ] ;
                                             "y", [ "y" ] ;
                                             "z", [ "t"; "y" ] ] ];
                rte_exp      = false } in
  let allv = [ "t"; "x"; "y"; "z" ] in
  let t_cdf = { t_dep with
                rte_property = Sp_cont_diff ;
                rte_outcome  = [ CEx, PCDf (all_diff allv) ] } in
  let r_li = [ "t", [ "t", Some 1. ] ;
               "x", [ ] ;
               "y", [ "y", Some 1. ] ;
               "z", [ "t", Some 1. ; "y", Some 1. ] ] in
  let t_li = { t_dep with
               rte_property = Sp_lipschitz ;
               rte_verbose  = true;
               rte_outcome  = [ CEx, PLip r_li ] } in
  [ t_dep ; t_cdf ; t_li ]


let test_05 =
  (* TODO: add test 5 *)
  [ ]

let test_06 =
  let t_dep = { rte_name     = "basic test 6 (while)" ;
                rte_filename = "test/smooth/st-06.py" ;
                rte_analysis = Ak_fwd_ai ;
                rte_verbose  = true ;
                rte_adom_num = Nd_none ;
                rte_property = Sp_dep ;
                rte_outcome  = [ CEx, PDep [ "t", [ "t" ] ;
                                             "u", [ "u" ] ;
                                             "x", [ "u" ] ;
                                             "y", [ "y"; "u" ] ;
                                             "z", [ "t"; "y"; "u" ] ] ];
                rte_exp      = false } in
  let others = [ "t"; "x"; "y"; "z" ] in
  let allv = "u" :: others in
  let ldiff =
    List.map (fun v -> v, (if v="t" || v="u" then allv else others)) allv in
  let t_cdf = { t_dep with
                rte_property = Sp_cont_diff ;
                rte_outcome  = [ CEx, PCDf ldiff ] } in
  let t_li =
    let r_li = [ "t", [ "t", Some 1. ] ;
                 "u", [ "u", Some 1. ] ;
                 "x", [ "u", None ] ;
                 "y", [ "u", None ; "y", Some 1. ] ;
                 "z", [ "t", Some 1. ; "u", None ; "y", Some 1. ] ] in
    { t_dep with
      rte_property = Sp_lipschitz ;
      rte_verbose  = true;
      rte_outcome  = [ CEx, PLip r_li ] } in
  [ t_dep ; t_cdf ; t_li ]

let test_07 =
  let allv = [ "a"; "b"; "t"; "x"; "y"; "z" ] in
  let allp = [ "b"; "t"; "x"; "y"; "z" ] in
  let t_dep_none =
    { rte_name     = "basic test 7 (if)" ;
      rte_filename = "test/smooth/st-07.py" ;
      rte_analysis = Ak_fwd_ai ;
      rte_verbose  = true ;
      rte_adom_num = Nd_none ;
      rte_property = Sp_dep ;
      rte_outcome  = [ CEx, PDep [ "a", [ "a" ] ;
                                   "b", [ "b" ] ;
                                   "t", [ "a"; "b" ] ;
                                   "x", [ ] ;
                                   "y", [ ] ;
                                   "z", [ "a"; "b" ] ] ];
      rte_exp      = false } in
  let t_dep_box =
    { t_dep_none with
      rte_adom_num = Nd_box ;
      rte_outcome  = [ CEx, PDep [ "b", [ "b" ] ;
                                   "t", [ "b" ] ;
                                   "x", [ ] ;
                                   "y", [ ] ;
                                   "z", [ "b" ] ] ] } in
  let t_diff_none =
    { t_dep_none with
      rte_property = Sp_cont_diff ;
      rte_outcome  = [ CEx, PCDf (all_diff allv) ] } in
  let t_diff_box =
    { t_diff_none with
      rte_adom_num = Nd_box ;
      rte_outcome  = [ CEx, PCDf (all_diff allp) ] } in
  let t_li_none =
    let r_li = [ "a", [ "a", Some 1. ] ;
                 "b", [ "b", Some 1. ] ;
                 (* interesting result: the analysis knows t does not depend
                  * on x (which is constant), though it does not know which
                  * branch is taken so maybe t=a or maybe t=b... *)
                 "t", [ "a", Some 1. ; "b", Some 1. ] ;
                 "x", [ ] ;
                 "y", [ ] ;
                 "z", [ "a", None; "b", None ] ] in
    { t_dep_none with
      rte_property = Sp_lipschitz ;
      rte_verbose  = true;
      rte_outcome  = [ CEx, PLip r_li ] } in
  let t_li_box =
    let r_li = [ "b", [ "b", Some 1. ] ;
                 "t", [ "b", Some 1. ] ;
                 "x", [ ] ;
                 "y", [ ] ;
                 "z", [ "b", Some 64. ] ] in
    { t_dep_box with
      rte_property = Sp_lipschitz ;
      rte_verbose  = true;
      rte_outcome  = [ CEx, PLip r_li ] } in
  [ t_dep_none ; t_dep_box ; t_diff_none ; t_diff_box ; t_li_none ; t_li_box ]


let test_08 =
  let allv = [ "a"; "b"; "t"; "x"; "y"; "z" ] in
  let allp = [ "b"; "t"; "x"; "y"; "z" ] in
  let t_dep_none =
    { rte_name     = "basic test (if, sampled input)" ;
      rte_filename = "test/smooth/st-08.py" ;
      rte_analysis = Ak_fwd_ai ;
      rte_verbose  = true ;
      rte_adom_num = Nd_none ;
      rte_property = Sp_dep ;
      rte_outcome  = [ CEx, PDep [ "a", [ "a" ] ;
                                   "b", [ "b" ] ;
                                   "t", [ "a"; "b" ] ;
                                   "x", [ ] ;
                                   "y", [ ] ;
                                   "z", [ "a"; "b" ] ] ];
      rte_exp      = false } in
  let t_dep_box =
    { t_dep_none with
      rte_adom_num = Nd_box ;
      rte_outcome  = [ CEx, PDep [ "b", [ "b" ] ;
                                   "t", [ "b" ] ;
                                   "x", [ ] ;
                                   "y", [ ] ;
                                   "z", [ "b" ] ] ] } in
  let t_diff_none =
    { t_dep_none with
      rte_property = Sp_cont_diff ;
      rte_outcome  = [ CEx, PCDf (all_diff allv) ] } in
  let t_diff_box =
    { t_diff_none with
      rte_adom_num = Nd_box ;
      rte_outcome  = [ CEx, PCDf (all_diff allp) ] } in
  let t_li_none =
    let r_li = [ "a", [ "a", Some 1. ] ;
                 "b", [ "b", Some 1. ] ;
                 (* interesting result: the analysis knows t does not depend
                  * on x (which is constant), though it does not know which
                  * branch is taken so maybe t=a or maybe t=b... *)
                 "t", [ "a", Some 1. ; "b", Some 1. ] ;
                 "x", [ ] ;
                 (* same comment as above *)
                 "y", [ "x", Some 1. ] ;
                 "z", [ "a", None; "b", None ; "x", None ] ] in
    { t_dep_none with
      rte_property = Sp_lipschitz ;
      rte_verbose  = true;
      rte_outcome  = [ CEx, PLip r_li ] } in
  let t_li_box =
    let r_li = [ "b", [ "b", Some 1. ] ;
                 "t", [ "b", Some 1. ] ;
                 "x", [ ] ;
                 "y", [ "x", Some 1. ] ;
                 "z", [ "b", Some 16. ; "x", None ] ] in
    { t_dep_box with
      rte_property = Sp_lipschitz ;
      rte_verbose  = true;
      rte_outcome  = [ CEx, PLip r_li ] } in
  [ t_dep_none ; t_dep_box ; t_diff_none ; t_diff_box ; t_li_none ; t_li_box ]

let test_09 =
  let t_dep = { rte_name     = "st-09 (if, control deps)" ;
                rte_filename = "test/smooth/st-09.py" ;
                rte_analysis = Ak_fwd_ai ;
                rte_verbose  = false ;
                rte_adom_num = Nd_none ;
                rte_property = Sp_dep ;
                rte_outcome  = [ CEx, PDep [ "a", [ "g" ] ;
                                             "g", [ "g" ] ] ];
                rte_exp      = false } in
  let t_cd  = { t_dep with
                rte_property = Sp_cont_diff ;
                rte_outcome  = [ CEx, PCDf [ "a", [ "a" ] ;
                                             "g", [ "a"; "g" ] ] ] } in
  let t_li =
    let r_li = [ "g", [ "g", Some 1. ] ;
                 "a", [ "g", None ] ] in
    { t_dep with
      rte_property = Sp_lipschitz ;
      rte_verbose  = true;
      rte_outcome  = [ CEx, PLip r_li ] } in
  [ t_dep ; t_cd ; t_li ]

let test_10 =
  let allv = [ "t"; "u"; "x"; "y"; "z" ] in
  let nodeps = List.map (fun v -> v, [ ]) allv in
  let alldiff = List.map (fun v -> v, allv) allv in
  let t_dep = { rte_name     = "st-10 (sample, assigns)" ;
                rte_filename = "test/smooth/st-10.py" ;
                rte_analysis = Ak_fwd_ai ;
                rte_verbose  = false ;
                rte_adom_num = Nd_none ;
                rte_property = Sp_dep ;
                rte_outcome  = [ CEx, PDep nodeps ];
                rte_exp      = false } in
  let t_cdf = { t_dep with
                rte_property = Sp_cont_diff ;
                rte_outcome  = [ CEx, PCDf alldiff ] } in
  let t_li =
    let r_li = [ "t", [ "x", Some 3. ; "y", Some 4. ] ;
                 "u", [ "x", Some 2. ] ;
                 "z", [ "x", Some 1. ; "y", Some 1. ] ] in
    { t_dep with
      rte_property = Sp_lipschitz ;
      rte_verbose  = true;
      rte_adom_num = Nd_box ;
      rte_outcome  = [ CEx, PLip r_li ] } in
  [ t_dep ; t_cdf ; t_li ]

let test_11 =
  let allv = [ "a"; "t"; "u"; "x"; "y"; "z" ] in
  let t_dep = { rte_name     = "st-11 (limiting, assigns)" ;
                rte_filename = "test/smooth/st-11.py" ;
                rte_analysis = Ak_fwd_ai ;
                rte_verbose  = false ;
                rte_adom_num = Nd_none ;
                rte_property = Sp_dep ;
                rte_outcome  = [ CEx, PDep [ "a", [ "a" ];
                                             "t", [ "x"; "y" ] ;
                                             "u", [ "a"; "x" ] ;
                                             "x", [ "x" ] ;
                                             "y", [ "y" ] ;
                                             "z", [ "x"; "y" ] ] ];
                rte_exp      = false } in
  let t_cdf = { t_dep with
                rte_property = Sp_cont_diff ;
                rte_outcome = [ CEx, PCDf [ "a", allv ;
                                            "t", [ "a"; "t"; "u"; "z" ] ;
                                            "u", [ "a"; "t"; "u"; "y"; "z" ] ;
                                            "x", [ "a"; "t"; "u"; "y"; "z" ] ;
                                            "y", [ "a"; "t"; "u"; "x"; "z" ] ;
                                            "z", [ "a"; "t"; "u"; "z" ] ] ] } in
  let t_li =
    let r_li = [ "a", [ "a", Some 1. ] ;
                 "t", [ "x", None ; "y", None ] ;
                 "u", [ "a", Some 8. ; "x", None ] ;
                 "x", [ "x", None ] ; (* imprecision clipping *)
                 "y", [ "y", None ] ; (* imprecision clipping *)
                 "z", [ "x", None ; "y", None ] ] in
    { t_dep with
      rte_property = Sp_lipschitz ;
      rte_verbose  = true;
      rte_adom_num = Nd_box ;
      rte_outcome  = [ CEx, PLip r_li ] } in
  [ t_dep ; t_cdf ; t_li ]


let test_12 =
  let allv = [ "a"; "t"; "u"; "x"; "y"; "z" ] in
  let t_dep = { rte_name     = "st-11 (range, assigns)" ;
                rte_filename = "test/smooth/st-12.py" ;
                rte_analysis = Ak_fwd_ai ;
                rte_verbose  = false ;
                rte_adom_num = Nd_none ;
                rte_property = Sp_dep ;
                rte_outcome  = [ CEx, PDep [ "a", [ "a" ];
                                             "t", [ "x"; "y" ] ;
                                             "u", [ "a"; "x" ] ;
                                             "x", [ "x" ] ;
                                             "y", [ "y" ] ;
                                             "z", [ "x"; "y" ] ] ];
                rte_exp      = false } in
  let diffs = List.map (fun v -> v, allv) allv in
  let t_cdf = { t_dep with
                rte_property = Sp_cont_diff ;
                rte_outcome = [ CEx, PCDf diffs ] } in
  let t_li =
    let r_li = [ "a", [ "a", Some 1. ] ;
                 "t", [ "x", Some 3. ; "y", Some 4. ] ;
                 "u", [ "a", Some 8. ; "x", None ] ;
                 "x", [ "x", Some 1. ] ;
                 "y", [ "y", Some 1. ] ;
                 "z", [ "x", Some 1. ; "y", Some 1. ] ] in
    { t_dep with
      rte_property = Sp_lipschitz ;
      rte_verbose  = true;
      rte_adom_num = Nd_box ;
      rte_outcome  = [ CEx, PLip r_li ] } in
  [ t_dep ; t_cdf ; t_li ]


let test_13 =
  let t_li =
    { rte_name     = "basic test 13 (Lipschitz, arithmetics)" ;
      rte_filename = "test/smooth/st-13.py" ;
      rte_analysis = Ak_fwd_ai ;
      rte_verbose  = true ;
      rte_adom_num = Nd_none ;
      rte_property = Sp_lipschitz ;
      rte_outcome  = [ CEx, PLip [ "a", [ "a", Some 1. ] ;
                                   "b", [ "b", Some 1. ] ;
                                   "c", [ "c", Some 1. ] ;
                                   "x", [ "c", Some 2. ] ;
                                   "y", [ "a", Some 1.; "b", Some 1. ] ;
                                   "z", [ "b", None ; "c", None ] ] ];
      rte_exp      = false } in
  [ t_li ]

(** All tests *)
let all_tests =
  test_00 @ test_01 @ test_02 @ test_03 @ test_04 (*@ test_05*) @ test_06 @
  test_07 @ test_08 @ test_09 @ test_10 @ test_11 @ test_12 @ test_13
