(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** GNU General Public License
 **
 ** Authors:
 **  Wonyeol Lee, KAIST
 **  Xavier Rival, INRIA Paris
 **  Hongseok Yang, KAIST
 **  Hangyeol Yu, KAIST
 **
 ** Copyright (c) 2019 KAIST and INRIA Paris
 **
 ** batch_smooth.ml: regression testing for the smoothness analysis *)
open Lib

open Analysis_sig
open Smooth_sig


(** Flags *)
let flag_force_silent:  bool ref = ref false
let flag_force_verbose: bool ref = ref false


(** Minimal infrastructure to run everything *)
let main () =
  (* Argument parsing *)
  Arg.parse
    [ "-s", Arg.Set flag_force_silent , "forcing all analysese silent";
      "-v", Arg.Set flag_force_verbose, "forcing all analysese verbose"
    ] (fun _ -> ()) "Batch testing for smoothness property analysis";
  Printf.printf "Regression testing for smoothness property analyzer\n\n";
  (* Constants and counting of test results *)
  let sep = String.make 72 '=' in
  let tests_ok = ref 0
  and tests_ko = ref 0 in
  (* Auxilliary functions *)
  let comp_verbose b = !flag_force_verbose || b && not !flag_force_silent in
  let parse f =
    Ir_parse.output := false ;
    Ir_parse.parse_code None (AI_pyfile f) |> Ir_util.simplify_delta_prog in
  (* Running one test *)
  let run_one i regt =
    Printf.printf "%s\nTest %d: %s\n file: %s\n" sep i regt.rte_name
      regt.rte_filename;
    try
      let res =
        match regt.rte_analysis with
        | Ak_comp ->
            Printf.printf " => needs to run compositional analysis (TODO)\n";
            false
        | Ak_fwd_ai ->
            let ir = parse regt.rte_filename in
            Smooth_utils.dbg_verbose := comp_verbose regt.rte_verbose;
            let analyze =
              if regt.rte_exp then Ai_smooth_exp_ai.analyze
              else Ai_smooth_fwd_ai.analyze in
            let output = analyze regt.rte_adom_num regt.rte_property ir in
            List.fold_left
              (fun acc goal -> acc && output.aout_check goal)
              true regt.rte_outcome in
      Printf.printf "Result for test %d (%s) => %b\n\n" i regt.rte_name res;
      incr (if res then tests_ok else tests_ko)
    with exc ->
      Printf.printf "Result for test %d (%s) => EXC(%s)\n\n" i regt.rte_name
        (Printexc.to_string exc);
      incr tests_ko in
  (* Run all the tests *)
  List.iteri run_one Rtest_data.all_tests;
  (* Display the results *)
  Printf.printf
    "\n%s\nRegression tests report:\n Total:  %d\n OK:     %d\n KO:     %d\n"
    sep (!tests_ok + !tests_ko) !tests_ok !tests_ko
let _ = ignore (main ())
