(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** GNU General Public License
 **
 ** Authors:
 **  Wonyeol Lee, KAIST
 **  Xavier Rival, INRIA Paris
 **  Hongseok Yang, KAIST
 **  Hangyeol Yu, KAIST
 **
 ** Copyright (c) 2019 KAIST and INRIA Paris
 **
 ** ai_smooth_fwd_ai.ml: main file for smoothness analysis
 **                      version based on forward abstract interpretation *)
open Ir_sig
open Smooth_sig

(** Construction of the analyzer *)
module Analyzer: ADOM_NUM -> ADOM_SMOOTHNESS -> SMOOTHNESS_ANALYZER

(** Static analyzer *)
val analyze: num_domain -> smoothness_property -> prog -> analysis_output
