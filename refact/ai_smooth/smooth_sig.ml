(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** GNU General Public License
 **
 ** Authors:
 **  Wonyeol Lee, KAIST
 **  Xavier Rival, INRIA Paris
 **  Hongseok Yang, KAIST
 **  Hangyeol Yu, KAIST
 **
 ** Copyright (c) 2019 KAIST and INRIA Paris
 **
 ** smooth_sig.ml: general definition for smoothness analyses *)
open Lib

open Ir_sig


(** Analysis parameterization *)

(* Numerical domain *)
(* (differs slightly from other analyses *)
type num_domain = Nd_none | Nd_box | Nd_oct | Nd_pol

(* Kind of analysis algorithm *)
type analysis_kind =
  | Ak_comp      (* compositional *)
  | Ak_fwd_ai    (* forward abstract interpretation *)

(* Target property for the analysis to compute *)
type smoothness_property =
  | Sp_dep       (* just dependences (baseline) *)
  | Sp_cont_diff (* continuous differentiability *)
  | Sp_lipschitz (* lipschitzness; infer bounds *)


(** Definition of expected analysis outcomes *)

(* Kinds of predicates to prove or not *)
type predicate =
    (* Dependencies:
     *  maps variable x to a list of variables it (may) depend on *)
  | PDep of (string * (string list)) list
    (* Continuous differentiability:
     *  maps variable x to a list of variables it (may not) be continously
     *    differentiable with respect to *)
  | PCDf of (string * (string list)) list
    (* Lipschitz continuity:
     *  maps variable x to a list of variables with Lipschitz constant
     *    (or absence of Lipschitz constant)
     *)
  | PLip of (string * (string * float option) list) list
(* Check kind *)
type check_kind =
  | CEx  (* result should be known exactly *)
  | CInc (* result should be included into *)
(* Analysis outcomes *)
type analysis_outcome = check_kind * predicate


(** Definition of regression testing entries *)

(* Regression testing entries
 *  most relevant data types are defined in smooth_sig.ml *)
type rte_entry =
    { (* regression test name *)
      rte_name:        string ;
      (* file to analyze *)
      rte_filename:    string ;
      (* kind of iterator (forward or compositional) *)
      rte_analysis:    analysis_kind ;
      (* whether we want verbose analysis *)
      rte_verbose:     bool;
      (* numeric domain (relevant only for forward) *)
      rte_adom_num:    num_domain ;
      (* smoothness property to check *)
      rte_property:    smoothness_property ;
      (* expected analysis outcome *)
      rte_outcome:     analysis_outcome list;
      (* whether to use experimental smoothness abstraction *)
      rte_exp:         bool }


(** Intervals *)

(* An interval is a pair of bounds
 * The "None" bound means either -oo or +oo *)
type interv = float option * float option


(** Exceptions used internally in the analysis definition *)
exception NotLE


(** Abstract domains signature *)

(** Signature for numeric abstract domains *)
module type ADOM_NUM =
  sig
    (** Abstract elements *)
    type t

    (** Pretty-printing *)
    val buf_t: string -> Buffer.t -> t -> unit
    val pp: string -> out_channel -> t -> unit

    (** Lattice operations *)
    val is_bot: t -> bool
    val top: t
    val join: t -> t -> t
    val widen: t -> t -> t
    val is_le: t -> t -> bool

    (** Basic operations on dimensions *)
    val dim_add: string -> t -> t

    (** Abstract operations *)
    (* Assignment *)
    val assign: string -> expr -> t -> t
    (* Assignment to an unknown interval (for distributions) *)
    val set_interv: string -> interv -> t -> t
    (* Condition test *)
    val guard: expr -> t -> t
    (* Checks that a condition holds *)
    val sat: expr -> t -> bool

    (** Getting information from the domain *)
    (* Interval of an expression; may return Top if expr cannot be
     * precisely interpreted in the underlying domain *)
    val get_expr_interv: t -> expr -> interv
  end

(** Signature of the smoothness abstract domain *)
module type ADOM_SMOOTHNESS =
  sig
    (** Name of the property being tracked *)
    val property: string (* name of the property *)
    val need_pp:  bool   (* whether it should be printed *)

    (** Abstract elements *)
    type t

    (** Pretty-printing *)
    val buf_t: string -> SS.t -> Buffer.t -> t -> unit
    val pp: string -> SS.t -> out_channel -> t -> unit

    (** Lattice operations *)
    val id: t -> t
    val top: t
    val join: t -> t -> t
    val widen: t -> t -> t
    val is_le: t -> t -> bool

    (** Abstract operations *)
    (* Add a new variable;
     *       initially not modified, so "equal to its initial value" *)
    val add_var: string -> t -> t
    (* Assignment
     *  vars:  SS.t
     *    all variables
     *  cvars: SS.t
     *    conditioning variables
     *  compute_dep: expr -> SS.t
     *    function to compute the depencency of a sub-expression
     *    (e.g., for the division)
     *  compute_interv: expr -> interv
     *    computes an interval for an expression (for Lipschitzness analysis)
     *)
    val assign: SS.t -> SS.t -> (expr -> SS.t) -> (expr -> interv)
      -> string -> expr -> t -> t
    (* Reinitializes information for a variable; e.g. on non determinism *)
    val reinit: string -> t -> t
    (* Dropping all information on variable id
     *   vars: is the set of all known variables *)
    val drop: string -> SS.t -> t -> t
    (* Composition *)
    val compose: t -> t -> t

    (** Getting results and checking them *)
    (* Checking some predicate
     *  vars:  SS.t
     *    all variables
     *  k,p:
     *    predicate to check; dependency is ignored *)
    val checker_predicate: SS.t -> check_kind * predicate -> t -> bool
  end


(** Analysis module type *)
module type SMOOTHNESS_ANALYZER =
  sig
    (** Abstract elements *)
    type t

    (** Pretty-printing *)
    val buf_t: string -> Buffer.t -> t -> unit
    val pp: string -> out_channel -> t -> unit

    (** Analyzer function *)
    val a_prog: block -> t

    (** Checker for analysis results *)
    val checker: t -> analysis_outcome -> bool
  end


(** Analyzer construction *)
type analysis_output = { aout_check: (analysis_outcome -> bool);
                         aout_buf_t: (string -> Buffer.t -> unit -> unit);
                         aout_pp:    (string -> out_channel -> unit -> unit) }
