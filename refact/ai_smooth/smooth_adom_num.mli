(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** GNU General Public License
 **
 ** Authors:
 **  Wonyeol Lee, KAIST
 **  Xavier Rival, INRIA Paris
 **  Hongseok Yang, KAIST
 **  Hangyeol Yu, KAIST
 **
 ** Copyright (c) 2019 KAIST and INRIA Paris
 **
 ** smooth_adom_num.mli: abstract domains for smoothness analysis
 **                      numerical abstract domains component *)
open Apron_sig
open Smooth_sig


(** Numerical domain with no information *)
module A_Num_None: ADOM_NUM


(** Numerical domain based on Apron *)
module A_Num_Apron_Make: APRON_MGR -> ADOM_NUM
