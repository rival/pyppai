(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** GNU General Public License
 **
 ** Authors:
 **  Wonyeol Lee, KAIST
 **  Xavier Rival, INRIA Paris
 **  Hongseok Yang, KAIST
 **  Hangyeol Yu, KAIST
 **
 ** Copyright (c) 2019 KAIST and INRIA Paris
 **
 ** smooth_adom_smoothness.mli:
 **                     abstract domains for smoothness analysis
 **                     smoothness abstract domains component *)
open Smooth_sig


(** Only dependency abstraction
 **  As it is already in the core domain, there is nothing more to track *)
module ADsSmooth: ADOM_SMOOTHNESS

(** Continuous differentiability *)
module ADsDiff: ADOM_SMOOTHNESS

(** Lipschitz continuity
 **   => this domain tries to infers coefficient for each variable *)
module ADsLipschitz: ADOM_SMOOTHNESS
