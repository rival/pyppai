(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** GNU General Public License
 **
 ** Authors:
 **  Wonyeol Lee, KAIST
 **  Xavier Rival, INRIA Paris
 **  Hongseok Yang, KAIST
 **  Hangyeol Yu, KAIST
 **
 ** Copyright (c) 2019 KAIST and INRIA Paris
 **
 ** ai_smooth_fwd_ai.ml: main file for smoothness analysis
 **                      version based on forward abstract interpretation *)
open Lib

open Apron_sig
open Ir_sig
open Smooth_sig

open Smooth_adom_num
open Smooth_adom_smoothness

open Ir_util
open Smooth_utils

(** Assumptions:
 **  (1) For now, sample is considered as a hack to have a non-deterministic
 **      initialization of a variable to a value in a range; it does not
 **      induce any dependency or non differentiability as we do not have a
 **      notion of random data-base.
 **      This should be changed to handle proper probabilistic programs.
 **  (2) To add some assumption on some initial range for a variable, we need
 **      to include a "limiter" in the source code, like this:
 **      if x < a:
 **          x = a
 **      if x > b:
 **          x = b
 **)

(** Error report *)
let warn_todo s =
  Printf.eprintf "TODO: %S\n" s
let todo s =
  failwith (Printf.sprintf "TODO: %s" s)
let unsup s =
  failwith (Printf.sprintf "UNSUPPORTED in smoothness analysis: %s" s)

(** Construction of the analyzer *)
module Analyzer = functor (N: ADOM_NUM) -> functor (P: ADOM_SMOOTHNESS) ->
  struct
    (** Experimental trace abstraction layer
     ** We leave it here for now;
     ** TODO: move to separate module and make it optional *)
    module T =
      struct
        (* replace with a proper flag *)
        let do_trace = true
        type info =
            { i_kovars: SS.t;
              i_okvars: expr SM.t }
        type tok =
          | T_beg (* some operations that were not tracked *)
          | T_if_info of info option (* trace info if any; None means top *)
        type t = tok list (* a trace of "open" flows *)
        let buf_t (ind: string) (buf: Buffer.t) (t: t): unit =
          match t with
          | [ ] | T_beg :: _ ->
              Printf.bprintf buf "%s<...>\n" ind
          | T_if_info None :: _ ->
              Printf.bprintf buf "%sIf<Top>\n" ind
          | T_if_info (Some i) :: _ ->
              Printf.bprintf buf "%sIf:\n" ind;
              SM.iter
                (fun v e ->
                  Printf.bprintf buf "%s  %s: %a\n" ind v Ir_util.buf_expr e
                ) i.i_okvars;
              Printf.bprintf buf "%s  KO: %a\n" ind buf_ss i.i_kovars
        let init = [ T_beg ]
        let top = init
        (* Temporary, precision drop when unable to compute post *)
        let invalidate (msg: string) (t: t): t =
          if do_trace then
            Printf.printf "TODO: invalidate trace info <%d>: %s\n"
              (List.length t) msg;
          match t with
          | T_if_info _ :: l -> T_beg :: l
          | _ -> t
        (* Assignment *)
        let assign (id: string) (e: expr) (t: t): t =
          match t with
          | [ ]
          | T_beg :: _ | T_if_info None :: _ -> t
          | T_if_info (Some i) :: tr ->
              let rec aux ex =
                match ex with
                | Nil | True | False | Num _ | Str _ -> ex
                | Name n ->
                    begin
                      try
                        if SS.mem n i.i_kovars then raise Stop;
                        StringMap.find n i.i_okvars
                      with
                      | Not_found -> ex
                    end
                | UOp (u, e) -> UOp (u, aux e)
                | BOp (b, e0, e1) -> BOp (b, aux e0, aux e1)
                | Comp (c, e0, e1) -> Comp (c, aux e0, aux e1)
                | List l -> List (List.map aux l)
                | Dict (l0, l1) -> Dict (List.map aux l0, List.map aux l1)
                | StrFmt (s, l) -> StrFmt (s, List.map aux l) in
              let i =
                try { i with i_okvars = SM.add id (aux e) i.i_okvars }
                with Stop -> { i with i_kovars = SS.add id i.i_kovars } in
              T_if_info (Some i) :: tr
        (* Post-condition *)
        let start_if (t: t): t =
          let i = Some { i_kovars = SS.empty ;
                         i_okvars = SM.empty } in
          T_if_info i :: t
        let end_if (t: t): t =
          match t with
          | T_if_info _ :: t -> t
          | _ -> failwith "end if, no parenthesis to close"
        (* Merging *)
        let exp_continuity_merge (t0: t) (t1: t)
            : (expr * expr) SM.t =
          match t0, t1 with
          | [ ], _ | _, [ ]
          | T_beg :: _, _ | _, T_beg :: _
          | T_if_info None :: _, _ | _, T_if_info None :: _ ->
              SM.empty
          | T_if_info (Some i0) :: _, T_if_info (Some i1) :: _ ->
              let kovars = SS.union i0.i_kovars i1.i_kovars in
              let modvars =
                let f m s = SM.fold (fun i _ -> SS.add i) m s in
                f i0.i_okvars (f i1.i_okvars SS.empty) in
              let modvars = SS.diff modvars kovars in
              let m =
                SS.fold
                  (fun v acc ->
                    let f m = try SM.find v m with Not_found -> Name v in
                    SM.add v (f i0.i_okvars, f i1.i_okvars) acc
                  ) modvars SM.empty in
              Printf.printf "Continuity\n-KO: %a\n-Exprs:\n" ss_pp kovars;
              SM.iter
                (fun id (e0, e1) ->
                  Printf.printf "  %s: %a, %a\n" id pp_expr e0 pp_expr e1
                ) m;
              m
      end
    (** Dependency information *)
    module D =
      struct
        (* dependences
         * function from variables to sets of variables D
         *   for each x, for two executions starting in stages where variables
         *   in D(x) do not change, then value for x does not change *)
        type t = SS.t SM.t
        (* compute the identity dependency for a given set of variables *)
        let id (t: t): t = SM.mapi (fun x _ -> SS.singleton x) t
        (* composition: first do t0, then t1 *)
        let compose (t0: t) (t1: t): t =
          SM.fold
            (fun x s acc ->
              let s =
                SS.fold
                  (fun y accs ->
                    let r = try SM.find y t0 with Not_found -> SS.singleton y in
                    SS.union r accs
                  ) s SS.empty in
              SM.add x s acc
            ) t1 t0
      end
    (** Abstract elements *)
    type t =
        { (* temporary:
           *   when it becomes true, it means a problem was encountered during
           *   the analysis and the result is not meaningful *)
          t_istop:  bool;
          (* all known variables
           * set of variables V
           *   all stores should define variables in v *)
          t_vars:   SS.t;
          (* variables controlling loops/errors
           * set of variables L
           *   for two executions, if variables in L do not change,
           *   non-termination and error behaviors are unchanged *)
          t_fin:    SS.t;
          (* dependences
           * function from variables to sets of variables D
           *   for each x, for two executions starting in stages where variables
           *   in D(x) do not change, then value for x does not change *)
          t_deps:   D.t;
          (* smoothness information
           *   the representation depends on the underlying domain P *)
          t_smooth: P.t;
          (* numerical abstraction *)
          t_num:    N.t;
          (* experimental trace abstraction, to attempt proving smoothness *)
          t_trace:  T.t }

    (** Abstraction of an empty memory state (initial state) *)
    let emp: t = { t_istop  = false;
                   t_vars   = SS.empty;
                   t_fin    = SS.empty;
                   t_deps   = SM.empty;
                   t_smooth = P.id P.top;
                   t_num    = N.top;
                   t_trace  = T.top }

    (** Pretty-printing *)
    let buf_t (ind: string) (buf: Buffer.t) (t: t): unit =
      let sep = String.make 32 '=' in
      Printf.bprintf buf "%s%s\n" ind sep;
      if t.t_istop then
        Printf.bprintf buf "%s => Failed, No information\n" ind
      else
        begin
          let nind = ind^"  " in
          Printf.bprintf buf "%sLoop/errors: %a\n" ind buf_ss t.t_fin;
          Printf.bprintf buf "%sDep:\n" ind;
          SM.iter
            (fun id dep ->
              Printf.bprintf buf "%s  %s => %a\n" ind id buf_ss dep
            ) t.t_deps;
          Printf.bprintf buf "%sNum:\n%a" ind (N.buf_t nind) t.t_num;
          if P.need_pp then
            Printf.bprintf buf "%s%s:\n%a" ind P.property
              (P.buf_t (ind^"  ") t.t_vars) t.t_smooth;
          Printf.bprintf buf "%sTrace:\n%a" ind (T.buf_t nind) t.t_trace
        end;
      Printf.bprintf buf "%s%s\n" ind sep
    let pp ind = buf_to_channel (buf_t ind)

    (** Management of variables *)
    (* Adding one variable to the environment *)
    let add_var (id: string) (io: interv option) (t: t): t =
      let do_add () =
        { t with
          t_vars   = SS.add id t.t_vars ;
          t_deps   = SM.add id (SS.singleton id) t.t_deps ;
          t_smooth = P.add_var id t.t_smooth;
          t_num    = N.dim_add id t.t_num } in
      match io with
      | None ->
          if SS.mem id t.t_vars then
            begin
              if not (SM.mem id t.t_deps) then
                failwith (Printf.sprintf "add_var: %s not found" id);
              t
            end
          else do_add ()
      | Some itv ->
          assert (not (SS.mem id t.t_vars));
          let t = do_add () in
          { t with t_num = N.set_interv id itv t.t_num }
    (* Enforcing a given environment *)
    let vars_force (vars: SS.t) (t: t): t =
      (assert (SS.subset t.t_vars vars));
      SS.fold (fun s a -> add_var s None a) (SS.diff vars t.t_vars) t

    (** Abstract lattice operations *)
    let is_bot (t: t): bool =
      N.is_bot t.t_num
    type upper_bnd_kind = Ub_join | Ub_widen
    let gen_upper_bnd (u: upper_bnd_kind) (contvars: SS.t) (t0: t) (t1: t): t =
      let vars = SS.union t0.t_vars t1.t_vars in
      let t0 = vars_force vars t0
      and t1 = vars_force vars t1 in
      let deps =
        SM.fold
          (fun i d0 acc ->
            let d1 = try SM.find i t1.t_deps with Not_found -> SS.empty in
            let du = SS.union d0 d1 in
            SM.add i du acc
          ) t0.t_deps t1.t_deps in
      let tnum, p_op =
        match u with
        | Ub_join  -> N.join t0.t_num t1.t_num , P.join
        | Ub_widen -> N.widen t0.t_num t1.t_num, P.widen in
      let tr = if t0.t_trace = t1.t_trace then t0.t_trace else T.top in
      { t_istop  = t0.t_istop || t1.t_istop;
        t_vars   = t0.t_vars;
        t_fin    = SS.union t0.t_fin t1.t_fin;
        t_deps   = deps;
        t_smooth = p_op t0.t_smooth t1.t_smooth;
        t_num    = tnum;
        t_trace  = tr }
    let union = gen_upper_bnd Ub_join
    let widen = gen_upper_bnd Ub_widen SS.empty
    let is_le (t0: t) (t1: t): bool =
      try
        if not (SS.equal t0.t_vars t1.t_vars) then raise NotLE;
        if not (SS.subset t0.t_fin t1.t_fin) then raise NotLE;
        if not (sssm_isle t0.t_deps t1.t_deps) then raise NotLE;
        if not (N.is_le t0.t_num t1.t_num) then raise NotLE;
        if not (P.is_le t0.t_smooth t1.t_smooth) then raise NotLE;
        true
      with NotLE -> false

    (** Dependency analysis over expressions *)
    (* TODO: when evaluating an expression, extend the scope of the t *)
    let rec a_expr (t: t) (e: expr): t * SS.t =
      match e with
      | Nil | True | False | Num _ | Str _ ->
          t, SS.empty
      | Name id ->
          begin
            try t, SM.find id t.t_deps
            with Not_found -> add_var id None t, SS.singleton id
          end
      | UOp (_, e0) ->
          a_expr t e0
      | BOp (_, e0, e1)
      | Comp (_, e0, e1) ->
          (* TODO: if errors, propage these cases in the error dep component *)
          let t, d0 = a_expr t e0 in
          let t, d1 = a_expr t e1 in
          t, SS.union d0 d1
      | List _ ->
          failwith "list"
      | Dict _ ->
          failwith "dict"
      | StrFmt _ ->
          failwith "str format"

    (** Abstract operations for post-conditions *)
    (* Assignment:
     *  g:  SS.t    conditioning variables *)
    let assign (g: SS.t) (id: string) (e: expr) (t: t): t =
      if !dbg_verbose then
        Printf.printf "start assign %s ::= %a\n" id Ir_util.pp_expr e;
      let vars = t.t_vars in
      (* add target variables, if not already there *)
      let t = add_var id None t in
      (* dependence analysis over the expression + addition of expr vars *)
      let t, dep_e = a_expr t e in
      (* computation of smoothness information *)
      let smooth =
        let comp_dep e = snd (a_expr t e) in
        flush stdout;
        P.assign vars g comp_dep (N.get_expr_interv t.t_num)
          id e t.t_smooth in
      (* Updating all information *)
      { t with
        t_deps   = SM.add id (SS.union g dep_e) t.t_deps;
        t_num    = N.assign id e t.t_num;
        t_smooth = smooth;
        t_trace  = T.assign id e t.t_trace }
    (* Condition test *)
    let guard (e: expr) (t: t): t =
      (* TODO: for now the computation of dependency is elsewhere *)
      { t with
        t_num = N.guard e t.t_num }
    let drop_info (msg: string) t =
      Printf.printf "DROPPING INFORMATION: %S\n" msg;
      { t with
        t_istop = true;
        t_trace = T.invalidate "drop_info" t.t_trace }

    (** Experimental code *)
    let exp_merge (e: expr) (m: (expr * expr) SM.t) (t: t): SS.t =
      (* 1. make boundary *)
      let num_boundary =
        match e with
        | Comp ((Eq | NotEq | Lt | LtE | Gt | GtE), e0, e1) ->
            let ebnd = Comp (Eq, e0, e1) in
            Printf.printf "Boundary, adding constraint: %a\n"
              Ir_util.pp_expr ebnd;
            N.guard ebnd t.t_num
        | _ -> t.t_num in
      (* 2. iterate over variables *)
      let cont_set =
        SM.fold
          (fun v (e0, e1) acc ->
            let b = N.sat (Comp (Eq, e0, e1)) num_boundary in
            Printf.printf "For variable %s: %a=%a => %b\n" v
              Ir_util.pp_expr e0 Ir_util.pp_expr e1 b;
            if b then SS.add v acc else acc
          ) m SS.empty in
      (* 3. return variables for which continuity holds *)
      cont_set

    (** Abstract interpreter *)
    let rec a_atomic (g: SS.t) (a: acmd) (t: t): t =
      (* Tracing *)
      if !dbg_verbose then
        Printf.printf "Analysis of:\n%a\nIn state:\n%a\n" Ir_util.pp_acmd a
          (pp "  ") t;
      flush stdout;
      (* Computation of abstract post-conditon, induction over the syntax *)
      match a with
      | Assert _ ->
          (* consider as error cases *)
          warn_todo (Printf.sprintf "assert: %s" (Ir_util.acmd_to_string a));
          drop_info "assert" t
      | Assume e ->
          begin
            match e with
            | True -> t
            | _ ->
                (* ignore for now *)
                todo "assume (non True case)"
          end
      | Assn (id, e) ->
          assign g id e t
      | AssnCall (id, ef, al, _) ->
          let t =
            let module M = struct exception Stop end in
            try
              match ef with
              | Name "assumerange" ->
                  let float_of_num = function
                    | Int i -> float_of_int i
                    | Float f -> f in
                  let rec float_of_expr = function
                    | Num n -> float_of_num n
                    | BOp (Add, f0, f1) -> float_of_expr f0 +. float_of_expr f1
                    | BOp (Sub, f0, f1) -> float_of_expr f0 -. float_of_expr f1
                    | BOp (Mult, f0, f1) -> float_of_expr f0 *. float_of_expr f1
                    | _ -> raise M.Stop in
                  let fmin, fmax =
                    match List.map float_of_expr al with
                    | [ a ; b ] -> a, b
                    | _ -> raise M.Stop in
                  add_var id (Some (Some fmin, Some fmax)) t
              | _ -> raise M.Stop
            with M.Stop ->
              (* ignore for now *)
              warn_todo (Printf.sprintf "call: %s, %s"
                           (Ir_util.expr_to_string ef)
                           (Ir_util.acmd_to_string a));
              drop_info "assncall" t in
          { t with t_trace = T.invalidate "call" t.t_trace }
      | Sample (id, _, d, l, obs) ->
          let t = add_var id None t in
          assert (obs = None); (* should not be an observe (unsupported) *)
          Printf.printf "Sample: %s [%d]\n" id (List.length l);
          begin
            match d, l with
            | (Uniform None, [ ]), [ e0 ; e1 ] ->
                let inf = fst (N.get_expr_interv t.t_num e0) in
                let sup = snd (N.get_expr_interv t.t_num e1) in
                let t, dep0 = a_expr t e0 in
                let t, dep1 = a_expr t e1 in
                let deps = SS.union g (SS.union dep0 dep1) in
                let range = inf, sup in
                { t with
                  t_deps   = SM.add id deps t.t_deps ;
                  t_smooth = P.reinit id t.t_smooth ;
                  t_num    = N.set_interv id range t.t_num ;
                  t_trace  = T.invalidate "sample" t.t_trace }
            | (dk, l), a ->
                unsup (Printf.sprintf "distribution %s %d %d"
                         (Ir_util.dist_kind_to_string dk) (List.length l)
                         (List.length a))
          end
    and a_stmt (g: SS.t) (s: stmt) (t: t): t =
      match s with
      | Atomic a ->
          a_atomic g a t
      | If (c, b0, b1) ->
          let t, c_dep = a_expr t c in
          let t0in = guard c t
          and t1in = guard (UOp (Not, c)) t in
          (*
            let t0in = { t0in with t_trace = T.start_if t0in.t_trace } in
            let t1in = { t1in with t_trace = T.start_if t1in.t_trace } in
            *)
          begin
            match is_bot t0in, is_bot t1in with
            | false, false ->
                (* either branch may be taken *)
                let g = (*SS.union g c_dep*) g in
                let t0out = a_block g b0 { t0in with
                                           t_deps   = D.id t0in.t_deps;
                                           t_trace  = T.start_if t0in.t_trace;
                                           t_smooth = P.id t.t_smooth }
                and t1out = a_block g b1 { t1in with
                                           t_deps   = D.id t1in.t_deps;
                                           t_trace  = T.start_if t1in.t_trace;
                                           t_smooth = P.id t.t_smooth } in
                let eqs = T.exp_continuity_merge t0out.t_trace t1out.t_trace in
                let contvars = exp_merge c eqs t in
                (* temporary and not clean;
                 *  looking for a hack to enforce continuity of vars
                 * => composition issue to recovers info about contvars *)
                let t0out = { t0out with t_trace = T.end_if t0out.t_trace }
                and t1out = { t1out with t_trace = T.end_if t1out.t_trace } in
                let tjoin = union contvars t0out t1out in
                { tjoin with
                  t_deps   = D.compose t.t_deps tjoin.t_deps;
                  t_smooth = P.compose t.t_smooth tjoin.t_smooth }
            | true, false ->
                (* true branch unreachable, only false branch may be taken *)
                a_block g b1 t1in
            | false, true ->
                (* false branch unreachable, only true branch may be taken *)
                a_block g b0 t0in
            | true, true -> failwith "all branches bottom"
          end
      | For (_, _, _) ->
          (* TODO: for loops build the range in a temporary;
           * see how to support this indirection (can we use the same algo ?) *)
          todo "for"
      | While (c, b0) ->
          (* TODO: check nested loops and alternations of conditions
           * and loops *)
          (* Abstract fixpoint *)
          let t = { t with t_trace = T.invalidate "while" t.t_trace } in
          let rec iter g t =
            (* TODO: figure out similar precision improvement as for if *)
            let t, c_dep = a_expr t c in
            Printf.printf "Iter:\n%a\n" (pp "  ") t;
            let gin = SS.union g c_dep in
            let tin = { t with
                        t_fin = SS.union t.t_fin c_dep } in
            let tin = guard c tin in
            let tout = a_block gin b0 tin in
            let tu = widen t tout in
            if is_le tu t then
              t
            else iter gin tu
          in
          let t = iter g t in
          guard (UOp (Not, c)) t
      | With (_, _) ->
          unsup "with statement"
      | Break | Continue ->
          unsup "control (break|continue) statement"
    and a_block (g: SS.t) (b: block) (t: t): t =
      let t =
        (* TODO: check whether g should also vary ! *)
        List.fold_left
          (fun t s ->
            a_stmt g s t
          ) t b in
      t

    (** Analysis of a complete program *)
    let a_prog (b: block) =
      a_block SS.empty b emp

    (** Checker for analysis results *)
    let checker (t: t) (ao: analysis_outcome): bool =
      match ao with
      | CEx , PDep l -> sssm_eq t.t_deps (sssm_of_list l)
      | CInc, PDep l -> sssm_isle t.t_deps (sssm_of_list l)
      | _   , PCDf _
      | _   , PLip _ -> P.checker_predicate t.t_vars ao t.t_smooth
  end


let analyze (dn: num_domain) (p: smoothness_property) (ir: prog):
    analysis_output =
  let build_apron_dom mod_apron_mgr =
    let module AMgr = (val mod_apron_mgr: APRON_MGR) in
    let module ANum = A_Num_Apron_Make( AMgr ) in
    (module ANum: ADOM_NUM) in
  let mod_anum =
    match dn with
    | Nd_none -> (module A_Num_None: ADOM_NUM)
    | Nd_box  -> build_apron_dom (module Apron_util.PA_box: APRON_MGR)
    | Nd_oct  -> build_apron_dom (module Apron_util.PA_oct: APRON_MGR)
    | Nd_pol  -> build_apron_dom (module Apron_util.PA_pol: APRON_MGR) in
  let mod_smoothness =
    match p with
    | Sp_dep       -> (module ADsSmooth:    ADOM_SMOOTHNESS)
    | Sp_cont_diff -> (module ADsDiff:      ADOM_SMOOTHNESS)
    | Sp_lipschitz -> (module ADsLipschitz: ADOM_SMOOTHNESS) in
  let module ANum = (val mod_anum: ADOM_NUM) in
  let module ASmooth = (val mod_smoothness: ADOM_SMOOTHNESS) in
  let module SA = Analyzer( ANum )( ASmooth ) in
  Printf.printf "Analysis of program:\n%a" Ir_util.pp_prog ir;
  let r = SA.a_prog ir in
  Printf.printf "Result:\n%a" (SA.pp "") r;
  { aout_check = SA.checker r;
    aout_buf_t = (fun ind buf () -> SA.buf_t ind buf r);
    aout_pp    = (fun ind ch () -> SA.pp ind ch r) }
