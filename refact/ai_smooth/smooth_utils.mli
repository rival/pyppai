(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** GNU General Public License
 **
 ** Authors:
 **  Wonyeol Lee, KAIST
 **  Xavier Rival, INRIA Paris
 **  Hongseok Yang, KAIST
 **  Hangyeol Yu, KAIST
 **
 ** Copyright (c) 2019 KAIST and INRIA Paris
 **
 ** smooth_utils.mli: utilities for smoothness analyses *)
open Lib

open Smooth_sig

(** Debugging *)
val dbg_verbose: bool ref

(** Exception for speeding up inclusion checking *)
exception NotLE

(** Basic functions over maps and sets over strings *)
(* Construction from lists *)
val ss_of_list: string list -> SS.t
val sssm_of_list: (string * string list) list -> SS.t SM.t
(* Basic pretty-printing *)
val buf_sssm: string -> Buffer.t -> SS.t SM.t -> unit
val pp_sssm: string -> out_channel -> SS.t SM.t -> unit
(* Checking inclusion *)
(*  returns true iff for all x in m0, m0(x) is empty or <= m1(x) *)
val sssm_isle: SS.t SM.t -> SS.t SM.t -> bool
(* Checking equality *)
val sssm_eq: SS.t SM.t -> SS.t SM.t -> bool

(** Basic functions over intervals *)
(* Printing *)
val buf_interv: Buffer.t -> interv -> unit
val pp_interv: out_channel -> interv -> unit
(* Absolute value bounds (left bound is neccessarily a positive value)
 * maybe we can do better and detect empty intervals; in that case the
 * left bound could be None too... *)
val abs_bounds_interv: interv -> float * float option
(* Computes a coefficient bounding values in an interval
 * (None = no bound, unbounded interval) *)
val abs_interv: interv -> float option
