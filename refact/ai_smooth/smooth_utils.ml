(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** GNU General Public License
 **
 ** Authors:
 **  Wonyeol Lee, KAIST
 **  Xavier Rival, INRIA Paris
 **  Hongseok Yang, KAIST
 **  Hangyeol Yu, KAIST
 **
 ** Copyright (c) 2019 KAIST and INRIA Paris
 **
 ** smooth_utils.ml: utilities for smoothness analyses *)
open Lib

open Smooth_sig

(** Debugging *)
let dbg_verbose = ref true

(** Exception for speeding up inclusion checking *)
exception NotLE

(** Basic functions over maps and sets over strings *)
(* Construction from lists *)
let ss_of_list (l: string list): SS.t =
  List.fold_left (fun a x -> SS.add x a) SS.empty l
let sssm_of_list (l: (string * string list) list): SS.t SM.t =
  List.fold_left (fun a (v, s) -> SM.add v (ss_of_list s) a) SM.empty l
(* Basic pretty-printing *)
let buf_sssm (ind: string) (buf: Buffer.t) (m: SS.t SM.t): unit =
  SM.iter
    (fun x s ->
      Printf.bprintf buf "%s%s => %a\n" ind x buf_ss s
    ) m
let pp_sssm ind = buf_to_channel (buf_sssm ind)
(* Checking inclusion *)
(*  returns true iff for all x in m0, m0(x) is empty or <= m1(x) *)
let sssm_isle (m0: SS.t SM.t) (m1: SS.t SM.t): bool =
  try
    SM.iter
      (fun x s0 ->
        let s1 = try SM.find x m1 with Not_found -> SS.empty in
        if not (SS.subset s0 s1) then raise NotLE
      ) m0;
    true
  with NotLE -> false
(* Checking equality *)
let sssm_eq (m0: SS.t SM.t) (m1: SS.t SM.t): bool =
  sssm_isle m0 m1 && sssm_isle m1 m0


(** Basic functions over intervals *)
(* Printing *)
let fbound_to_string = function
  | None -> "+oo"
  | Some f -> string_of_float f
let buf_interv (buf: Buffer.t) ((bl, br): interv): unit =
  match bl, br with
  | None, None -> Printf.bprintf buf "]-oo,+oo["
  | None, Some fr -> Printf.bprintf buf "]-oo,%f]" fr
  | Some fl, None -> Printf.bprintf buf "[%f,+oo[" fl
  | Some fl, Some fr -> Printf.bprintf buf "[%f,%f]" fl fr
let pp_interv = buf_to_channel buf_interv
(* Absolute value bounds (left bound is neccessarily a positive value)
 * maybe we can do better and detect empty intervals; in that case the
 * left bound could be None too... *)
let abs_bounds_interv (i: interv): float * float option =
  match i with
  | None, None -> 0., None
  | None, Some r -> if r < 0. then 0. -. r, None else 0., None
  | Some l, None -> if l > 0. then l, None else 0., None
  | Some l, Some r ->
      let l = Float.abs l and r = Float.abs r in
      min l r, Some (max l r)
(* Computes a coefficient bounding values in an interval
 * (None = no bound, unbounded interval) *)
let abs_interv (i: interv): float option =
  match i with
  | None, None -> None
  | Some f, None | None, Some f -> Some (Float.abs f)
  | Some l, Some r -> Some (max (Float.abs l) (Float.abs r))
