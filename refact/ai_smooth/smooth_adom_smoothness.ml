(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** GNU General Public License
 **
 ** Authors:
 **  Wonyeol Lee, KAIST
 **  Xavier Rival, INRIA Paris
 **  Hongseok Yang, KAIST
 **  Hangyeol Yu, KAIST
 **
 ** Copyright (c) 2019 KAIST and INRIA Paris
 **
 ** smooth_adom_smoothness.ml:
 **                     abstract domains for smoothness analysis
 **                     smoothness abstract domains component *)
open Lib

open Ir_sig
open Smooth_sig

open Smooth_utils


(** Debugging *)
let loc_debug = true


(** Only dependency abstraction
 **  As it is already in the core domain, there is nothing more to track *)
module ADsSmooth =
  (struct
    (** Name of the property being tracked *)
    let property: string = "Dependency"
    let need_pp: bool    = false

    (** Abstract elements *)
    type t = unit

    (** Pretty-printing *)
    let buf_t (_: string) (_: SS.t) (_: Buffer.t) (_: t): unit = ()
    let pp (ind: string) (vars: SS.t) = buf_to_channel (buf_t ind vars)

    (** Lattice operations *)
    let id () = ()
    let top: t = ()
    let join (_: t) (_: t): t = ()
    let widen (_: t) (_: t): t = ()
    let is_le (_: t) (_: t): bool = true

    (** Abstract operations *)
    let add_var (_: string) (_: t): t = ()
    let assign (_: SS.t) (_: SS.t) (_: expr -> SS.t) (_: expr -> interv)
        (_: string) (_: expr) (_: t): t = ()
    let reinit (_: string) (_: t): t = ()
    let drop (_: string) (_: SS.t) (_: t): t = ()
    let compose _ _ = ()

    (** Getting results and checking them *)
    let checker_cont_diff (_: SS.t) (_: check_kind) (_: SS.t SM.t) (_: t) =
      false
    let checker_predicate (_: SS.t) (_: check_kind * predicate) (_: t): bool =
      false
  end: ADOM_SMOOTHNESS)


(** Continuous differentiability *)
module ADsDiff =
  (struct
    (** Name of the property being tracked *)
    let property = "Continuous differentiability"
    let need_pp = true

    (** Abstract elements *)
    (* This maps associates a variable x to the set of variables
     * with respect to which x may NOT be continuously differentiable *)
    type t = SS.t SM.t

    (** Pretty-printing *)
    (* Pretty printing to buffer takes extra arguments:
     *  - indentation
     *  - set of all variables (for the diff) *)
    let buf_t (ind: string) (vars: SS.t) (buf: Buffer.t) (t: t): unit =
      SM.iter
        (fun id vars_ncd ->
          let vars_cd = SS.diff vars vars_ncd in
          Printf.bprintf buf "%s%s => %a (Not: %a)\n" ind id buf_ss vars_cd
            buf_ss vars_ncd
        ) t
    (* Same arguments as buf_t *)
    let pp (ind: string) (vars: SS.t): out_channel -> t -> unit =
      buf_to_channel (buf_t ind vars)

    (** Lattice operations *)
    let id _ = SM.empty
    let top = SM.empty
    let join (t0: t) (t1: t): t =
      SM.fold
        (fun i nd0 acc ->
          let nd1 = try SM.find i t1 with Not_found -> SS.empty in
          let nd = SS.union nd0 nd1 in
          SM.add i nd acc
        ) t0 t1
    let widen (t0: t) (t1: t): t = join t0 t1
    let is_le (t0: t) (t1: t): bool =
      (* TODO: check ! *)
      sssm_isle t1 t0

    (** Abstract operations *)
    (* Add a new variable;
     *       initially not modified, so "equal to its initial value" *)
    let add_var (id: string) (t: t): t =
      let nd = try SM.find id t with Not_found -> SS.empty in
      SM.add id nd t
    (* Assignment
     *  vars:  SS.t
     *    all variables
     *  cvars:
     *    conditioning variables
     *  compute_dep:
     *    function to compute the depencency of a sub-expression
     *    (e.g., for the division)
     *  - TODO: we probably need to add more like this to handle
     *    product domain implementation, and e.g., for the Lipschitzness *)
    let assign (vars: SS.t) (cvars: SS.t) (compute_dep: expr -> SS.t)
        (_: expr -> interv) (id: string) (e: expr) (t: t): t =
      let rec aux (e: expr): SS.t =
        match e with
        | Nil | True | False | Num _ | Str _ -> SS.empty
        | Name id ->
            begin
              try SM.find id t
              with Not_found -> SS.empty
            end
        | UOp (_, _) ->
            failwith "todo: cd, assign, uop"
        | BOp (bop, e0, e1) ->
            begin
              match bop with
              | Add | Sub | Mult ->
                  SS.union (aux e0) (aux e1)
              | Div ->
                  (* TODO: detect when we can prove denum is not zero *)
                  SS.union (SS.union (aux e0) (aux e1))
                    (compute_dep e1)
              | _ ->
                  failwith "todo: cd, assign, bop, other op"
            end
        | _ ->
            failwith "todo: P.assign" in
      let evars_ncd = aux e in
      let vars_ncd = SS.union cvars evars_ncd in
      if loc_debug then
        Printf.printf "Assign(CD) G = %a %s := %a\n%a => %a\n"
          ss_pp cvars
          id Ir_util.pp_expr e (pp "    " vars) t ss_pp evars_ncd;
      SM.add id vars_ncd t
    (* Reinitializes information for a variable; e.g. on non determinism *)
    let reinit (id: string) (t: t): t = SM.add id SS.empty t
    (* Dropping all information on variable id
     *   vars: is the set of all known variables *)
    let drop (id: string) (vars: SS.t) (t: t): t =
      if loc_debug then
        Printf.printf "Drop information on %s => %a\n" id ss_pp vars;
      let oldm = try SM.find id t with Not_found -> SS.empty in
      let newm = SS.union oldm vars in
      SM.add id newm t
    let compose _ _ = failwith "todo: composition (diff)"

    (** Getting results and checking them *)
    (* Checks continuous differentiability information
     *  vars:  SS.t
     *    all variables *)
    let checker_cont_diff (vars: SS.t) (ck: check_kind)
        (tgtres: SS.t SM.t) (t: t): bool =
      let outcome = SM.map (fun nd -> SS.diff vars nd) t in
      Printf.printf "status:\n%a\ngoal:\n%a" (pp_sssm "  ") outcome
        (pp_sssm "  ") tgtres;
      match ck with
      | CEx  -> sssm_eq outcome tgtres
      | CInc -> sssm_isle outcome tgtres

    (* Checking some predicate
     *  vars:  SS.t
     *    all variables
     *  k,p:
     *    predicate to check; dependency is ignored *)
    let checker_predicate (vars: SS.t) ((ck, pr): check_kind * predicate)
        (t: t): bool =
      match pr with
      | PDep _ | PLip _ -> false
      | PCDf l -> checker_cont_diff vars ck (sssm_of_list l) t
  end: ADOM_SMOOTHNESS)


(** Lipschitz continuity
 **   => this domain tries to infers coefficient for each variable *)
module ADsLipschitz =
  (struct
    (** Name of the property being tracked *)
    let property: string = "Lipschitz continuity"
    let need_pp: bool    = true

    (** Structure to describe a vector of k-Lipschitz predicates *)
    module BM =
      struct
        (** Abstraction of Lipschitzness information over a function f
         **  x => None    means no Lipschitz constant for x
         **  x => Some k  means f is k-Lipschitz wrt x
         **  x => absent  means f is 0-Lipschitz wrt x
         **       absent  also equivalent to Some 0.
         **)
        type t = float option SM.t

        (** Pretty-printing *)
        let buf (buf: Buffer.t) (m: float option SM.t): unit =
          let module M = struct exception Stop end in
          let is_const =
            try
              SM.iter
                (fun _ -> function
                  | None -> raise M.Stop
                  | Some i -> if i > 0. then raise M.Stop
                ) m;
              true
            with M.Stop -> false in
          if is_const then
            Printf.bprintf buf " {|Constant|}"
          else
            SM.iter
              (fun oid -> function
                | None   -> Printf.bprintf buf " oo.|%s|" oid
                | Some f -> if f > 0. then Printf.bprintf buf " %f.|%s|" f oid
              ) m
        let pp = buf_to_channel buf

        (** Operations *)
        (* Get Lipschitz coefficient for a variable *)
        let get_k (x: string) (t: t): float option =
          try SM.find x t with Not_found -> Some 0.
        (* Drops information on all coordinates *)
        let drop_info (t: t): t =
          SM.map (fun _ -> None) t
        (* Constant: 0-Lipschitz wrt all variables *)
        let constant: t = SM.empty
        (* Variable: 1-Lipschitz wrt itself; 0-Lipschitz for others *)
        let nvar (x: string): t = SM.add x (Some 1.) SM.empty
        (* Addition *)
        let add (t0: t) (t1: t): t =
          SM.fold
            (fun x k0 acc ->
              let k1 = get_k x t1 in
              let kr =
                match k0, k1 with
                | None, _ | _, None -> None
                | Some l0, Some l1 -> Some (l0 +. l1) in
              SM.add x kr acc
            ) t0 t1
        (* Multiplication by a constant *)
        let mul (k: float) (t: t): t =
          assert (0. <= k);
          SM.map
            (function
              | None -> if k = 0. then Some 0. else None
              | Some l -> Some (k *. l)
            ) t
        (* Multiplication by an interval:
         * - if either bound is oo, then drops all information on all
         *   variables for which some information is known
         * - otherwise, multiply everything withe greatest bound (in norm) *)
        let mul_interv (it: interv) (t: t): t =
          let bound = abs_interv it in
          match bound with
          | None ->
              Printf.printf "mult by int with no bound %a\n" pp_interv it;
              (* no bound; drop all information on variables for which there
               * is a dependency *)
              SM.map (fun k -> if k = Some 0. then k else None) t
          | Some b ->
              Printf.printf "mult by int with bound %f\n" b;
              if b = 0. then
                (* all coefficients become 0 *)
                SM.empty
              else
                SM.map (Option.map (fun x -> x *. b)) t
         (* Guard:
         *  turns a t into oo-Lipschitz for all conditioning variables *)
        let guard (g: SS.t) (t: t): t =
          SS.fold (fun x acc -> SM.add x None acc) g t
        (* Intersection: for each variable returns most precise information *)
        let inter (t0: t) (t1: t): t =
          (* make sure that for any variable constrained into t1, it is also
           * constrained in t0 as well; if needed, make it explicit with
           * Some 0 *)
          let t0 =
            SM.fold
              (fun x _ acc ->
                if SM.mem x acc then acc
                else SM.add x (Some 0.) acc
              ) t1 t0 in
          SM.fold
            (fun x k0 acc ->
              let k1 = get_k x t1 in
              let ku =
                match k0, k1 with
                | None, k | k, None -> k
                | Some l0, Some l1 -> Some (min l0 l1) in
              SM.add x ku acc
            ) t0 t0
      end

    (** Abstract elements *)
    (* An abstract value maps each variable x to bounds of it wrt the others,
     * Namely:
     * - when x is not in the abstract state, then x is not a known
     *   variable at this point, which means that it is 1,x Lipschitz
     *   and 0,y Lipschitz for all y!=x
     * - when x is found in the abstract state and mapped to map m:
     *   - if y is in m, then m(y) is
     *     - None, if no Lipschitz constant of x=f(y,...) can be found;
     *       (i.e., any variation of y may cause an unbounded variation of x)
     *     - Some k if x=f(y,...) is k-Lipschitz in y
     *   - if y is not in m, then x is 0,y Lipschitz (constant wrt y)
     *     (this case is equivalent to the case where y is mapped to (Some 0.))
     *)
    type t = BM.t SM.t

    (** Pretty-printing *)
    let buf_t (ind: string) (_: SS.t) (buf: Buffer.t) (t: t): unit =
      SM.iter
        (fun id m ->
          Printf.bprintf buf "%s%s:%a\n" ind id BM.buf m
        ) t
    let pp (ind: string) (vars: SS.t) = buf_to_channel (buf_t ind vars)

    (** Lattice operations *)
    let id (t: t): t =
      SM.mapi (fun x _ -> SM.add x (Some 1.) SM.empty) t
    let top: t = SM.empty
    let join (t0: t) (t1: t): t =
      if !dbg_verbose then
        Printf.printf "JOIN:\n(l):\n%a\n(r):\n%a\n"
          (pp "  " SS.empty) t0 (pp "  " SS.empty) t1;
      let homogeneise (tref: t) (t: t): t =
        SM.fold
          (fun id _ (acc: t) ->
            (* when not found; id does not exist in that map; it should be
             * viewed as 1-lipschitz wrt itself *)
            if SM.mem id t then acc
            else SM.add id (BM.nvar id) acc
          ) tref t in
      let t0 = homogeneise t1 t0 in
      let t1 = homogeneise t0 t1 in
      let tj =
        SM.fold
          (fun id m0 acc ->
            let m1 =
              try SM.find id t1
              with Not_found -> failwith "join: corrupt state" in
            let mu =
              SM.fold
                (fun x k0 acc ->
                  let k1 = try SM.find x m1 with Not_found -> Some 0. in
                  let ku =
                    match k0, k1 with
                    | None, _ | _, None -> None
                    | Some l0, Some l1 -> Some (max l0 l1) in
                  SM.add x ku acc
                ) m0 m1 in
            SM.add id mu acc
          ) t0 t1 in
      if !dbg_verbose then
        Printf.printf "JOIN:\n%a\n" (pp "  " SS.empty) tj;
      tj
    let widen (t0: t) (t1: t): t =
      let get_keys t acc = SM.fold (fun a _ -> SS.add a) t acc in
      let keys = get_keys t0 (get_keys t1 SS.empty) in
      SS.fold
        (fun x acc ->
          match SM.find_opt x t0, SM.find_opt x t1 with
          | None, None -> failwith "unbound non-mem key case"
          | None, Some bm
          | Some bm, None -> SM.add x bm acc
          | Some bm0, Some bm1 ->
              let bm =
                SM.fold
                  (fun y k0 acc ->
                    let k1 = try SM.find y bm1 with Not_found -> Some 0. in
                    let k =
                      match k0, k1 with
                      | None, _ | _, None -> None
                      | Some l0, Some l1 -> if l1 <= l0 then k0 else None in
                    SM.add y k acc
                  ) bm0 SM.empty in
              let bm =
                SM.fold
                  (fun y k1 bm ->
                    if SM.mem y bm0 then bm
                    else
                      match k1 with
                      | None -> SM.add y None bm
                      | Some l -> SM.add y (if l = 0. then Some 0. else None) bm
                  ) bm1 bm in
              SM.add x bm acc
        ) keys SM.empty
    let is_le (t0: t) (t1: t): bool =
      if !dbg_verbose then
        Printf.printf "ISLE:\n(l):\n%a\n(r):\n%a\n"
          (pp "  " SS.empty) t0 (pp "  " SS.empty) t1;
      try
        SM.iter
          (fun x bm0 ->
            let bm1 = try SM.find x t1 with Not_found -> SM.empty in
            SM.iter
              (fun y ok0 ->
                let ok1 = try SM.find y bm1 with Not_found -> Some 0. in
                match ok0, ok1 with
                | _, None -> ( )
                | None, Some f ->
                    (*Printf.printf "NS(%s,%s,%f)\n" x y f;*)
                    raise NotLE
                | Some v0, Some v1 ->
                    (*if v0 > v1 then
                      Printf.printf "SS(%s,%s,%f,%f)\n" x y v0 v1;*)
                    if v0 > v1 then raise NotLE
              ) bm0
          ) t0;
        true
      with NotLE -> false

    (** Abstract operations *)
    (* Get all variables *)
    let get_vars (t: t): SS.t =
      SM.fold
        (fun x bm acc ->
          SM.fold (fun v _ -> SS.add v) bm (SS.add x acc)
        ) t SS.empty
    (* Add a new variable;
     *       initially not modified, so "equal to its initial value" *)
    let add_var (id: string) (t: t): t =
      let vars = SS.add id (get_vars t) in
      (* all other variables are 0-lipschitz wrt id *)
      if false then Printf.printf "Beforeadd:\n%a\n" (pp "  " vars) t;
      let t = SM.map (fun m -> SM.add id (Some 0.) m) t in
      if false then Printf.printf "Afteradd:\n%a\n" (pp "  " vars) t;
      (* id is 1-lipschitz wrt itself *)
      SM.add id (SM.add id (Some 1.) SM.empty) t
    (* Assignment
     *  vars:  SS.t
     *    all variables
     *  cvars: SS.t
     *    conditioning variables
     *  compute_dep: expr -> SS.t
     *    function to compute the depencency of a sub-expression
     *    (e.g., for the division)
     *)
    let assign (vars: SS.t) (gvars: SS.t)
        (_: expr -> SS.t) (compute_interv: expr -> interv)
        (id: string) (e: expr) (t: t): t =
      let rec aux_expr (e: expr): BM.t =
        let r = aux_expr_x e in
        Printf.printf "Expr %s => %a\n" (Ir_util.expr_to_string e) BM.pp r;
        r
      and aux_expr_x e =
        let fail s =
          Printf.printf "Expr (%s): %s\n" s (Ir_util.expr_to_string e);
          failwith "todo: expr" in
        match e with
        | Num _ -> (*BM.constant ()*)
            (* TODO: this case is very obscure *)
            (*SS.fold (fun x acc -> SM.add x (Some 0.) acc) okvars SM.empty*)
            SM.empty
        | Name x ->
            begin
              (* if x know, find the information about it;
               * otherwise, add x, with x=f(x,...) 1-lipschitz in x *)
              try SM.find x t
              with Not_found -> BM.nvar x (* new variable *)
            end
        | BOp (o, e0, e1) ->
            let bm0 = aux_expr e0 and bm1 = aux_expr e1 in
            begin
              match o with
              | Add | Sub -> BM.add bm0 bm1
              | Mult ->
                  let it0 = compute_interv e0 and it1 = compute_interv e1 in
                  BM.add (BM.mul_interv it0 bm1) (BM.mul_interv it1 bm0)
              | Div ->
                  let it0 = compute_interv e0 and it1 = compute_interv e1 in
                  let a0, ob0 = abs_bounds_interv it0
                  and a1, ob1 = abs_bounds_interv it1 in
                  let vars = SM.fold (fun x _ -> SS.add x) t SS.empty in
                  let fone (l0: float option) (b0: float option)
                      (l1: float option) (a1: float): float option =
                    if a1 > 0. then
                      match l0, b0, l1 with
                      | None, _, _ -> None
                      | Some l0, None, None -> None
                      | Some l0, Some f, None
                      | Some l0, None, Some f ->
                          if f = 0. then
                            Some (l0 /. a1)
                          else None
                      | Some l0, Some b0, Some l1 ->
                          Some (l0 /. a1 *. (1. +. l1 *. b0 /. a1))
                    else
                      match l0, l1 with
                      | Some l0, Some l1 ->
                          if l0 = 0. && l1 = 0. then Some 0.
                          else None
                      | _, _ -> None in
                  SS.fold
                    (fun x acc ->
                      let ok0 = SM.find_opt x bm0
                      and ok1 = SM.find_opt x bm1 in
                      match ok0, ok1 with
                      | Some k0, Some k1 ->
                          let ok =
                            match fone k0 ob0 k1 a1, fone k1 ob1 k0 a0 with
                            | Some ka, Some kb -> Some (min ka kb)
                            | Some k, None | None, Some k -> Some k
                            | None, None -> None in
                          SM.add x ok acc
                      | Some k, None ->
                          SM.add x (fone k ob0 None a1) acc
                      | None, Some k ->
                          SM.add x (fone k ob1 None a0) acc
                      | None, None -> acc
                    ) vars SM.empty
              | _ -> fail "operator"
            end
        | _ -> fail "other case" in
      let r = aux_expr e in
      let r = BM.guard gvars r in
      if !dbg_verbose then
        Printf.printf "Assign: %s = %a\n%a\n => %a\n" id Ir_util.pp_expr e
          (pp "  " vars) t BM.pp r;
      SM.add id r t
    (* Reinitializes information for a variable; e.g. on non determinism *)
    let reinit (id: string) (t: t): t = SM.remove id t
    (* Dropping all information on variable id *)
    let drop (id: string) (_: SS.t) (_: t): t =
      failwith "todo: drop"
    let compose (t0: t) (t1: t): t =
      Printf.printf "Composition called:\n%a\n%a"
        (pp "  " SS.empty) t0 (pp "" SS.empty) t1;
      let tc =
        SM.fold
          (fun v bm acc ->
            let nbm =
              SM.fold
                (fun y kopt bacc ->
                  match kopt with
                  | None ->
                      (* no bound: TODO *)
                      failwith "todo: composition (lipschitzness), no limit"
                  | Some k ->
                      let bm0 =
                        try SM.find y t1
                        with Not_found -> BM.nvar y in
                      BM.add bacc (BM.mul k bm0)
                ) bm BM.constant in
            SM.add v nbm acc
          ) t1 t0 in
      Printf.printf "Result:\n%a\n" (pp "" SS.empty) tc;
      tc

    (** Getting results and checking them *)
    (* Checking some predicate
     *  vars:  SS.t
     *    all variables
     *  k,p:
     *    predicate to check; dependency is ignored *)
    let checker_predicate (_: SS.t) (p: check_kind * predicate) (t: t): bool =
      let module M = struct exception Stop end in
      let test_consume l =
        List.fold_left
          (fun acc (v, ctr) ->
            let bm = try SM.find v t with Not_found -> BM.constant in
            let bm =
              List.fold_left
                (fun acc (w, l) ->
                  let k = try SM.find w bm with Not_found -> Some 0. in
                  match k, l with
                  | None, None -> SM.remove w acc
                  | Some _, None -> acc
                  | None, Some _ -> raise M.Stop
                  | Some kf, Some lf ->
                      if kf > lf then raise M.Stop
                      else if kf = lf then SM.remove w acc
                      else acc
                ) bm ctr in
            if bm = SM.empty then SM.remove v acc
            else SM.add v bm acc
          ) t l in
      match p with
      | _, PDep _ | _, PCDf _ -> false
      | CEx , PLip l ->
          begin
            try
              (* first, we try to discharge all constraints *)
              let t = test_consume l in
              (* second, we see which elements remains unconsummed *)
              SM.iter
                (fun v ->
                  SM.iter
                    (fun w -> function
                      | None ->
                          Printf.printf "failed check: %s <= oo %s\n" v w;
                          raise M.Stop
                      | Some f ->
                          if f > 0. then
                            begin
                              Printf.printf "failed check: %s <= %f %s\n" v f w;
                              raise M.Stop
                            end
                    )
                ) t;
              true
            with M.Stop -> false
          end
      | CInc, PLip l ->
          begin
            try
              ignore (test_consume l);
              true
            with M.Stop -> false
          end
  end: ADOM_SMOOTHNESS)
