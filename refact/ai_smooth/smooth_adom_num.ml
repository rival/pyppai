(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** GNU General Public License
 **
 ** Authors:
 **  Wonyeol Lee, KAIST
 **  Xavier Rival, INRIA Paris
 **  Hongseok Yang, KAIST
 **  Hangyeol Yu, KAIST
 **
 ** Copyright (c) 2019 KAIST and INRIA Paris
 **
 ** smooth_adom_num.ml: abstract domains for smoothness analysis
 **                     numerical abstract domains component *)
open Lib

open Ir_sig
open Smooth_sig

open Apron
open Apron_sig
open Apron_util


(** Numerical domain with no information *)
(* TODO! *)
module A_Num_None =
  (struct
    (** Abstract elements and main definitions *)
    type t = unit

    (** Pretty-printing *)
    let buf_t (ind: string) (buf: Buffer.t) (_: t): unit =
      Printf.bprintf buf "%s<None>\n" ind
    let pp (ind: string) = buf_to_channel (buf_t ind)

    (** Lattice operations *)
    let is_bot (_: t) = false
    let top: t = ()
    let join (_: t) (_: t): t = ()
    let widen (_: t) (_: t): t = ()
    let is_le (_: t) (_: t): bool = true

    (** Basic operations on dimensions *)
    let dim_add (_: string) (t: t): t = t

    (** Abstract operations *)
    (* Assignment *)
    let assign (_: string) (_: expr) (t: t): t = t
    (* Assignment to an unknown interval (for distributions) *)
    let set_interv (_: string) (_: interv) (t: t): t = t
    (* Condition test *)
    let guard (_: expr) (t: t): t = t
    (* Checks that a condition holds *)
    let sat (_: expr) (_: t): bool = false

    (** Getting information from the domain *)
    (* Interval of an expression; returns top *)
    let get_expr_interv (_: t) (e: expr): interv =
      match e with
      | Num n ->
          let f =
            match n with
            | Int i -> float_of_int i
            | Float f -> f in
          Some f, Some f
      | _ -> None, None
  end: ADOM_NUM)


(** Numerical domain based on Apron *)
module A_Num_Apron_Make = functor (Amg: APRON_MGR) ->
  (struct
    (** Abstract elements and main definitions *)
    module A = Apron.Abstract1
    let man = Amg.man
    type t = Amg.t A.t

    (** Utilities and bookkeeping *)
    (* Preparation of a condition for Apron *)
    let make_condition (e: expr) (t: t): Tcons1.t =
      Ir_util.make_apron_cond (A.env t) e

    (** Pretty-printing *)
    let buf_t (ind: string) (buf: Buffer.t) (t: t): unit =
      let vars =
        let _, vf = Apron.Environment.vars (A.env t) in
        vf in
      Printf.bprintf buf "%sEnv:" ind;
      let isfirst = ref true in
      Array.iter
        (fun id ->
          Printf.bprintf buf "%s %s" (if !isfirst then "" else ",")
            (Var.to_string id);
          isfirst := false;
        ) vars;
      Printf.bprintf buf "\n%a" (buf_linconsarray ind)
        (A.to_lincons_array man t)
    let pp (ind: string) = buf_to_channel (buf_t ind)

    (** Lattice operations *)
    let is_bot = A.is_bottom man
    let top: t =
      let env_empty = Environment.make [| |] [| |] in
      A.top man env_empty
    let join: t -> t -> t = A.join man
    let widen: t -> t -> t = A.widening man (* TODO: thresholds ! *)
    let is_le (t0: t) (t1: t): bool =
      A.is_leq man t0 t1

    (** Basic operations on dimensions *)
    let dim_add (id: string) (t: t): t =
      if false then Printf.printf "Adding variable %s in Apron\n" id;
      let var = make_apron_var id in
      let env_old = A.env t in
      let env_new =
        try Environment.add env_old [| |] [| var |]
        with e ->
          failwith (Printf.sprintf "dim_add: %s" (Printexc.to_string e)) in
      A.change_environment man t env_new false

    (** Abstract operations *)
    (* Assignment *)
    let assign (x: string) (e: expr) (t: t): t =
      if false then Printf.printf "about to do assignment...\n";
      (* convert the expression to Apron IR *)
      let lv = make_apron_var x
      and rv = Ir_util.make_apron_expr (A.env t) e in
      (* perform the Apron assignment *)
      A.assign_texpr_array man t [| lv |] [| rv |] None
    (* Assignment to an unknown interval (for distributions) *)
    let set_interv (x: string) ((finf, fsup): interv) (t: t): t =
      (* build an expression corresponding to the interval *)
      let lv = make_apron_var x
      and rv =
        let c =
          match finf, fsup with
          | None  , None   -> Coeff.i_of_float neg_infinity infinity
          | None  , Some s -> Coeff.i_of_float neg_infinity s
          | Some i, None   -> Coeff.i_of_float i            infinity
          | Some i, Some s -> Coeff.i_of_float i            s        in
        Texpr1.cst (A.env t) c in
      (* perform the Apron assignment *)
      A.assign_texpr_array man t [| lv |] [| rv |] None
    (* Condition test *)
    let guard (e: expr) (t: t): t =
      let eacons = Tcons1.array_make (A.env t) 1 in
      Tcons1.array_set eacons 0 (make_condition e t);
      let t = A.meet_tcons_array man t eacons in
      (* TODO: bottom reduction!!! *)
      t
    (* Checks that a condition holds *)
    let sat (e: expr) (t: t): bool =
      let econs = make_condition e t in
      if false then Printf.printf "Apron sat:\n%a\n" (pp " ") t;
      A.sat_tcons man t econs

    (** Getting information from the domain *)
    (* Interval of an expression; may return Top if expr cannot be
     * precisely interpreted in the underlying domain *)
    let get_expr_interv (t: t) (e: expr): interv =
      let ae = Ir_util.make_apron_expr (A.env t) e in
      let itv = A.bound_texpr man t ae in
      if false then
        begin
          flush stdout;
          Format.printf "Output interval: %a\n" Apron.Interval.print itv;
          flush stdout
        end;
      let do_bound b =
        match b with
        | Apron.Scalar.Float f -> Some f
        | Apron.Scalar.Mpqf q ->
            let mmax = Mpqf.of_float Float.max_float
            and mmin = Mpqf.of_float (0. -. Float.max_float) in
            if Mpqf.cmp mmin q >= 0 || Mpqf.cmp q mmax >= 0 then None
            else Some (Mpqf.to_float q)
        | Apron.Scalar.Mpfrf _ -> failwith "interval; mpfrf" in
      do_bound itv.inf, do_bound itv.sup
  end: ADOM_NUM)
