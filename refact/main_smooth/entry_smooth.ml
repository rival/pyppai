(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** GNU General Public License
 **
 ** Authors:
 **  Wonyeol Lee, KAIST
 **  Xavier Rival, INRIA Paris
 **  Hongseok Yang, KAIST
 **  Hangyeol Yu, KAIST
 **
 ** Copyright (c) 2019 KAIST and INRIA Paris
 **
 ** entry_smooth.ml: entry point for smoothness analysis *)
open Analysis_sig
open Smooth_sig

(** Main function for smoothness analysis:
 ** - by default, tries to process the guides and models of all the
 **   main Pyro examples
 ** - override mode: analyze a single example *)
let main () =
  let fopt: string option ref       = ref None
  and dnum: num_domain ref          = ref Nd_none
  and prop: smoothness_property ref = ref Sp_dep
  and kind: analysis_kind ref       = ref Ak_comp
  in
  let do_exp: bool ref = ref false in
  (* Setter functions *)
  let set_prop (s: string) =
    match s with
    | "contdiff"  -> prop := Sp_cont_diff
    | "lipschitz" -> prop := Sp_lipschitz
    | _ -> failwith "unbound property" in
  let set_cdiff  = Arg.Unit (fun () -> prop := Sp_cont_diff)
  and set_dep    = Arg.Unit (fun () -> prop := Sp_dep)
  and set_lipsch = Arg.Unit (fun () -> prop := Sp_lipschitz) in
  let set_dnum v = Arg.Unit (fun () -> dnum := v) in
  let set_kind k = Arg.Unit (fun () -> kind := k) in
  let set_verb   = Arg.Set Smooth_utils.dbg_verbose
  and clear_verb = Arg.Clear Smooth_utils.dbg_verbose in
  (* Argument parsing *)
  Arg.parse
    [ "-ai-none" , set_dnum Nd_none    , "Num analysis, { Top } domain" ;
      "-ai-box"  , set_dnum Nd_box     , "Num analysis, Apron, Boxes" ;
      "-ai-oct"  , set_dnum Nd_oct     , "Num analysis, Apron, Octagons" ;
      "-ai-pol"  , set_dnum Nd_pol     , "Num analysis, Apron, Polyhedra" ;
      "-ak-comp" , set_kind Ak_comp    , "Compositional analysis" ;
      "-ak-fwd"  , set_kind Ak_fwd_ai  , "Forward abstract interpretation" ;
      "-p"       , Arg.String set_prop , "Property to analyze" ;
      "-prop"    , Arg.String set_prop , "Property to analyze" ;
      "-dep"     , set_dep             , "Analysis of simple dependences" ;
      "-cdiff"   , set_cdiff           , "Analysis of cont. differentiability" ;
      "-lipsch"  , set_lipsch          , "Analysis of Lipschitzness" ;
      "-verb"    , set_verb            , "Turns on verbose output" ;
      "-silent"  , clear_verb          , "Turns off verbose output" ;
      "-exp"     , Arg.Set do_exp      , "Using experimental smoothness abst" ;
    ] (fun s -> fopt := Some s) "Smoothness property analysis";
  (* Parsing code *)
  let ir =
    match !fopt with
    | Some x ->
        Ir_parse.output := false ;
        Ir_parse.parse_code None (AI_pyfile x) |> Ir_util.simplify_delta_prog
    | None -> failwith "no file given" in
  (* Analysis launching *)
  let analyze =
    match !kind with
    | Ak_comp -> failwith "comp analysis: unimp"
    | Ak_fwd_ai ->
        if !do_exp then Ai_smooth_exp_ai.analyze
        else Ai_smooth_fwd_ai.analyze in
  ignore (analyze !dnum !prop ir)
let _ = ignore (main ( ))
