(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** GNU General Public License
 **
 ** Authors:
 **  Wonyeol Lee, KAIST
 **  Xavier Rival, INRIA Paris
 **  Hongseok Yang, KAIST
 **  Hangyeol Yu, KAIST
 **
 ** Copyright (c) 2021 KAIST and INRIA Paris
 **
 ** ddom_DIFF.ml: domains signatures for compositional information *)
open Lib

open Ddom_sig
open Ir_sig

module DD_none =
  (struct
    let name = "single point compositional abstraction; always returns top"
    let isnontop = false
    (* Set of parameters *)
    let init_domain ~(params: SS.t) ~(vars: SS.t): unit = ()
    (* Abstractions of transformations *)
    type t = unit
    (* Prtty-printing *)
    let pp (ind: string) (chan: out_channel) (t: t): unit = ()
    (* Temporary *)
    let error (_: string): t = ()
    (* Abstraction of identity function *)
    let id (_: SS.t): t = ()
    (* Composition *)
    let compose _ (_: t) (_: t): t = ()
    (* Lattice operations *)
    let top: t = ()
    let join (_: t) (_: t): t = ()
    let equal (_: t) (_: t): bool = true
    (* Abstraction of basic operations *)
    let assign _ (_: idtf) (_: expr): t = ()
  end: DOM_DIFF_COMP)

module DD_default =
  (struct
    let name = "compositional differentiability"
    let isnontop = true
    (* Some utilities, put here for now, maybe move later *)
    type par =
      | Pvar of string
      | Ppar of string
      | Pdens
    module POrd =
      struct
        type t = par
        let compare = compare
      end
    module PS = Set.Make( POrd )
    module PM = Map.Make( POrd )
    (* Set of parameters *)
    let ref_all_pars: SS.t ref = ref SS.empty
    let ref_all_vars: SS.t ref = ref SS.empty
    let init_domain ~(params: SS.t) ~(vars: SS.t): unit =
      Printf.printf "Domain init:\n - parameters: %a\n - vars: %a\n"
        ss_pp params ss_pp vars;
      ref_all_pars := params;
      ref_all_vars := vars
    (* Abstractions of transformations *)
    type u =
        { (* dependency partial map
           *    when x is not in the map, it means x=>{x}
           *    which means that x depends only on itself *)
          t_dep:  SS.t SM.t;
          (* differentiability partial map
           *    when x is not in the map, it meanx x=>V#
           *    which means x is differentiable wrt all variables *)
          t_diff: SS.t SM.t; }
    type t =
      | T_ok of u (* to fill *)
      | T_err of string
    (* Temporary *)
    let error (msg: string): t = T_err msg
    (* Prtty-printing *)
    let pp (ind: string) (chan: out_channel) (t: t): unit =
      let subind = "    "^ind in
      let f chan m =
        SM.iter
          (fun x -> Printf.fprintf chan "%s%s => %a\n" subind x ss_pp)
          m in
      match t with
      | T_ok u ->
          Printf.fprintf chan "%sOk state\n%s Deps:\n%a%s Diff:\n%a"
            ind ind f u.t_dep ind f u.t_diff
      | T_err s -> Printf.fprintf chan "%sKo state [ %s ]\n" ind s
    (* Some functions for debugging *)
    let crash msg =
      flush stdout;
      failwith msg
    let sanity_check (msg: string) (_: SS.t) (t: t): unit =
      let f m =
        let s = SM.fold (fun x _ -> SS.add x) m SS.empty in
        if not (SS.equal !ref_all_pars s) then
          Printf.printf "Validity condition failed (%s)\n%a" msg (pp "  ") t in
      match t with
      | T_ok u ->
          f u.t_dep;
          f u.t_diff
      | T_err _ -> ()
    (* Abstraction of identity function *)
    (* TODO: maybe make it static! *)
    let uid (_: SS.t): u =
      let allpars = !ref_all_pars in
      let dep =
        SS.fold (fun x -> SM.add x (SS.singleton x)) allpars SM.empty in
      let diff =
        SS.fold (fun x -> SM.add x allpars) allpars SM.empty in
      { t_dep  = dep;
        t_diff = diff }
    let id (_: SS.t): t =
      let allpars = !ref_all_pars in
      let r = T_ok (uid allpars) in
      sanity_check "id" allpars r;
      r
    (* Composition *)
    let compose (allpars: SS.t) (t0: t) (t1: t): t =
      sanity_check "join-l" allpars t0;
      sanity_check "join-r" allpars t1;
      (* TODO: issue: we need to require allpars here,
       *              which is not satisfactory *)
      match t0, t1 with
      | T_ok u0, T_ok u1 ->
          let get_dep0 x =
            try SM.find x u0.t_dep with Not_found -> SS.empty in
          let get_deps0 s =
            SS.fold (fun y -> SS.union (get_dep0 y)) s SS.empty in
          let get_diff0 x =
            try SM.find x u0.t_diff
            with Not_found -> crash (Printf.sprintf "get_diff: %s" x) in
          let get_diffs0 s =
            SS.fold (fun y -> SS.inter (get_diff0 y)) s allpars in
          let dep =
            SM.fold
              (fun x d1 acc ->
                let d = SS.fold (fun y -> SS.union (get_dep0 y)) d1 SS.empty in
                SM.add x d acc
              ) u1.t_dep SM.empty in
          let diff =
            SM.fold
              (fun x p1 acc ->
                let d1 = try SM.find x u1.t_dep with Not_found -> SS.empty in
                let p = get_diffs0 (SS.inter p1 d1) in
                let f y = not (SS.mem y (get_deps0 (SS.diff d1 p1))) in
                let p = SS.filter f p in
                SM.add x p acc
              ) u1.t_diff SM.empty in
          let r = T_ok { t_dep  = dep ;
                         t_diff = diff } in
          sanity_check "join-out" allpars r;
          r
      | T_ok _, _ -> t1
      | _, _ -> t0
    (* Lattice operations *)
    let top: t = T_err "top"
    let join (t0: t) (t1: t): t =
      match t0, t1 with
      | T_ok _, T_ok _ -> T_err "join todo"
      | T_ok _, _ -> t1
      | _, _ -> t0
    let equal (t0: t) (t1: t): bool =
      match t0, t1 with
      | T_ok _, T_ok _ -> Printf.printf "TODO: equal\n"; false
      | _, _ -> false
    (* Abstraction of basic operations *)
    let fv_expr (e: expr) =
      let rec aux_expr acc = function
        | Nil | True | False | Num _ | Str _ -> acc
        | _ -> failwith "todo" in
      aux_expr SS.empty e
    let rec diff_expr (allpars: SS.t) (e: expr): SS.t =
      match e with
      | Nil | True | False | Num _ | Str _ -> allpars
      | _ -> failwith "todo"
    let assign (allpars: SS.t) (x: idtf) (e: expr): t =
      let t = uid allpars in
      let r =
        T_ok { t_dep  = SM.add x (fv_expr e) t.t_dep ;
               t_diff = SM.add x (diff_expr allpars e) t.t_diff } in
      sanity_check "assign" allpars r;
      r
  end: DOM_DIFF_COMP)
