(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** GNU General Public License
 **
 ** Authors:
 **  Wonyeol Lee, KAIST
 **  Xavier Rival, INRIA Paris
 **  Hongseok Yang, KAIST
 **  Hangyeol Yu, KAIST
 **
 ** Copyright (c) 2021 KAIST and INRIA Paris
 **
 ** ddom_diff.mli: domains signatures for compositional information *)
open Lib

open Ddom_sig

module DD_none:    DOM_DIFF_COMP
module DD_default: DOM_DIFF_COMP
