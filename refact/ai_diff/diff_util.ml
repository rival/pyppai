(** pyppai: basic abstract interpreter for python probabilistic programs
 **
 ** GNU General Public License
 **
 ** Authors:
 **  Wonyeol Lee, KAIST
 **  Xavier Rival, INRIA Paris
 **  Hongseok Yang, KAIST
 **  Hangyeol Yu, KAIST
 **
 ** Copyright (c) 2019 KAIST and INRIA Paris
 **
 ** diff_util.ml: utilities for the differentiability analysis *)
open Lib

open Ir_sig


(** Utilities for the printing of results *)
let ppm pp chan m =
  SM.iter (fun s -> Printf.fprintf chan "\t%-30s\t=>\t%a\n" s pp) m

(** Utilities for handling options *)
let bind_opt x f =
  match x with
  | None -> None
  | Some x0 -> f x0

(** Utilities for the operations in the diff domain *)
let map_join_union join m0 m1 =
  SM.fold
    (fun v0 c0 acc ->
      try SM.add v0 (join c0 (SM.find v0 m1)) acc
      with Not_found -> SM.add v0 c0 acc
    ) m0 m1
let map_join_inter join m0 m1 =
  SM.fold
    (fun v0 c0 acc ->
      try match join c0 (SM.find v0 m1) with
      | None -> acc
      | Some c -> SM.add v0 c acc
      with Not_found -> acc
    ) m0 SM.empty
let map_equal pred eq m0 m1 =
  let m0 = SM.filter pred m0 in
  let m1 = SM.filter pred m1 in
  let ck v c0 = try eq c0 (SM.find v m1) with Not_found -> false in
  SM.cardinal m0 = SM.cardinal m1 && SM.for_all ck m0
let lookup_with_default (k: string) (m: 'a SM.t) (default: 'a) : 'a =
  try SM.find k m with Not_found -> default

(** Preanalysis to get the set of parameters for which the differentiability
 ** analysis should provide results *)
let prog_varparams ~(getpars: bool) ~(getvars: bool) (p: prog): SS.t =
  let aux_id acc id =
    if getvars then SS.add id acc
    else acc in
  let aux_par acc id =
    if getpars then SS.add id acc
    else acc in
  let rec aux_expr (acc: SS.t): expr -> SS.t = function
    | Nil | True | False | Num _ | Str _ -> acc
    | Name id -> aux_id acc id
    | UOp (_, e) -> aux_expr acc e
    | BOp (_, e0, e1) | Comp (_, e0, e1) -> aux_expr (aux_expr acc e0) e1
    | List el | StrFmt (_, el) -> aux_expr_list acc el
    | Dict (el0, el1) -> aux_expr_list (aux_expr_list acc el0) el1
  and aux_expr_list (acc: SS.t): expr list -> SS.t =
    List.fold_left aux_expr acc in
  let aux_acmd (acc: SS.t): acmd -> SS.t = function
    | Assert e | Assume e -> aux_expr acc e
    | Assn (id, e) -> aux_expr (aux_id acc id) e
    | AssnCall (_, Name "pyro.param", Str pname :: Name _ :: _, _)
    | AssnCall (_, Name "pyro.param", Str pname :: _, _)
    | AssnCall (_, Name "pyro.module", Str pname :: Name _ :: _, _ ) ->
        aux_par acc pname
    | Sample (_, Str pname, _, _, _) ->
        (* this case cannot filterout the no_obs case as in ai_diff... *)
        (*aux_par pname*) acc
    | AssnCall (_, Name _, _, _) -> acc
    | AssnCall (_, _, _, _) -> failwith "unhandled case"
    | Sample (_, _, _, _, _) -> acc in
  let rec aux_stmt (acc: SS.t): stmt -> SS.t = function
    | Atomic ac -> aux_acmd acc ac
    | If (e, b0, b1) -> aux_block (aux_block (aux_expr acc e) b0) b1
    | For (e0, e1, b) -> aux_block (aux_expr (aux_expr acc e0) e1) b
    | While (e, b) -> aux_block (aux_expr acc e) b
    | With (l, b) ->
        let acc =
          List.fold_left
            (fun acc (e, o) ->
              let acc = aux_expr acc e in
              match o with
              | None -> acc
              | Some e -> aux_expr acc e
            ) acc l in
        aux_block acc b
    | Break | Continue -> acc
  and aux_block (acc: SS.t): block -> SS.t = List.fold_left aux_stmt acc in
  aux_block SS.empty p
